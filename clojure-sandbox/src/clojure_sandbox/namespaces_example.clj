(ns clojure-sandbox.namespaces-example
  (:require [clojure.set :as s])
  (:import (java.util Random)))

; can refer to clojure.set as s because of :require in ns above
(defn do-union [& sets]
  (apply s/union sets))


; can refer to Random without java.util because of :import in ns above
(defn do-random []
  (.nextInt (Random.)))


(do-union)
(do-random)