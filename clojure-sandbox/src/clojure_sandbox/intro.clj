(ns intro)

;; let
; binds variables but only within scope of let
(let [x 10
      y 20]
  (+ x y))
x ;unable to resolve x - it is outside scope of let

(let [numbers '(1 2 3)]
  (apply + numbers)
  ; equivalent to (+ 1 2 3)
  )
; => 6

;; functions
; functions are first-class and can be passed to other functions
; functions can be specified with multiple arities (ex: [], [name], [name greeting])
(defn greet
  ([] (greet "Hello" "World"))
  ([name] (greet "Hello" name))
  ([greeting name] (str greeting ", " name "!")))

(defn concat-strings [& strings]
  (apply str strings))

(defn messenger
  ([] (messenger "Hello, World!"))
  ([msg] (print msg)))

(defn messenger2 [greeting & who]
  (print greeting who))
(messenger2 "hello" "world" "class")

(defn messenger3 [greeting & who]
  (apply print who))
(messenger3 "hello" "world" "class")


;; mutablility
(defn mutability []
  (def my-map {:fred "ethel"})
  (println "my-map=" my-map)
  (def my-map2 (assoc my-map :homer "marge"))
  (println "my-map=" my-map)
  (println "my-map2=" my-map2)
  )


;; closure
(defn greeting-builder [greeting]
  (fn [who] (print greeting who))) ; variable scope of this closure contains greeting

(def hello-er (greeting-builder "Hello")) ; "Hello" goes out of scope after this

(hello-er "world!") ; greeting "Hello" available here because scope contains fn returned by greeting-builder

;; (hello-er "world!") -> ((greeting-builder "Hello") "world!") -> ((fn [who] (print "Hello" who)) "world!")



;; calling Java
; constant, static metods
Math/PI
(Math/sqrt 25)
(Boolean/compare true false)
(System/currentTimeMillis)

; Java like new Integer(99).toString.length()
(.length (.toString 99))
(.. 99 toString length)

; wrapping a java method in a function
(defn length-wrapper [obj] (.length obj))
(length-wrapper "four") ; => 4

(#(.length %) "four") ; => 4

