(ns clojure-sandbox.sequences)


;;;; General Notes ;;;;
;; abstracts iterations
;; can be backed by a data structure or a function
;; any clojure data structure can be treated as a seq
;; can be lazy or infinite
;; seq not frequently used explicitly, but is often used behind the scenes
;; sequences are represented like lists in REPL ex:(1 2 3) but are not lists
; use (set! *print-length* n) to limit number of sequence elements printed to n

;; 4 functions: seq, first, rest, cons
; seq returns a sequence for the given collection (or nil if empty)
; first returns the first element of the given collection
; rest returns the rest of the elements in the collection, ignoring the first element
; cons returns a new sequence consisting of a given item prepended to a given collection


(seq [1 2 3])             ; => (1 2 3)
(first '(1 2 3))          ; => 1
(first [1 2 3])           ; => 1
(first (seq [1 2 3]))     ; => 1
(rest [1 2 3])            ; => (2 3)
(rest (seq [1 2 3]))      ; => (2 3)
(cons 1 [1 2 3])          ; => (1 1 2 3)
(cons 1 (seq [1 2 3]))    ; => (1 1 2 3)




;;;; Sequences over functions ;;;;
(def aRange (range 1 4))          ; aRange points at a lazy sequence - the values are not yet computed
(first aRange)                    ; => 1
(second aRange)                   ; => 2, range function is invoked 2x: once to get first element, again to return 2nd
(rest aRange)                     ; => (2 3)




;;;; Sequence Library ;;;;

;; some functions generate sequences
;; ex:list, vector, map, sql resultset, stream, directory, iterator, xml, etc
(set! *print-length* 10)
(seq [1 2 3])                     ; => (1 2 3)
; range generates a seq of numbers with optional end, start/end, start/end/step
(range)                           ; => (0 1 2 3 4 5 6 7 8 9 ...)
(range 3)                         ; => (0 1 2)
(range 1 7 2)                     ; => (1 3 5)
; iterate returns seq of function evaluation and seed value, w/ previous function output applied in successive evalutions
(iterate #(* 2 %) 2)              ; => (2 4 8 16 32 64 128 256 512 1024 ...)
; re-seq matches regex to string and returns seq of matches
(re-seq #"[aeiou]" "clojure")     ; => ("o" "u" "e")


;; some functions use sequences to generate other sequences
;; ex: map, filter, reduce, count, some, replace, etc
; take returns a sequence of the first 3 items of the given sequence or collection
(take 3 (range))                          ; => (0 1 2)
(take 3 [1 2 3 4 5])                      ; => (1 2 3)
; drop is like take but returns the remainder
(drop 3 (range))                          ; => (3 4 5 6 7 8 9 10 11 12 ...)
; map applies the given function to every element in the given sequence/collection
(map #(* % %) [0 1 2 3])                  ; => (0 1 4 9)
; filter applies predicate to every element of sequence/collection
(filter even? (range))                    ; => (0 2 4 6 8 10 12 14 16 18 ...)
(filter odd? [1 2 3 4 5])                 ; => (1 3 5)
(apply str (interpose "," (range 3)))     ; => "0,1,2"


;; some functions use sequences to generate other data structures
; reduce invokes the given function with each value in the sequence, using the previous invokation result as the first
;  arg in the next invocation
(reduce + (range 4))              ; => 6
(reduce + 10 (range 4))           ; => 16, 10 is only used as the first arg to + the first invocation
; into pours values from sequence (or sequence backing object) into the given data structure
(into #{} "hello")                ; => #{\e \h \l \o}, only 1 'l' because #{} is a set
(into #{"z"} "hello")             ; => #{"z" \e \h \l \o}
(into {} [[:x 1] [:y 2]])         ; => {:x 1, :y 2}
; some takes a function and a collection/sequence and returns first value of the function that resolves to true
(some {2 :b 3 :c} [1 nil 2 3])    ; => :b


(def fibs
  (map first
       (iterate (fn [[a b]]
           [b (+ a b)])
         [0 1])))
(take 5 fibs)               ; => (0 1 1 2 3)
(drop 5 fibs)               ; => (5 8 13 21 34 55 89 144 233 377 ...)
(map inc (take 5 fibs))     ; => (1 2 2 3 4)


