(ns clojure-sandbox.namespaces)

;; introspecting namespaces
; returns map of public symbols in given namespace
(ns-publics 'clojure.string)
(ns-publics 'clojure.java.io)
; also
(dir clojure.java.io)

; *ns* will tell you the current namespace you're in
*ns*

; the-ns returns the Namespace object for the given namespace
(the-ns 'clojure.core)

; ns-name returns the namespace name as a symbol
(ns-name 'clojure.core)

; ns-map returns a map of all symbols in the given namespace
(ns-map 'clojure.core)

; ns-interns returns a map of only def'd variables
(ns-interns 'clojure.core)

; ns-publics returns a map of public vars
(ns-publics 'clojure.core)

; ns-imports returns a map of imported java classes in the namespace
(ns-imports 'clojure.core)

; ns-refers returns a map of vars from other namespaces
(ns-refers 'clojure.repl)

; ns-aliases returns a map of all aliases
(ns-aliases 'clojure.core)



;; qualifying function calls:
(clojure.string/replace "The quick, brown fox" "quick" "slow")
(clojure.core/replace ["zero" "one" "two" "three" "four" "five"] [4 2 0])

;; namespace-qualifying keywords
:x      ; no namespace
::x     ; namespace "clojure-sandbox.namespaces"
:foo/x  ; namespace "foo"


;; require loads the namespace if not already loaded
(require 'clojure.set)
(clojure.set/union #{1 2} #{2 3 4})
; optionally a name can be given
(require '[clojure.set :as set])
(set/union #{1 2} #{2 3 4})
; optionally pass :reload or :reload-all to force reload
(require 'clojure.set :reload)
(require 'clojure.set :reload-all) ; reload-all reloads dependency + it's dependencies, but no deeper transitives


;; use loads the namespace if not already loaded
; argument must be a symbol, and must be quoted
; refers all symbols into current namespace
; warns when symbols crash
; not recommended except for REPL
(reverse "string") ; => (\g \n \i \r \t \s) (resolves to clojure.core/reverse)
(use 'clojure.string) ; throws warning for function names reverse, replace that collide
(reverse "string") ; => "gnirts" (resolves to clojure.string/reverse)

; use only to load only specific symbols
(use '[clojure.string :only (reverse)]) ; => warns only about reverse


;; import makes Java classes available without package qualification
; argument is a list
; does not support *
(import (java.util UUID Random))
(UUID/randomUUID)


;; ns creates a namespace and loads given namespaces
; also loads all of clojure.core, java.lang
(ns foo.bar)
; use :require to load other namespaces, optionally with an alias
; this is probably used in the ns statement at the top of the file
(ns this.namespace
  (:require [some.other.namespace :as otherone]))
; now we can ref (otherone/foo)

; use :use to combine (use) behavior with (ns)
(ns this.namespace
  (:use [some.other.namespace :only (foo bar)]))
; now we can ref (bar) rather than (some.other.namespace/bar)

; use :import to combine (import) behavior with (ns)
(ns this.namespace
  (:import (java.util Random UUID)))
; now we can ref Random instead of java.util.Random

; an example of what an ns statement might look like in a real program
(ns name
  (:require [some.ns.foo :as foo]
            [other.ns.bar :as bar])
  (:use [this.ns.baz :only (a b c)]
        [that.ns.quux :only (d e f)])
  (:import (java.io File FileWriter)
           (java.net URL URI)))


;; private vars
; use ^:private metadata to definition
; or use defn- as a shortcut
; prevents symbols from being pulled in by use
; !! these can be worked around so not truly hidden !!


