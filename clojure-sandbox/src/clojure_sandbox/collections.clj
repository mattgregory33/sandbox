(ns clojure-sandbox.collections
  (:require [clojure.set :as set]))


;;;; General Notes ;;;;
;; seq can be used across all collection types
;; collections are immutable
;; immutability is efficient because collections are persistent
;; persistence ex:
; linked list x = x1->x2->x3
; linked list y = y1->y2->x1->x2->x3
; y is y1->y2 + x, x is unchanged and still available



;;;; Lists ;;;;
;; singly linked
;; prepend is O(1)
;; lookup is O(1) at head, O(n) anywhere else
;; ordered

()                ;=> an empty list
(1 2 3)           ;=> error, because clojure interprets 1 as a function
(list 1 2 3)      ;=> (1 2 3)
'(1 2 3)          ;=> (1 2 3)
(quote (1 2 3))   ;=> (1 2 3)
; conj adds 2nd param to given list in whatever way is most efficient
(conj '(2 3) 1)   ;=> (1 2 3), 1 is prepended because prepending is most efficient for lists

(def myList '(2 3))
(conj myList 1)   ;=> (1 2 3)
myList            ;=> (2 3), still unchanged



;;;; Vectors ;;;;
;; similar to arrays
;; append is O(1)
;; lookup is O(1)
;; ordered

[]                    ;=> an empty vector
[1 2 3]               ;=> [1 2 3]
[1 2 (+ 1 2)]         ;=> [1 2 3]
'[1 2 (+ 1 2)]        ;=> [1 2 (+ 1 2)]
(vector 1 2 3)        ;=> [1 2 3]
(vec '(1 2 3))        ;=> [1 2 3]
(vec myList)          ;=> [2 3]
(nth [1 2 3] 0)       ;=> 1
(nth (vec myList) 0)  ;=> 2
(conj [2 3] 1)        ;=> [2 3 1], 1 is appended because it's most efficient for vectors



;;;; Maps ;;;;
;; key/value pairs (i.e. HashMap)
;; insert and lookup are O(1)
;; unordered

{}                    ;=> an empty map
{:a 1 :b 2}           ;=> {:a 1, :b 2}
(:a {:a 1 :b 2})      ;=> 1, use keyword in function position to access element of map
(:b {:a 1 :b 2})      ;=> 2
({:a 1 :b 2} :a)      ;=> 1, effectively a key lookup on keyword :a
(assoc {:a 1} :b 2)   ;=> {:a 1, :b 2}
(dissoc {:a 1} :a)    ;=> {}
(conj {} [:a 1])      ;=> {:a 1}
(conj {:b 2} [:a 1])  ;=> {:a 1}

(def jdoe {:name "John Doe", :address {:zip 90210}})
(get jdoe :name)                        ;=> "John Doe"
(get jdoe :age 21)                      ;=> 21, no :age in map but 21 passed in as default
(get jdoe :name "Bob")                  ;=> "John Doe"
(get-in jdoe [:address :zip])           ;=> 90210
(get-in jdoe [:address :street] "Main") ;=> "Main"
; assoc-in assigns a value in the path and returns a new map
(assoc jdoe :name "Jane Doe")           ;=> {:name "Jane Doe", :address {:zip 90210}}
(assoc-in jdoe [:address :zip] 12345)   ;=> {:name "John Doe", :address {:zip 12345}}
; update-in executes a function on a value in the path and returns a new map
(update-in jdoe [:address :zip] inc)    ;=> {:name "John Doe", :address {:zip 90211}}
jdoe                                    ;=> {:name "John Doe", :address {:zip 90210}}



;;;; Sets ;;;;
;; a collection of distinct values (i.e. Set)
;; inserts are O(1)
;; lookups are O(1)
;; unordered

#{}                                     ;=> an empty set
#{:a :b}                                ;=> #{:b :a}
(#{:a :b} :a)                           ;=> :a
(conj #{:a :b} :c)                      ;=> #{:c :b :a}
(contains? #{:a :b} :a)                 ;=> true

(set/union #{:a} #{:b})                 ;=> #{:b :a}
(set/difference #{:a :b} #{:a})         ;=> #{:b}
(set/intersection #{:a :b} #{:b :c})    ;=> #{:b}