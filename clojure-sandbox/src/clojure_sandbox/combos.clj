(ns combos)


(defn validMonth [combo]
  (and (>= combo 100) (<= combo 1299)))


(defn dayPart [combo]
  (* (- (/ combo 100) (int (/ combo 100))) 100))


(defn validDay [combo]
  (and (>= (dayPart combo) 1) (<= (dayPart combo) 31)))


(defn noDupes [combo]
  (== (count (seq (format "%04d" combo))) (count (distinct (seq (format "%04d" combo))))))


;; prints out 4-digit padlock combinations fitting the following criteria:
;; 1) matches pattern MMDD (assuming all months have 31 days)
;; 2) contains no duplicate digits
(loop [i 100]
  (if (<= i 1231)
    (do
      (if (and (validMonth i) (validDay i) (noDupes i))
        (printf "%04d\n" i))
      (recur (inc i))))
  )

