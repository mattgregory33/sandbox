(ns clojure-sandbox.destructuring)


;;;; General Notes ;;;;
;; destructuring is a declarative way of pulling apart data from lists, vectors, maps, etc
;; nests to allow deep, arbitrary access into data structures
;; can be used in fn, defn params and let bindings



;;;; Sequential Destructuring ;;;;
(def stuff [7 8 9 10 11])
; binds a, b, c to first three elements in stuff
(let [[a b c] stuff]
  (list (+ a b) (+ b c)))     ; => (15 17)
; f is bound to nil because there are only 5 elements in stuff
(let [[a b c d e f] stuff]
  (list d e f))               ; => (10 11 nil)

; can also destructure everything else in a collection using &
(let [[a & others] stuff]
  (println a)                 ; => 7
  (println others))           ; => (8 9 10 11)

; discard elements using _
; _ is a perfectly valid symbol and just used by convention as a placeholder for discarded elements in destructures
(let [[_ & others] stuff]
  (println others)            ; => (8 9 10 11)
  (println _))                ; => 7
(let [[_ _ & others] stuff]
  (println others))           ; => (9 10 11)

(def names ["Bob" "Sally" "Joe"])
(let [[name1] names] name1)                 ; => "Bob"
(let [[_ name2] names] name2)               ; => "Sally"
(let [[_ & otherNames] names] otherNames)   ; => ("Sally" "Joe")

; desctructuring can be done in function params as well
(defn rest-names [[_ & rest-names]]
  rest-names)
(rest-names names)                          ; => ("Sally" "Joe")




;;;; Associative (map) Destructuring ;;;;
(def stuffMap {:a 7 :b 4})
; binds values for keys :a and :b to vars a and b
(let [{a :a, b :b} stuffMap]
  [a b])                              ; => [7 4]
(let [{a :a, b :b} stuffMap]
  (+ a b))                            ; => 11
; can also use :keys keyword, this fetches keys :a and :b and makes them available in vars of the same name
(let [{:keys [a b]} stuffMap]
  [a b])                              ; => [7 4]
; undefined keys resolve to nil
(let [{:keys [a b c]} stuffMap]
  [a b c])                            ; => [7 4 nil]
; use :or keyword to provide a default value
(let [{:keys [a b c]
       :or {c 3}} stuffMap]
  [a b c])                            ; => [7 4 3]

; applying a vector of keys with & in a function declaration emulates named arguments
(defn game [planet & {:keys [human-players computer-players]}]
  (println "Planet:      " planet)
  (println "Num Players: " (+ human-players computer-players)))
(game "Planet X" :human-players 1 :computer-players 999)   ; named args emulated


(defn draw-point [& {:keys [x y z]
                     :or {x 0
                          y 0
                          z 0}}]
  [x y z])
(draw-point)                          ; => [0 0 0]
(draw-point :x 10 :y 20)              ; => [10 20 0]
(draw-point :x 10 :y 20 :z 30)        ; => [10 20 30]
; named args allow them to be passed in any order
(draw-point :z 30 :y 20 :x 10)        ; => [10 20 30]