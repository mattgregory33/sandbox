(ns clojure-sandbox.flowcontrol)


;;;; General Notes ;;;;

;; everything in clojure is an expression, which always return a value
;; blocks of multiple expressions (ex: let, do, fn) return the value of the last expression
;; flow control operators are expressions
; composable
; extensible (via macros)


;;;; Truthiness ;;;;
(if true :truthy :falsey)       ;=> :truthy
(if (Object.) :truthy :falsey)  ;=> :truthy
(if [] :truthy :falsey)         ;=> :truthy

(if false :truthy :falsey)      ;=> :falsey
(if nil :truthy :falsey)        ;=> :falsey
(if (seq []) :truthy :falsey)   ;=> :falsey

(if Boolean/FALSE :truthy :falsey)    ;=> :falsey
(def evil-false (Boolean. false))
(= evil-false false)                  ;=> true
(if evil-false :truthy :falsey)       ;=> :truthy, because it is an Object



;;;; if ;;;;
(str "2 is " (if (even? 2) "even" "odd"))     ;=> "2 is even"
(if (true? false) "impossible!")              ;=> nil, because no false branch



;;;; if/do ;;;;
;; allows multiple expressions per branch
(if (even? 5)
  (do (println "even")
      true)
  (do (println "odd")
      false))                                 ;=> prints "odd", returns false



;;;; if-let ;;;;
;; allows assignment like let in combination with if
(if-let [x (even? 3)]
  (println x)
  (println "odd"))                            ;=> prints "odd", returns nil
; x is available to println x because of let


(defn show-evens [coll]
  (if-let [evens (seq (filter even? coll))]
    (println (str "The evens are: " evens))
    (println "There were no evens.")))
(show-evens (range 10))                       ;The evens are: (0 2 4 6 8)
(show-evens [1 3 5 7])                        ;There were no evens.




;;;; cond ;;;;
;; returns first condition that evaluates to true
(let [x 5]
  (cond
    (< x 2) "x < 2"
    (< x 10) "x < 10"))                       ;=> "x < 10"
(let [x 1]
  (cond
    (< x 2) "x < 2"
    (< x 10) "x < 10"))                       ;=> "x < 2"
(let [x 11]
  (cond
    (< x 2) "x < 2"
    (< x 10) "x < 10"
    :else "x >= 11"))                       ;=> "x >= 11"



;;;; condp ;;;;
;; like cond but evaluates function predicate
(defn foo [x]
  (condp = x
    5 "x=5"
    10 "x=10"
    "neither"))
(foo 11)                          ;=> "neither



;;;; case ;;;;
;; like switch
;; matching is O(1)
(defn foo-c [x]
  (case x
    5 "x=5"
    10 "x=10"
    "neither"))
(foo-c 11)                          ;=> "neither
; foo-c is same as foo above, except faster


(defn str-binary-cond [n]
  (cond
    (= n 0) "zero"
    (= n 1) "one"
    :else "unknown"))
(str-binary-cond 0)            ;=> "zero"
(str-binary-cond 1)            ;=> "one"
(str-binary-cond 2)            ;=> "unknown"

(defn str-binary-condp [n]
  (condp = n
    0 "zero"
    1 "one"
    "unknown"))
(str-binary-condp 0)            ;=> "zero"
(str-binary-condp 1)            ;=> "one"
(str-binary-condp 2)            ;=> "unknown"

(defn str-binary-case [n]
  (case  n
    0 "zero"
    1 "one"
    "unknown"))
(str-binary-case 0)            ;=> "zero"
(str-binary-case 1)            ;=> "one"
(str-binary-case 2)            ;=> "unknown"
