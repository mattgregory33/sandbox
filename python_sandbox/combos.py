for x in range(1, 1232):
    combo = format(x, '04')
    monthPart = combo[0:2]
    dayPart = combo[2:4]
    if (int(monthPart) <= 12 and int(dayPart) <= 31):
        if (len(set(list(monthPart + dayPart))) > 3):
            print(monthPart + dayPart)

