package org.mgregory.samples.json.jackson;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/26/12
 */
public class MappingListOfDatesToJsonSample {

    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));

        List<Date> listOfDates = new ArrayList<Date>(5);
        listOfDates.add(new Date());
        listOfDates.add(new Date());
        listOfDates.add(new Date());
        listOfDates.add(new Date());
        listOfDates.add(new Date());

        try {
            mapper.writeValue(System.out, listOfDates);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
