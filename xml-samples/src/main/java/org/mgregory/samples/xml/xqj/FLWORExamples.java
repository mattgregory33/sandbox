package org.mgregory.samples.xml.xqj;

import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.XQuery;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 2/15/13
 * Time: 4:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class FLWORExamples {

    static Context context = new Context();


    @Test
    public void filterElementsBySingleDirectChildElementValue() throws BaseXException {
        StringBuffer query = new StringBuffer();
        query.append("let $doc := doc('xml-samples/src/main/resources/xml/videos.xml')").append("\n");
        query.append("for $v in $doc//video\n");//.append("$a in $doc//actors/actor").append("\n");
        query.append("where ends-with($v/title, '2')").append("\n");
//        query.append("and $v/actorRef = $a/@id").append("\n");
        query.append("return $v");

        query(query.toString());
    }

    /**
     * This method evaluates a query by using the database command.
     * The results are automatically serialized and printed.
     * @param query query to be evaluated
     * @throws org.basex.core.BaseXException if a database command fails
     */
    static void query(final String query) throws BaseXException {
        System.out.println(new XQuery(query).execute(context));
    }

}
