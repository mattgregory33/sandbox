package org.mgregory.samples.xml.xqj;

import net.xqj.basex.BaseXXQDataSource;

import javax.xml.namespace.QName;
import javax.xml.xquery.*;

/**
 * User: matt
 * Date: 2/12/13
 * Time: 9:00 PM
 */
public class HelloBaseXWorld {

    public static void main(String[] args) throws XQException
    {
        XQDataSource xqs = new BaseXXQDataSource();
        xqs.setProperty("serverName", "localhost");
        xqs.setProperty("port", "1984");

        // Change USERNAME and PASSWORD values
        XQConnection conn = xqs.getConnection("USERNAME", "PASSWORD");

        XQPreparedExpression xqpe =
                conn.prepareExpression("declare variable $x as xs:string external; $x");

        xqpe.bindString(new QName("x"), "Hello World!", null);

        XQResultSequence rs = xqpe.executeQuery();

        while(rs.next())
            System.out.println(rs.getItemAsString(null));

        conn.close();
    }

}
