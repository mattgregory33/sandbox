package org.mgregory.samples.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/12/12
 */
@Path("/helloworld")
public class HelloJerseyWorldResource {

    @GET
    @Produces("text/plain")
    public String sayHello() {
        return "Hello from Jersey!";
    }

    @GET
    @Produces("text/plain")
    @Path("/{name}")
    public String sayHelloTo(@PathParam("name") String name) {
        return "Hello to " + name + " from Jersey!";
    }

    @GET
    @Produces("text/plain")
    @Path("/bye")
    public String sayGoodbye() {
        return "Goodby from Jersey!";
    }

    @GET
    @Produces("text/plain")
    @Path("/{name}/bye")
    public String sayGoodbyeTo(@PathParam("name") String name) {
        return "Goodbye to " + name + " from Jersey!";
    }
}
