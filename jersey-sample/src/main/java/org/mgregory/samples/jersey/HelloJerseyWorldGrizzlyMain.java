package org.mgregory.samples.jersey;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.grizzly.http.server.HttpServer;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/12/12
 */
public class HelloJerseyWorldGrizzlyMain {

    public static final URI BASE_URI = getBaseURI();

    protected static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/").port(9999).build();
    }

    protected static HttpServer startServer() throws IOException {
        System.out.println("Starting Grizzly...");
        ResourceConfig rc = new PackagesResourceConfig("org.mgregory.samples.jersey");
        return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
    }

    public static void main(String[] args) throws IOException {
        HttpServer httpServer = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nTry out %shelloworld\nHit enter to stop it...",
                BASE_URI, BASE_URI));
        System.in.read();
        httpServer.stop();
    }
}
