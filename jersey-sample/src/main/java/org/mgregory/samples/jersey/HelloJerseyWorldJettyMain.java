package org.mgregory.samples.jersey;

import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/12/12
 */
public class HelloJerseyWorldJettyMain {

    public static final URI BASE_URI = getBaseURI();

    protected static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/").port(8080).build();
    }

    protected static Server startServer() throws Exception {
        System.out.println("Starting Jetty...");
        Server server = new Server(8080);
        Context root = new Context(server, "/scheduler", Context.SESSIONS);
        root.addServlet(new ServletHolder(new ServletContainer(new PackagesResourceConfig("org.mgregory.samples.jersey"))), "/*");
        server.start();
        System.out.println("context path: " + root.getContextPath());
        return server;
    }

    public static void main(String[] args) throws Exception {
        Server server = null;
            System.setProperty("DEBUG", "true");
            server = startServer();
            System.out.println(String.format("Jersey app started with WADL available at "
                    + "%sapplication.wadl\nTry out %shelloworld\nHit enter to stop it...",
                    BASE_URI, BASE_URI));
            System.in.read();
            server.stop();
    }
}
