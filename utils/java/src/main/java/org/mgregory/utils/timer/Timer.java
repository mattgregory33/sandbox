package org.mgregory.utils.timer;

/**
 * Use this class to enable a simple timer mechanism.  Wrap code you want to time in startTimer() and stopTimer()
 * calls to capture the number of milliseconds spent between startTimer() and stopTimer() calls.
 */
public class Timer {

    private String name;
    private long start = 0;
    private long time = 0;

    private Timer(String name) {
        this.name = name;
    }

    public static Timer startTimer(String name) {
        Timer t = new Timer(name);
        t.start();
        return t;
    }

    private void start() {
        start = System.currentTimeMillis();
        time = 0;
    }

    public void stopTimer() {
        time = System.currentTimeMillis() - start;
    }

    @Override
    public String toString() {
        if (time == 0) {
            return name + " still running";
        } else {
            return name + " completed in " + time + "ms";
        }
    }
}
