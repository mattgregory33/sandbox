package org.mgregory.samples.jackson.custommapping.beans;

import java.util.List;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/7/12
 */
//@JsonSerialize(using = WrappingSerializer.class)
public class ParentBean extends IdentifiedBean {

    private String parentField;
    private List<ChildBean> children;

    public String getParentField() {
        return parentField;
    }

    public void setParentField(String parentField) {
        this.parentField = parentField;
    }

    public List<ChildBean> getChildren() {
        return children;
    }

    public void setChildren(List<ChildBean> children) {
        this.children = children;
    }

}
