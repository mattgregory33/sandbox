package org.mgregory.samples.jackson.custommapping;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.mgregory.samples.jackson.custommapping.beans.ChildBean;
import org.mgregory.samples.jackson.custommapping.beans.ParentBean;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/7/12
 */
public class SerializationRunner {

    public static void main(String[] args) throws IOException {
        ParentBean parent = new ParentBean();
        ChildBean child1 = new ChildBean();
        ChildBean child2 = new ChildBean();

        child1.setChildField("child1");
        child2.setChildField("child2");
        parent.setParentField("parent");
        parent.setChildren(Arrays.asList(child1, child2));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
        System.out.println(mapper.writeValueAsString(parent));

        System.out.println("\n\n");

        System.out.println(mapper.writeValueAsString(child1));
    }
}
