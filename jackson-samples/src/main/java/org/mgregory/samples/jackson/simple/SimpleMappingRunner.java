package org.mgregory.samples.jackson.simple;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.mgregory.samples.jackson.simple.beans.SimplePersonBean;

import java.io.IOException;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 7/3/12
 */
public class SimpleMappingRunner {

    public static void main(String[] args) throws IOException {
        SimplePersonBean person = new SimplePersonBean();
        person.setFirstName("matt");
        person.setLastName("gregory");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
        System.out.println(mapper.writeValueAsString(person));
    }
}
