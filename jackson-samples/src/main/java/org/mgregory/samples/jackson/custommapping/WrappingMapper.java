package org.mgregory.samples.jackson.custommapping;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/17/12
 */
public class WrappingMapper extends ObjectMapper {

    @Override
    public String writeValueAsString(Object value) throws IOException, JsonGenerationException, JsonMappingException {
        writeValueAsStringPre(value);
        super.writeValueAsString(value);    //To change body of overridden methods use File | Settings | File Templates.
        return writeValueAsStringPost(value);
    }

    private String writeValueAsStringPost(Object value) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    private void writeValueAsStringPre(Object value) {
        //To change body of created methods use File | Settings | File Templates.
    }
}
