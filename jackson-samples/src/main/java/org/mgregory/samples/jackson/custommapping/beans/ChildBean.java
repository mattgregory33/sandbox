package org.mgregory.samples.jackson.custommapping.beans;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/7/12
 */
public class ChildBean extends IdentifiedBean {

    private String childField;

    public String getChildField() {
        return childField;
    }

    public void setChildField(String childField) {
        this.childField = childField;
    }
}
