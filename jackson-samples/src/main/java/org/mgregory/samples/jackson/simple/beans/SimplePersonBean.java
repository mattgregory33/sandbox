package org.mgregory.samples.jackson.simple.beans;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 7/3/12
 */
public class SimplePersonBean {

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
