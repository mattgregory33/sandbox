package org.mgregory.samples.jackson.custommapping.customserializer;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.mgregory.samples.jackson.custommapping.beans.ParentBean;

import java.io.IOException;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/7/12
 */
public class WrappingSerializer extends JsonSerializer<ParentBean> {

    @Override
    public void serialize(ParentBean value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        jgen.writeString(value.getBeanId());
    }
}
