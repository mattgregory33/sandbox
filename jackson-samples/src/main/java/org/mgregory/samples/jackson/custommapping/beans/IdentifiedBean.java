package org.mgregory.samples.jackson.custommapping.beans;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonTypeIdResolver;
import org.mgregory.samples.jackson.custommapping.customtypeid.MetaWrappingTypeIdResolver;

import java.util.UUID;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/7/12
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CUSTOM, include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonTypeIdResolver(value = MetaWrappingTypeIdResolver.class)
public class IdentifiedBean //implements JsonSerializableWithType
 {

    private String beanId;

    public String getBeanId() {
        return UUID.randomUUID().toString();
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

//     @Override
//     public void serializeWithType(JsonGenerator jgen, SerializerProvider provider, TypeSerializer typeSer) throws IOException, JsonProcessingException {
//         jgen.writeStartObject();
//         jgen.writeStringField("serializedFrom", "serializeWithType()");
//         jgen.writeStringField("type", typeSer.getTypeIdResolver().idFromValue(this));
//         jgen.writeObjectField("typeSer", typeSer);
////         jgen.writeStringField("beanId", this.getBeanId());
//         jgen.writeEndObject();
//     }
//
//     @Override
//     public void serialize(JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
//         jgen.writeStartObject();
//         jgen.writeStringField("serializedFrom", "serialize()");
//         jgen.writeStringField("beanId", this.getBeanId());
//         jgen.writeEndObject();
//     }

 }
