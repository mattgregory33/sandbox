package org.mgregory.samples.jackson.custommapping.customtypeid;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.jsontype.TypeIdResolver;
import org.codehaus.jackson.type.JavaType;
import org.mgregory.samples.jackson.custommapping.beans.IdentifiedBean;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 6/7/12
 */
public class MetaWrappingTypeIdResolver implements TypeIdResolver {

    public MetaWrappingTypeIdResolver() {}

    @Override
    public String idFromValue(Object value) {
        StringBuilder metaWrapper = new StringBuilder();
        metaWrapper.append("\"guid\"").append(":").append("\"").append(((IdentifiedBean) value).getBeanId()).append("\",\n");
        metaWrapper.append("\"field_values\":");
        System.out.println(metaWrapper.toString());
        return metaWrapper.toString();
    }

    @Override
    public String idFromValueAndType(Object value, Class<?> suggestedType) {
        System.out.println(value);
        System.out.println(suggestedType);
        return "idFromValueAndType";
    }

    @Override
    public JavaType typeFromId(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.CUSTOM;
    }

    @Override
    public void init(JavaType javaType) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
