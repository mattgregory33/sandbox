require('set')

for i in 1..1231
  combo = "%04d" % i
  monthPart = combo[0..1]
  dayPart = combo[2..3]

  if monthPart.to_i <= 12 && dayPart.to_i <= 31
    if combo.split("").to_set.length == 4
      puts monthPart + dayPart
    end
  end
end