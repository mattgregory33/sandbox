package org.mgregory.samples.java.threadedobserver;

import java.util.Observable;

/**
 * User: matt
 * Date: 4/13/12
 * Time: 10:58 PM
 */
public class SampleObservableRunnable extends Observable implements Runnable {

    private boolean shouldStopProcess;

    @Override
    public void run() {
        System.out.println("SampleObservableRunnable executing...");
        if (System.currentTimeMillis() % 9 == 0)
            setShouldStopProcess(true);
    }

    public boolean isShouldStopProcess() {
        return shouldStopProcess;
    }

    public void setShouldStopProcess(boolean shouldStopProcess) {
        System.out.println("Oops, something happened");
        this.shouldStopProcess = shouldStopProcess;
        setChanged();
        notifyObservers();
    }
}
