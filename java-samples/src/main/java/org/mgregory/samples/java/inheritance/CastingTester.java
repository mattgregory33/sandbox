package org.mgregory.samples.java.inheritance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 11/28/12
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class CastingTester {

    @Test
    public void testCastingDoesNotCreateNewObject() {
        String s = "a new string";
        Object o = (Object) s;

        System.out.println(s);
        System.out.println(o);

        assertEquals(s, o);
    }
}
