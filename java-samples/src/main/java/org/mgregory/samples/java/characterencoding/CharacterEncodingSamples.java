package org.mgregory.samples.java.characterencoding;

import org.junit.Ignore;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 5/8/13
 * Time: 9:46 AM
 * //TODO: Responsibility Statement
 */
public class CharacterEncodingSamples {

    private final String leftQuote = "“";

    @Ignore
    @Test
    public void printCharsets() {
        for (Charset charSet : Charset.availableCharsets().values()) {
            System.out.println(String.format("%s %s", charSet.name(), charSet.displayName()));
        }
    }

    @Test
    public void testConvertingToAndFromDifferentEncodings() throws UnsupportedEncodingException {
        doConversion(leftQuote, "UTF-8", "UTF-8");
        doConversion(leftQuote, "UTF-16", "UTF-16");
        doConversion(doConversion(doConversion(leftQuote, "UTF-8", "UTF-16"), "UTF-8", "UTF-16"), "UTF-8", "UTF-16");
        doConversion(doConversion(doConversion(leftQuote, "UTF-16", "UTF-8"), "UTF-16", "UTF-8"), "UTF-16", "UTF-8");
        doConversion(doConversion(doConversion(leftQuote, "UTF-8", "UTF-32"), "UTF-8", "UTF-32"), "UTF-8", "UTF-32");
        doConversion(doConversion(doConversion(leftQuote, "UTF-32", "UTF-8"), "UTF-32", "UTF-8"), "UTF-32", "UTF-8");
        doConversion(doConversion(doConversion(leftQuote, "UTF-16", "UTF-32"), "UTF-16", "UTF-32"), "UTF-16", "UTF-32");
        doConversion(doConversion(doConversion(leftQuote, "UTF-32", "UTF-16"), "UTF-32", "UTF-16"), "UTF-32", "UTF-16");
        doConversion(doConversion(doConversion(leftQuote, "windows-1232", "UTF-16"), "windows-1232", "UTF-16"), "windows-1232", "UTF-16");
        doConversion(doConversion(doConversion(leftQuote, "UTF-16", "windows-1232"), "UTF-16", "windows-1232"), "UTF-16", "windows-1232");

    }

    private String doConversion(String character, String fromEncoding, String toEncoding) throws UnsupportedEncodingException {
        String converted = convertEncoding(character, fromEncoding, toEncoding);
        System.out.println(String.format("%s -> %s = %s -> %s", fromEncoding, toEncoding, character, converted));
        return converted;
    }


    private String convertEncoding(String character, String fromEncoding, String toEncoding) throws UnsupportedEncodingException {
        byte[] bytes = character.getBytes(fromEncoding);
        return new String(bytes, Charset.forName(toEncoding));
    }


}
