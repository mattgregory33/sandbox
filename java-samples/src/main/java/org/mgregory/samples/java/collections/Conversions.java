package org.mgregory.samples.java.collections;

import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;

public class Conversions {

    @Test
    public void convertSetToList() {
        Set<String> theSet = new HashSet<String>();
        String one = "1";
        theSet.add(one);
        String two = "2";
        theSet.add(two);
        String three = "3";
        theSet.add(three);

        List<String> actual = Arrays.asList(theSet.toArray(new String[]{}));

        List<String> expected = Arrays.asList(one, two, three);

        assertThat(actual, hasItems(expected.toArray(new String[expected.size()])));
    }
}
