package org.mgregory.samples.java.inheritance;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 11/14/12
 * Time: 2:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class InheritanceTester {

    @Test
    public void testConstantInheritance() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ParentClass parent = new ParentClass();
        assertEquals(1337, parent.getValue());

        ChildClass child = new ChildClass();
        assertEquals(1337, child.getValue());

        Method getValueMethod = ParentClass.class.getDeclaredMethod("getValue", null);
        getValueMethod.setAccessible(true);

        assertEquals(1337, getValueMethod.invoke(parent, null));
    }
}
