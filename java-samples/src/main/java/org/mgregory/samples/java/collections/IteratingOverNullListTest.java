package org.mgregory.samples.java.collections;

import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 12/5/12
 * Time: 8:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class IteratingOverNullListTest {

    @Test
    public void testIterationOverNullList() {

        List l = null;

        for (Object o : l) {
            System.out.println("in the loop");
        }
    }

    @Test
    public void testIterationOverWrappedNullList() {
        AnObjectWithAList objectWithAList = new AnObjectWithAList();
        for (Object o: objectWithAList.getList()) {
            System.out.println("in the loop");
        }
    }

    private class AnObjectWithAList {
        List l;
        List getList() {
            return l;
        }
    }
}
