package org.mgregory.samples.java.dates;

import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 11/30/12
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingCycleSimulator {

    @Test
    public void simulateBillingCyclesUsingSimpleNDayAlgorithm() {
        int dayOfMonth = 1;
        int cycleLength = 30;

        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");

        Calendar cycleDate = new GregorianCalendar(2012, Calendar.JANUARY, dayOfMonth);
        for (int i=0; i<12; i++) {
            System.out.print("billing cycle: " + formatter.format(cycleDate.getTime()));
            cycleDate.add(Calendar.DAY_OF_MONTH, cycleLength);
            System.out.println(" - " + formatter.format(cycleDate.getTime()));
            cycleDate.add(Calendar.DAY_OF_MONTH, 1);
        }

    }
}
