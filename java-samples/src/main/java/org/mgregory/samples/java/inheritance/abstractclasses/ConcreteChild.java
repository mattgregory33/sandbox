package org.mgregory.samples.java.inheritance.abstractclasses;

public class ConcreteChild extends AbstractParent {


    @Override
    public String sayHelloFrom() {
        return "ConcreteChild says Hello from " + getClassName();
    }
}
