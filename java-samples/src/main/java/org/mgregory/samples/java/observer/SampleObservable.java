package org.mgregory.samples.java.observer;

import java.util.Observable;

/**
 * User: matt
 * Date: 4/13/12
 * Time: 10:22 PM
 */
public class SampleObservable extends Observable {

    private String value = "initial";

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        System.out.println("I'm changing my value from " + this.value + " to " + value);
        this.value = value;
        setChanged();
        notifyObservers();
    }
}
