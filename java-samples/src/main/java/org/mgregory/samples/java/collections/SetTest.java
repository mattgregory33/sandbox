package org.mgregory.samples.java.collections;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 11/12/12
 * Time: 3:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetTest {

    private Set<Long> setOfLongs;

    @Before
    public void setUp() throws InterruptedException {
        setOfLongs = new HashSet<Long>();
        setOfLongs.add(System.currentTimeMillis()); Thread.sleep(1);
        setOfLongs.add(System.currentTimeMillis()); Thread.sleep(1);
        setOfLongs.add(System.currentTimeMillis()); Thread.sleep(1);
        setOfLongs.add(System.currentTimeMillis());
    }

    @Test
    public void testSetToString() {
        System.out.println(setOfLongs.toString());
    }

    @Test
    public void blah() {
        System.out.println(setOfLongs.toString().substring(1, setOfLongs.toString().length()-1));
    }
}
