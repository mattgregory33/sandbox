package org.mgregory.samples.java.dates;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 2/20/13
 * Time: 10:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class DateFormattingExamples {

    @Test
    public void fromStringToDate() throws ParseException {
        String dateStr = "3/20/2013";
        Date expected = new GregorianCalendar(2013, Calendar.MARCH, 20).getTime();

        assertDateParsedFromString(dateStr, expected, "MM/dd/yyyy");
        assertDateParsedFromString(dateStr, expected, "M/dd/yyyy");
        assertDateParsedFromString(dateStr, expected, "M/d/yyyy");
        assertDateParsedFromString(dateStr, expected, "M/d/yyy");
        assertDateParsedFromString(dateStr, expected, "M/d/yy");
        assertDateParsedFromString(dateStr, expected, "M/d/y");

        dateStr = "3/2/2013";
        expected = new GregorianCalendar(2013, Calendar.MARCH, 2).getTime();

        assertDateParsedFromString(dateStr, expected, "MM/dd/yyyy");
        assertDateParsedFromString(dateStr, expected, "M/dd/yyyy");
        assertDateParsedFromString(dateStr, expected, "M/d/yyyy");
        assertDateParsedFromString(dateStr, expected, "M/d/yyy");
        assertDateParsedFromString(dateStr, expected, "M/d/yy");
        assertDateParsedFromString(dateStr, expected, "M/d/y");

        dateStr = "3/2/13";
        expected = new GregorianCalendar(2013, Calendar.MARCH, 2).getTime();

        assertDateParsedFromString(dateStr, expected, "MM/dd/yy");
        assertDateParsedFromString(dateStr, expected, "M/dd/yy");
        assertDateParsedFromString(dateStr, expected, "M/d/yy");
    }

    private void assertDateParsedFromString(String dateStr, Date expected, String formatStr) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
        assertEquals(expected, formatter.parse(dateStr));
    }

    @Test
    public void fromDateToString() {
        Date date = new GregorianCalendar(2013, Calendar.APRIL, 15).getTime();

        assertStringFormattedFromDate(date, "4/15/2013", "M/dd/yyyy");
        assertStringFormattedFromDate(date, "4/15/2013", "M/d/yyyy");
        assertStringFormattedFromDate(date, "04/15/2013", "MM/dd/yyyy");

        date = new GregorianCalendar(2013, Calendar.APRIL, 1).getTime();
        assertStringFormattedFromDate(date, "4/1/2013", "M/d/yyyy");

    }

    private void assertStringFormattedFromDate(Date date, String expected, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        assertEquals(expected, formatter.format(date));
    }
}
