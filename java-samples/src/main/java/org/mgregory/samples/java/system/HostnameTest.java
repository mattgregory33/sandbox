package org.mgregory.samples.java.system;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 8/1/13
 * Time: 11:54 AM
 */
public class HostnameTest {

    @Test
    public void getSystemHostnameSample() throws UnknownHostException {
        System.out.println(InetAddress.getLocalHost().getHostName());
    }
}
