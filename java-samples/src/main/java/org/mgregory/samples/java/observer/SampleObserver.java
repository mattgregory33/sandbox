package org.mgregory.samples.java.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * User: matt
 * Date: 4/13/12
 * Time: 10:23 PM
 */
public class SampleObserver implements Observer {

    private SampleObservable observable;

    public SampleObserver(SampleObservable observable) {
        this.observable = observable;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (this.observable == observable) {
            System.out.println("I saw observable object change it's value to " + ((SampleObservable)observable).getValue());
        }
    }


}
