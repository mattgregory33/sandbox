package org.mgregory.samples.java.threadedobserver;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * User: matt
 * Date: 4/13/12
 * Time: 10:56 PM
 */
public class ThreadedObserverSampleMain implements Observer {

    public static void main(String[] args) {
        ThreadedObserverSampleMain main = new ThreadedObserverSampleMain();
        try {
            main.run();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void run() throws InterruptedException {
        SampleObservableRunnable observable = new SampleObservableRunnable();
        observable.addObserver(this);

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(observable, 0, 2, SECONDS);

        while(true) {
            System.out.println("Hello from the main thread!");
            Thread.sleep(1000);
        }
    }

    @Override
    public void update(Observable observable, Object o) {
//        if (o instanceof SampleObservableRunnable && ((SampleObservableRunnable)o).isShouldStopProcess()) {
        if (observable instanceof SampleObservableRunnable && ((SampleObservableRunnable)observable).isShouldStopProcess()) {
            System.out.println("Shut down threads and stop process here");
            Runtime.getRuntime().exit(0);
        }
    }
}
