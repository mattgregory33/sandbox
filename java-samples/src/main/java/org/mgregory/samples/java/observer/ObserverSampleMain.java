package org.mgregory.samples.java.observer;

/**
 * User: matt
 * Date: 4/13/12
 * Time: 10:15 PM
 */
public class ObserverSampleMain {

    public static void main(String[] args) {
        ObserverSampleMain main = new ObserverSampleMain();
        main.run();
    }

    public void run() {
        SampleObservable observable = new SampleObservable();
        SampleObserver observer = new SampleObserver(observable);
        observable.addObserver(observer);

        for (int i=0; i<10; i++) {
            observable.setValue("newvalue" + i);
        }
    }
}
