package org.mgregory.samples.java.inheritance.abstractclasses;

public class ConcreteChildThatOverridesGetClassName extends AbstractParent {

    @Override
    public String getClassName() {
        return this.getClass().getName();
    }

    @Override
    public String sayHelloFrom() {
        return "ConcreteChildThatOverridesGetClassName says Hello from " + getClassName();
    }
    
}
