package org.mgregory.samples.java.operators;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 2/1/13
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class NegationOperatorTest {

    @Test
    public void canNegateIntegerValueOf() {
        assertEquals((int)1, (int)Integer.valueOf("1"));
        assertEquals(-1, -Integer.valueOf("1"));
    }
}
