package org.mgregory.samples.java.inheritance;

import com.sun.org.apache.bcel.internal.classfile.ConstantDouble;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 11/14/12
 * Time: 2:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParentClass {

    private static final int CONSTANT = 1337;

    private int value = CONSTANT;

    protected int getValue() {
        return (value > 0 ? value : CONSTANT);
    }
}
