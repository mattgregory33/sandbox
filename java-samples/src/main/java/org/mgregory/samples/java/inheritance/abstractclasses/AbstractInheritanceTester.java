package org.mgregory.samples.java.inheritance.abstractclasses;

import org.junit.Test;

public class AbstractInheritanceTester {

    public static void main(String[] args) {
        ConcreteChild cc = new ConcreteChild();
        System.out.println(cc.sayHelloFrom());
        
        ConcreteChildThatOverridesGetClassName ccto = new ConcreteChildThatOverridesGetClassName();
        System.out.println(ccto.sayHelloFrom());
    }
}
