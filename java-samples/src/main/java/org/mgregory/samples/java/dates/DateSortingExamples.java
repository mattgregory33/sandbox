package org.mgregory.samples.java.dates;

import java.util.*;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/26/12
 */
public class DateSortingExamples {

    public static void main(String[] args) {

        System.out.println("Trying to sort a list...");

        // An unsorted list
        List<Date> unsorted = getUnsortedList();

        System.out.println("Unsorted:\n" + unsorted);

        // sort the list
        Collections.sort(unsorted);

        System.out.println("Sorted:\n" + unsorted);

        System.out.println("\n\n");

        // we can't sort an unmodifiable list
        List<Date> unmodifiable = Collections.unmodifiableList(getUnsortedList());
        try {
            System.out.println("Trying to sort an unmodifiable list...");
            Collections.sort(unmodifiable);
        } catch (UnsupportedOperationException e) {
            System.out.println("An exception was thrown because we tried to sort an unmodifiable list, as expected.");
        }

        System.out.println("\n\n");

        // we have to copy the unmodifiable list first, then sort that
        System.out.println("Copying the unmodifiable list into a new list, then sorting...");
        List<Date> modifiable = new ArrayList<Date>(unmodifiable);
        Collections.sort(modifiable);
        System.out.println("Sorted:\n" + modifiable);
    }

    private static List<Date> getUnsortedList() {
        List<Date> unsorted = new ArrayList<Date>(12);
        unsorted.add(new GregorianCalendar(2012, 12, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 11, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 10, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 9, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 8, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 7, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 6, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 5, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 4, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 3, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 2, 1).getTime());
        unsorted.add(new GregorianCalendar(2012, 1, 1).getTime());
        return unsorted;
    }
}
