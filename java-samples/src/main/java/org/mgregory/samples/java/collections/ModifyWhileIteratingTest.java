package org.mgregory.samples.java.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 3/20/13
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class ModifyWhileIteratingTest {

    public static void main(String[] args) {

        List<String> items = new ArrayList<String>();
        items.add("a");
        items.add("b");
        items.add("c");
        items.add("d");
        items.add("e");

        List<String> items2 = new ArrayList<String>();
        items2.add("1");
        items2.add("2");
        items2.add("3");
        items2.add("4");
        items2.add("5");

        for (String item: items) {
            System.out.println(item);
            items = items2;
            for (String item2: items) {
                System.out.println(item2);
            }
        }
    }
}
