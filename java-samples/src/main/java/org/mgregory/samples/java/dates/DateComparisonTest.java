package org.mgregory.samples.java.dates;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static java.util.Calendar.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.number.OrderingComparisons.greaterThan;
import static org.hamcrest.number.OrderingComparisons.lessThan;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/24/12
 */
public class DateComparisonTest {

    @Test
    public void testChronologicalComparisonMonthGranularity() {
        Calendar first = new GregorianCalendar(2012, JANUARY, 1);
        Calendar middle = new GregorianCalendar(2012, JUNE, 1);
        Calendar last = new GregorianCalendar(2012, DECEMBER, 1);
        Calendar duplicateLast = new GregorianCalendar(2012, DECEMBER, 1);

        assertThat(first, allOf(lessThan(middle), lessThan(last)));
        assertThat(middle, allOf(greaterThan(first), lessThan(last)));
        assertThat(last, allOf(greaterThan(first), greaterThan(middle)));
        assertThat(last, allOf(not(greaterThan(duplicateLast)), not(lessThan(duplicateLast))));

        assertThat(first.getTime(), allOf(lessThan(middle.getTime()), lessThan(last.getTime())));
        assertThat(middle.getTime(), allOf(greaterThan(first.getTime()), lessThan(last.getTime())));
        assertThat(last.getTime(), allOf(greaterThan(first.getTime()), greaterThan(middle.getTime())));
        assertThat(last.getTime(), allOf(not(greaterThan(duplicateLast.getTime())), not(lessThan(duplicateLast.getTime()))));

        // we can compare Calendars with .before() and .after()
        assertTrue(first.before(middle));
        assertTrue(first.before(last));
        assertTrue(middle.after(first));
        assertTrue(middle.before(last));
        assertTrue(last.after(first));
        assertTrue(last.after(middle));
        assertFalse(duplicateLast.before(last));
        assertFalse(duplicateLast.after(last));
        assertTrue(duplicateLast.equals(last));

        // we can compare Dates with .before() and .after()
        assertTrue(first.getTime().before(middle.getTime()));
        assertTrue(first.getTime().before(last.getTime()));
        assertTrue(middle.getTime().after(first.getTime()));
        assertTrue(middle.getTime().before(last.getTime()));
        assertTrue(last.getTime().after(first.getTime()));
        assertTrue(last.getTime().after(middle.getTime()));
        assertFalse(duplicateLast.getTime().before(last.getTime()));
        assertFalse(duplicateLast.getTime().after(last.getTime()));
        assertTrue(duplicateLast.getTime().equals(last.getTime()));
    }
}
