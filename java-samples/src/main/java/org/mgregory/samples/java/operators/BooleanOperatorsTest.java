package org.mgregory.samples.java.operators;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 12/17/12
 * Time: 8:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class BooleanOperatorsTest {

    @Test
    public void testDoubleAnd() {
        assertThat(false && false, is(false));
        assertThat(false && true, is(false));
        assertThat(true && false, is(false));
        assertThat(true && true, is(true));
    }

    @Test
    public void testDoubleOr() {
        assertThat(false || false, is(false));
        assertThat(false || true, is(true));
        assertThat(true || false, is(true));
        assertThat(true || true, is(true));
    }

    @Test
    public void testSingleAnd() {
        assertThat(false & false, is(false));
        assertThat(false & true, is(false));
        assertThat(true & false, is(false));
        assertThat(true & true, is(true));
    }

    @Test
    public void testSingleOr() {
        assertThat(false | false, is(false));
        assertThat(false | true, is(true));
        assertThat(true | false, is(true));
        assertThat(true | true, is(true));
    }

    @Test
    public void testXOr() {
        assertThat(false ^ false, is(false));
        assertThat(false ^ true, is(true));
        assertThat(true ^ false, is(true));
        assertThat(true ^ true, is(false));
    }
}
