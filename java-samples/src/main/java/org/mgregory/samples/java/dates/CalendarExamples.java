package org.mgregory.samples.java.dates;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/26/12
 */
public class CalendarExamples {

    @Test
    public void defaultCalendar() {
        Calendar defaultCal = Calendar.getInstance();
        System.out.println("Default calendar: " + defaultCal);
        System.out.println("Default calendar time: " + defaultCal.getTime());
        System.out.println("Calendar.YEAR=" + defaultCal.get(Calendar.YEAR));
        System.out.println("Calendar.MONTH=" + defaultCal.get(Calendar.MONTH));
        System.out.println("Calendar.DAY_OF_MONTH=" + defaultCal.get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void dateOnlyConstructor() {
        Calendar cal = new GregorianCalendar(2012, Calendar.DECEMBER, 12);
        System.out.println("new GregorianCalendar(2012, Calendar.DECEMBER, 12) ->" + cal.getTime());
    }

    @Test
    public void dateTimeConstructor() {
        Calendar cal = new GregorianCalendar(2012, Calendar.DECEMBER, 12, 0, 0, 0);
        System.out.println("new GregorianCalendar(2012, Calendar.DECEMBER, 12, 0, 0, 0) -> " + cal.getTime());

        Calendar cal2 = new GregorianCalendar(2012, Calendar.DECEMBER, 12, 13, 0, 0);
        System.out.println("new GregorianCalendar(2012, Calendar.DECEMBER, 12, 13, 0, 0) -> " + cal2.getTime());

        Calendar cal3 = new GregorianCalendar(2012, Calendar.DECEMBER, 12, 0, 12, 0);
        System.out.println("new GregorianCalendar(2012, Calendar.DECEMBER, 12, 0, 12, 0) -> " + cal3.getTime());

        Calendar cal4 = new GregorianCalendar(2012, Calendar.DECEMBER, 12, 0, 0, 12);
        System.out.println("new GregorianCalendar(2012, Calendar.DECEMBER, 12, 0, 0, 12) -> " + cal4.getTime());
    }

    @Test
    public void invalidDates() {
        Calendar cal = new GregorianCalendar(2012, Calendar.FEBRUARY, 31);
        System.out.println("new GregorianCalendar(2012, Calendar.FEBRUARY, 31) -> " + cal.getTime());

        Calendar cal2 = new GregorianCalendar();
        cal2.set(Calendar.YEAR, 2012);
        cal2.set(Calendar.MONTH, Calendar.FEBRUARY);
        System.out.println("getActualMaximum(Calendar.DAY_OF_MONTH) for Feb 2012 = " + cal2.getActualMaximum(Calendar.DAY_OF_MONTH));
    }
}
