package org.mgregory.samples.java.dates;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static java.util.Calendar.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/24/12
 */
public class CalendarArithmeticTest {

    private Calendar actual;

    @Before
    public void setUp() {
        actual = new GregorianCalendar(2012, APRIL, 15, 0, 0, 0);
    }

    @Test
    public void testAdd1Hour() {
        actual.add(HOUR_OF_DAY, 1);
        GregorianCalendar expected = new GregorianCalendar(2012, APRIL, 15, 1, 0, 0);
        assertThat(expected, equalTo(actual));
        assertThat(expected.getTime(), equalTo(actual.getTime()));
    }

    @Test
    public void testAdd24Hours() {
        actual.add(HOUR_OF_DAY, 24);
        GregorianCalendar expected = new GregorianCalendar(2012, APRIL, 16, 0, 0, 0);
        assertThat(expected, equalTo(actual));
        assertThat(expected.getTime(), equalTo(actual.getTime()));
    }

    @Test
    public void testAdd25Hours() {
        actual.add(HOUR_OF_DAY, 25);
        GregorianCalendar expected = new GregorianCalendar(2012, APRIL, 16, 1, 0, 0);
        assertThat(expected, equalTo(actual));
        assertThat(expected.getTime(), equalTo(actual.getTime()));
    }

}
