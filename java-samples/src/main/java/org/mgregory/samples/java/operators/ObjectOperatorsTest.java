package org.mgregory.samples.java.operators;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.core.Is.is;

public class ObjectOperatorsTest {

    @Test
    public void instanceofHandlesNulls() {
        assertThat(null instanceof Object, is(false));

    }
}
