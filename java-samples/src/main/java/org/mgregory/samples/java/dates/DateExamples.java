package org.mgregory.samples.java.dates;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 2/13/13
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class DateExamples {

    @Test
    public void testDateConstructionFromString() {
        try {
            Date date = (Date)getDate();
            fail("somehow no exception was thrown");
        } catch (Throwable t) {
            assertTrue(t instanceof ClassCastException);
        }
    }

    private Object getDate() {
        return new Date().toString();
    }
}
