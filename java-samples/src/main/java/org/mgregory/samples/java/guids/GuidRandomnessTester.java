package org.mgregory.samples.java.guids;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: mgregory
 * Date: 11/27/12
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class GuidRandomnessTester {

    @Test
    public void testGuidRandomness() {
        int numTests = 500000;
        Set<String> setOfGuids = new HashSet<String>(numTests);
        assertEquals(0, setOfGuids.size());
        for (int i=0; i<numTests; i++) {
            setOfGuids.add(UUID.randomUUID().toString());
        }

        assertEquals(numTests, setOfGuids.size());
    }
}
