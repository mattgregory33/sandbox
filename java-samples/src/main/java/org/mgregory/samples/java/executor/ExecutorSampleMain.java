package org.mgregory.samples.java.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * User: matt
 * Date: 4/13/12
 * Time: 10:42 PM
 */
public class ExecutorSampleMain {

    public static void main(String[] args) {
        ExecutorSampleMain main = new ExecutorSampleMain();
        try {
            main.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new SampleRunnable(), 0, 2, SECONDS);

        for (int i=0; i<10; i++) {
            System.out.println("Hello from the main thread!");
            Thread.sleep(1000);
        }
    }
}
