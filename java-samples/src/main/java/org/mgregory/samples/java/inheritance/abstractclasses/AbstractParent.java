package org.mgregory.samples.java.inheritance.abstractclasses;

public abstract class AbstractParent {
    
    public String getClassName() {
        return this.getClass().getName();
    }
    
    public abstract String sayHelloFrom();
}
