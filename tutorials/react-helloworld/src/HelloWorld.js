import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Person from './Person.js';

class HelloWorld extends Component {

	constructor(props) {
		super(props)
		this.state = {value: 1}

		this._addByOne = this._addByOne.bind(this)
	}

	componentDidMount() {

	}

	_addByOne() {
		this.setState({
			value: this.state.value + 1
		})	
	}

	render() {
		var greeting = (this.props.isPerson) ? <Person name={this.props.name}/> : "World"

		// if (this.props.isPerson) {
			// greeting = <Person name={this.props.name}/>
		// }

		return (
			<div>
				<h1>Hello, {greeting}!</h1>
				{this.state.value}
				<button onClick={this._addByOne}>Click Me</button>
			</div>
		);
	}

}

HelloWorld.propTypes = {
	name: PropTypes.string.isRequired
};


// HelloWorld.defaultProps = {name: 'Default'};

export default HelloWorld;