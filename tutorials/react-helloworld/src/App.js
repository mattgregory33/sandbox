import React, { Component } from 'react';
import logo from './logo.svg';
import HelloWorld from './HelloWorld.js';
import './App.css';


class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="App-intro">
          <HelloWorld name="joe" isPerson={true} />
        </div>
      </div>
    );
  }
}

export default App;
