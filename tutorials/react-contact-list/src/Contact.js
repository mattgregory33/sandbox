import React from 'react';

const Contact = ({contact, item}) =>
	<ul>
		<li>{contact.name} {contact.phone}</li> {item}
	</ul>

export default Contact;
