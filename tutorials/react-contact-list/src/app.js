import React from 'react';
import {render} from 'react-dom';
import ContactsList from './ContactsList';

let contacts = [{
	id: 1,
	name: 'Matt',
	phone: '555-555-5555'
},{
	id: 2,
	name: 'Ashley',
	phone: '333-333-3333'
},{
	id: 3,
	name: 'Harrison',
	phone: '777-777-7777'
},{
	id: 4,
	name: 'Evelyn',
	phone: '444-444-4444'
}]

class App extends React.Component {

	render() {
		return (
			<div>
				<h1>Contact List</h1>
				<ContactsList contacts={this.props.contacts} />
			</div>
		)
	}
}

render(<App contacts={contacts}/>, document.getElementById('app'));