class Calculator(val lhs: Int, val rhs: Int) {

  def add(): String = {
    "%s".format(rhs+lhs)
  }
}

val c1 = new Calculator(1, 2)
c1.add()



class FriendlyCalculator(lhs: Int, rhs: Int) extends Calculator(lhs, rhs) {

  override def add: String = {
    "The total is: %s".format(super.add)
  }
}

val c2 = new FriendlyCalculator(3, 4)
c2.add()