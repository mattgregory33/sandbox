trait Counter {
  // protected available to subclasses but not package
  protected var count = 0 // fields on traits can specify a default value
  protected var startAt: Int // fields on traits can require implementations to override
  def increment()
  override def toString: String = "%d".format(count);
}


  class Foo {
  val foo = new IncrementByOne()
  //foo.count // error, cannot access count
}

class IncrementByOne extends Counter {
  override var startAt: Int = 0 // must be specified
  override def increment(): Unit = {
    if (count < startAt) count = startAt
    count += 1
  }
}

class IncrementExponentially(rate: Int) extends Counter {
  override var startAt: Int = 1 // must be specified
  override def increment(): Unit = {
    if (count < startAt) count = startAt
    if (count == 0) count = 1 else count *= rate
  }
}


object Tester {
  def main(args: Array[String]): Unit = {
    val i1 = new IncrementByOne
    for (i <- 1 to 10) {
      i1.increment()
      println(i1)
    }

    val i2 = new IncrementExponentially(2)
    for (i <- 1 to 10) {
      i2.increment()
      println(i2)
    }
  }
}