// new Customer class w/ name and address
// "primary constructor"
// val tells scala to create a field for the param w/ a public getter
// var tells scala to create a field for the param w/ public getter, setter
// no val/var means value can only be specified vie constructor
class Customer(val name: String, val address: String) {
  // creates a public settable field accessible by 'id =' or 'id_='
  var id = ""

  // creates a private  field
  private var prvt = 0

  // cannot override prvt setter to expose publicly this way - scala will also generate this
  // signature and the names will collide
//  def prvt_=(value: Int): Unit = {
//    if (this.prvt < 1)
//      this.prvt = value
//  }

  // this is the way to create a private field with public accessors
  private var _goodPrivate = ""

  // public accessor methods wraps private _goodPrivate var
  def goodPrivate = _goodPrivate
  def goodPrivate_=(value: String): Unit = {
    if (_goodPrivate.isEmpty)
      _goodPrivate = value
  }
}
// compare to java w/ definition for class, constructor, fields

// instantiate Customer
val customer1 = new Customer("Joe", "123 Main St")
customer1.id = "1"
customer1.id_=("2")
println(customer1.id)
//customer1.prvt = 1 // compile error, prvt is private

customer1.goodPrivate = "set it"
println("before: " + customer1.goodPrivate)
customer1.goodPrivate = "set it again"
println("after: " + customer1.goodPrivate)

// compiling above and decompiling w/ java yields standard java code