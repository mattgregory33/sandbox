val langVal: String = "Scala";
// val creates an immutable variable
//langVal = "Java"; // error: reassignment of val


var langVar: String = "Java";
// var is mutable
langVar = "Scala";
// don't need semicolon
langVar = "Python"


val age = 35;
// operator precedence same as in Java
var maxHeartRate = 210 - age * .5;



// FUNCTIONS
def min(x: Int, y: Int): Int = {
  if (x < y)
    return x
  else
    return y
}

min (0, 1)
min (1, 0)
min (99, 98)
min (352, 2)

// functions will return last value, so return not necessary
def max(x: Int, y: Int): Int = {
  if (x > y)
    x
  else
    y
}

max (0, 1)
max (1, 0)
max (99, 98)
max (6, 3236)

// return type is inferred so can be dropped from signature
// equal sign tells scala the function returns something so the function could be written on one line
def eq(x: Int, y: Int) = if (x == y) true else false
eq(0, 1)
eq(1, 1)
eq(323, 536)



// OPERATOR OVERLOADING
// operators are just methods

age
// equivalent
age * .5
age.*(.5)

// equivalent
35.toString()
35 toString



// COLLECTIONS
val list = List("a", "b", "c")

//equivalent
list.foreach(value => println(value))
list.foreach(println)

for (value <- list) println(value)
for (value <- list.reverse) println(value)



// JAVA INTEROPERABILITY
// [] brackets instead of <> for generics
val jList = new java.util.ArrayList[String]
jList.add("hello")
jList.add("world")
jList

// scala has no primitives, no int, long, float, etc
val total = BigDecimal(10) + BigDecimal(20)



// CLASS HEIRARCHY
// Any is the highest level (abstract) scala class, everything extends it
// two subclasses: abstract AnyVal, concrete AnyRef

// value classes extend AnyVal (int, float, long, etc)
// Unit value type is similar to void
// scala value classes implement == like Java .equals
42 == 42 // same as java new Integer(42).equals(new Integer(42));

// reference classes extend AnyRef
// AnyRef is an alias for java.lang.Object
// AnyRef objects can use eq method to test reference equality like Java ==
new String("a") == new String("a") //true
new String("a") eq new String("a") //false


// scala.Null extends all AnyRef types
// scala.Nothing extends all AnyVal and AnyRef types, including Nothing

def printAny(value: Any) = println(value)

def printAnyVal(value: AnyVal) = println(value)

def printAnyRef(value: AnyRef) = println(value)

val aVal = 5
val aRef = List(1, 2, 3)

printAny(aVal)
printAny(aRef)
printAnyVal(aVal)
//printAnyRef(aVal) // runtime error type mismatch, aVal is an Int not an AnyVal
//printAnyVal(aRef) // compile error, aRef not an AnyVal
printAnyRef(aRef)


// METHOD ARGS
// supports varargs
// can name arguments
// can default value arguments
// can specify lambdas as arguments

//ex: named args, default values
def printArgs(first: Int, second: Int = 1): Unit = {
    println("first=" + first)
    println("second=" + second)
}

printArgs(7)
printArgs(1, 2)
printArgs(second=6, first=3);
printArgs(first=99);

//ex: lambdas as args
def test(f: () => Boolean): Boolean = {
    f()
}

test(() => if (true) true else false)
test(() => if (false) true else false)