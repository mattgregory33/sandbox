
object NonogramSolver extends App {

  def mkCombos(size: Int): List[String] = {
    (0 until Math.pow(2, size).toInt).toList map(i => s"%${size}s".format(i.toBinaryString).replaceAll(" ", "0"))
  }

  def mkRegex(args: Array[String]): String = {
    args map (arg => s"1{$arg}") mkString ("0*", "0+", "0*")
  }

  def findMatches(combos: List[String], regex: String): List[String] = combos.filter(c => c.matches(regex))

  def computeResult(matches: List[String]): List[Boolean] = (matches map (_.toList)).transpose map (_.mkString) map (_.matches("1*"))
  
  computeResult(findMatches(mkCombos(args.head.toInt), mkRegex(args.tail))).zipWithIndex.foreach(i => println(s"${i._2+1}: ${i._1}"))
}
