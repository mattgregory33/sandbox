// ordered
// elements can be different types
// do not implement Iterable

val personInfo = ("Homer", "Simpson", 40, "M")

// arrow creates simple key-value pair
val personGenderPair = "Homer" -> "M"
val genderPersonPair = "F" -> "Marge"


// iterating
personInfo.productIterator.foreach(i => println("Value = " + i))


// assigning vals from tuple
val (first, last, age, gender) = personInfo

// alternative,
val first2 = personInfo._1
val last2 = personInfo._2


// passing tuple elements into function
// scala type inference cannot automatically accomplish this for us
def printPersonInfo(name: String, gender: String) = println(s"Name:$name|Gender:$gender")

// calling this function is painful, tupled applies params to printPersonInfo in order
(printPersonInfo _).tupled(personGenderPair)
(printPersonInfo _).tupled(genderPersonPair)


// to get the number of elements in a tuple:
personInfo.productArity
