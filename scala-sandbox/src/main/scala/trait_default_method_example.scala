import scala.collection.mutable

// Traits are like java interfaces

class Customer(val name: String, val address: String)  extends Ordered[Customer] {
  override def compare(that: Customer): Int = name.compare(that.name)
  override def toString: String = "%s".format(name)
}

// [] define generics like java <>
trait Sortable[A <: Ordered[A]] extends Iterable[A] {
  // sort method definition with default implementation
  def sort: Seq[A] = {
    this.toList.sorted
  }
}

class Customers extends Sortable[Customer] {
  private val customers = mutable.Set[Customer]()

  def add(customer: Customer) = customers.add(customer)

  // implemented from Sortable
  def iterator: Iterator[Customer] = customers.iterator
}

// demonstrates Customers are sorted by name
object Customers {
  def main(args: Array[String]): Unit = {
    val customers = new Customers()
    customers.add(new Customer("Zoe Zoerson", "Zimbabwe"))
    customers.add(new Customer("Abe Abington", "Appleton"))
    customers.add(new Customer("Dave Davis", "Denver"))
    customers.add(new Customer("William Washington", "Weaselton"))
    println(customers.sort)
  }
}


class CustomersSortableByAddress extends Customers {
  // overrides Sortables default sort implementation (via Customers) to use address in comparison
  override def sort: Seq[Customer] = {
    this.toList.sorted(new Ordering[Customer] {
      def compare(a: Customer, b: Customer) = a.address.compare(b.address)
    })
    // IDE suggests writing this as:
    // this.toList.sorted((a: Customer, b: Customer) => a.address.compare(b.address))

  }
}

// demonstrates CustomersSortableByAddress are sorted by address
object CustomersByAddress {
  def main(args: Array[String]): Unit = {
    val customers = new CustomersSortableByAddress()
    customers.add(new Customer("Frank Frink", "France"))
    customers.add(new Customer("Ted Tanner", "Texas"))
    customers.add(new Customer("Bob Barker", "Bloomington"))
    customers.add(new Customer("Martha Mabry", "Mayberry"))
    println(customers.sort)
  }
}