// String interpolation can access vals/vars by name to format strings
val name = "Matt"
var greeting = "Hello"

s"$greeting $name, this is an interpolated string"

s"we can also apply expressions: ${name*2}"

