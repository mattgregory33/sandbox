// use [] instead of <>
// empty <> is inferred (i.e. no need to new ArrayList<>())

val names: List[String] = List()


// method params work just like java
trait Stack[T] {
  def push(t: T)
  def pop: T
}

class ListStack[T]() extends Stack[T] {
  private var elements: List[T] = List()

  override def push(t: T): Unit = {
    elements = t +: elements // List is immutable so we return new list with t prepended
  }

  override def pop: T = {
    if (elements.isEmpty)
      throw new IndexOutOfBoundsException
    val head = elements.head
    elements = elements.tail // reset elements to remainder of elements minus head
    head
  }
}

val stack = new ListStack[String]
stack.push("a")
stack.push("b")
stack.push("c")
//stack.push(7) // compile error, 7 not a string
stack.pop
stack.pop




class Animal extends Comparable[Animal] {
  override def compareTo(o: Animal): Int = 0
}
class Lion extends Animal
class Zebra extends Animal

// A has upper bound of animal
// also specify lower bound with <: or both
def sort[A <: Animal ](list: List[A]) = {}

var enclosure = List[Lion]()
enclosure = new Lion +: enclosure
enclosure = new Lion +: enclosure
sort(enclosure)

var zoo = List[Animal]()
zoo = new Zebra +: zoo
zoo = new Lion +: zoo
zoo = new Lion +: zoo
sort(zoo)