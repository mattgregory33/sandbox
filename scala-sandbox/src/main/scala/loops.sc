var i: Int = 0

// do and while are the same as java
do {
  println(i)
  i += 1 // (no ++ in scala)
} while (i < 10)

i = 0
while (i < 10) {
  println(i)
  i += 1
}


// for loops are different, they are "for comprehensions"
// no for (i=0; i<n; i++) {} in scala

// <- indicates what follows is a generator, anything that can generate an iterable can be used
for (i <- 0 to 9) {
  println(i)
}

// equivalent to: (to is a method on Int returning an iterable up to given end)
for (i <- 0.to(9)) {
  println(i)
}

// for loop can yield results into a new val:
val doubled = for(num <- 0 to 9) yield {
  num * 2
}

val range = 10 until 20
// until will stop at size - 1 automatically
for (num <- 0 until range.size) {
  println(range(num))
}

// indices returns a range of 0 to length-1
for (num <- range.indices) {
  println(range(num))
}

// pattern guards can be applied in the for loop
// print range values with even indices
for (num <- range.indices if num % 2 == 0) {
  println(range(num))
}

// also foreach, which takes a lambda
(0 to 9).foreach(i => println(i))
(0 to 9).foreach(println(_)) //shorthand


// no break or continue to short circuit loops built in
//  can use scala.util.control.Breaks and breakable{ break() } to