import com.mgregory.sandbox.scala.books.fpinscala.laziness._

Stream(1,2,3).toList

Stream(1,2,3,4,5).take(3).toList
Stream(1,2,3,4,5).take(0).toList
Stream(1,2,3,4,5).take(5).toList

Stream(1,2,3,4,5).drop(3).toList
Stream(1,2,3,4,5).drop(0).toList
Stream(1,2,3,4,5).drop(5).toList

Stream(1,2,3,4,5).takeWhile(_ % 2 == 0).toList
Stream(1,2,3,4,5).takeWhile(_ % 2 != 0).toList
Stream(1,2,3,4,5).takeWhile(_ <= 5).toList
Stream(1,2,3,4,5).takeWhile(_ >= 0).toList
Stream(1,2,3,4,5).takeWhile(_ < 3).toList
