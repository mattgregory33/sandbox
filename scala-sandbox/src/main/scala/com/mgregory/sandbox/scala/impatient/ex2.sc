// signum function returns 1, 0, or -1 depending if the input is positive, zero, or negative
def signum(num: Int) = if (num > 0) 1 else if (num == 0) 0 else -1

signum(1)
signum(0)
signum(-1)



// an empty block expression type is Unit, with a value of ()
{}


// x = y = 1 could be helpful for delaying assignment of y based on a call to x, assuming x is a def'd function
var y = 0
y // 0
def x = y = 1
y // 0
x // Unit
y // 1 now after calling x



for (i <- 10 to 0 by -1) println(i)


// procedure (note no = before body block)
def countdown(from: Int) {
  for (i <- from to 0 by -1) println(i)
}

countdown(7)
countdown(65)


// for loop to calculate product of unicode values of "Hello"
// note: toInt is not needed on i
var product: Long = 1
for (i <- "Hello") {
  product *= i
}
println(product)


// same as above for loop without the loop
"Hello".foldLeft(1:Long)(_ * _)


// a function to compute product from previous two examples
def product(s: String): Long = {
  s.foldLeft(1:Long)(_ * _)
}
product("Hello")
product("scala")


// same as above, using recursive function
def productR(s: String): Long = {
  if (s.length == 0) 1 else s.head * productR(s.tail)
}
productR("Hello")
productR("scala")



// computes x raised to n power recursively
def powerR(x: Int, n: Int): BigInt = {
  if (n == 0) 1 else x * powerR(x, n-1)
}
powerR(2,2)
powerR(2,3)
powerR(5,7)
