package com.mgregory.sandbox.scala.essential.recursive_datatypes

/**
  * model JSON
  * - handle arrays = value sequence
  * - handle objects = key/value pair sequence
  * - value types: string, int, double, object, array
  *
  * - valid top level: value, array, object
  *
  * [ "a string" , 1.0 , true ]
  * {
  * "a" : [ 1 , 2 , 3 ] ,
  * "b" : [ "a" , "b" , "c" ],
  * "c" : { "doh" : true , "ray" : false , "me" : 1 }
  * }
  */

sealed trait JsonValue {
  override def toString: String = this match {
    case JsonString(value) => s"""\"$value\""""
    case JsonInt(value) => s"$value"
    case JsonDouble(value) => f"$value"
    case JsonBool(value) => s"$value"
    case JsonArray(value) => s"""[${value.mkString(",")}]"""
    case JsonObject(value) => s"""{${value map {case (k,v) => s"""\"$k":$v"""} mkString(",")}}"""
  }
}

case class JsonString(value: String) extends JsonValue

case class JsonDouble(value: Double) extends JsonValue

case class JsonInt(value: Int) extends JsonValue

case class JsonBool(value: Boolean) extends JsonValue

case class JsonArray(value: Seq[JsonValue]) extends JsonValue

case class JsonObject(value: Map[String, JsonValue]) extends JsonValue