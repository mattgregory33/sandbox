import com.mgregory.sandbox.scala.essential.{Cat, ChipShop}


val oswald = new Cat("Oswald", "Black", "Milk")
val henderson = new Cat("Henderson", "Ginger", "Chips")
val quentin = new Cat("Quentin", "Tabby and White", "Curry")

oswald.name
henderson.color
quentin.food

ChipShop.willServe(oswald)
ChipShop.willServe(henderson)
ChipShop.willServe(quentin)

