import com.mgregory.sandbox.scala.essential.Person

// Person instantiation via Person class constructor
val homer = Person("Homer", "Simpson")
homer.firstName
homer.lastName

// Person instantiation via Person object apply method
val marge = Person("Marge Simpson")
marge.firstName
marge.lastName

