package com.mgregory.sandbox.scala.essential.randomwords

class RandomText(val subjects: List[String]) {


  def generateText = {
    for {
      s <- subjects
      v <- verbsFor(s)
      o <- objectsFor(v)
    } yield {
      (s, v, o)
    }
  }

  private def objectsFor(v: String) = {
    val objects = v match {
      case "wrote" => List("the book", "the letter", "the code")
      case "chased" => List("the ball", "the dog", "the cat")
      case "slept on" => List("the bed", "the mat", "the train")
      case "meowed at" => List("Noel", "the door", "the food cupboard")
      case "barked at" => List("the postman", "the car", "the cat")
    }
    objects
  }

  private def verbsFor(s: String) = {
    val verbs = s match {
      case "Noel" => List("wrote", "chased", "slept on")
      case "The cat" => List("meowed at", "chased", "slept on")
      case "The dog" => List("barked at", "chased", "slept on")
    }
    verbs
  }
}
