import com.mgregory.sandbox.scala.books.fpinscala.List._
import com.mgregory.sandbox.scala.books.fpinscala._

// we can apply Cons just like any other function
foldRight(List(1,2,3), Nil:List[Int])((l, newConsList) => Cons(l,newConsList))
foldRight(List(1,2,3), Nil:List[Int])(Cons(_,_))

// foldRight can compute the length of the list
foldRight(List("a","b","c"), 0)((_, total) => total + 1)