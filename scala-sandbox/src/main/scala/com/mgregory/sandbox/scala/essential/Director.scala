package com.mgregory.sandbox.scala.essential

case class Director(firstName: String, lastName: String, yearOfBirth: Int) {

  def name: String = s"$firstName $lastName"

}

object Director {

  def older(director1: Director, director2: Director): Director =
    if (director1.yearOfBirth < director2.yearOfBirth) director1 else director2

}
