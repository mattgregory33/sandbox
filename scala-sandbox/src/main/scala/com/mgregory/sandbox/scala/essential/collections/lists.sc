import com.mgregory.sandbox.scala.essential.generics.types.{Empty, Full}
// there are immutable and mutable lists
// lists are singly-linked lists, terminated by Nil

// Cons operator '::' is used to construct lists
// must include Nil terminator
val consWeekdays = "Mon" :: "Tues" :: "Wed" :: "Thu" :: "Fri" :: Nil
val consWeekendDays = "Sat" :: "Sun" :: Nil

// List constructor can construct lists
val listWeekdays = List("Mon", "Tue", "Wed", "Thu", "Fri")
val listWeekendDays = List("Sat", "Sun")

// ':::' concatenates lists (only lists)
val concatDays = consWeekdays ::: listWeekendDays

// '++' also contcatenates lists but also other collections
val concatDays2 = listWeekdays ++ consWeekendDays


// List.flatten will flatten a List of Lists into a single list
val daysListList = List(listWeekdays, listWeekendDays)
val allDays = daysListList.flatten


// zip combines lists into tuples
val listAlldayIndexes = List(1,2,3,4,5,6,7)
listAlldayIndexes zip allDays
// zip will do as many as possible constrained by smaller side
listAlldayIndexes zip consWeekdays
listWeekendDays zip listAlldayIndexes


// list introspection:
allDays.head
allDays.tail // (end of list after head, not last element)
allDays.size
allDays.reverse
allDays(2)
allDays.contains("Mon")
allDays.contains("June")


// list traversal:
for (day <- allDays) println(day)

allDays forall (_ != "Thu") // false: all elements are not equal to "Thu"
allDays forall (_ != "Aug") // true: all elements are not equal to "Aug"

allDays endsWith listWeekendDays
allDays endsWith listWeekdays

allDays startsWith listWeekendDays
allDays startsWith listWeekdays

// given a list, use map to return a List of all elements and their negations
val list = List(1, 2, 3)
list flatMap {l => List(l, -l)}

// given a list, return a List[Maybe[Int]] containing None for the odds
val evensOdds = List(Full(3), Full(2), Full(1))
evensOdds map (e => if (e.value % 2 == 0) e else Empty())