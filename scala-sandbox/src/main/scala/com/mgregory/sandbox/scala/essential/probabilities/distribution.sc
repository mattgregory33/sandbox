import com.mgregory.sandbox.scala.essential.probabilities.Distribution

val d = Distribution.uniform(List(1,2,3))

d.map(i => i * 2)


sealed trait Coin
case object Heads extends Coin
case object Tails extends Coin

val fairCoin: Distribution[Coin] = Distribution.uniform(List(Heads, Tails))

val threeFlips = for {
  f1 <- fairCoin
  f2 <- fairCoin
  f3 <- fairCoin
} yield (f1, f2, f3)


//I put my food into the oven and after some time it ready to eat and produces
// delicious smell with probability 0.3 and otherwise it is still raw and
// produces no smell with probability 0.7. If there are delicious smells the
// cat comes to harass me with probability 0.8, and otherwise it stays asleep.
// If there is no smell the cat harasses me for the hell of it with probability
// 0.4 and otherwise stays asleep. Implement this model and answer the
// question: if the cat comes to harass me what is the probability my food is
// producing delicious smells (and therefore is ready to eat.)

sealed trait FoodSmell
case object DeliciousSmell extends FoodSmell
case object NoSmell extends FoodSmell

sealed trait CatAction
case object CatComes extends CatAction
case object CatStayAsleep extends CatAction

def catActionFor(smell: FoodSmell): Distribution[CatAction] = {
  smell match {
    case DeliciousSmell => Distribution.discrete(List((CatComes, 0.8), (CatStayAsleep, 0.2)))
    case NoSmell => Distribution.discrete(List((CatComes, 0.4), (CatStayAsleep, 0.6)))
  }
}

val foodModel = for {
  d1 <- Distribution.discrete(List((DeliciousSmell, 0.3), (NoSmell, 0.7)))
  d2 <- catActionFor(d1)
} yield (d1, d2)

val pHarassing: Double = foodModel.events filter { case ((_, CatComes), _) => true case ((_, CatStayAsleep), _) => false } map { case (a, p) => p } sum
val pCookedGivenHarassing: Option[Double] = foodModel.events collectFirst[Double] { case ((DeliciousSmell, CatComes), p) => p } map (_ / pHarassing)