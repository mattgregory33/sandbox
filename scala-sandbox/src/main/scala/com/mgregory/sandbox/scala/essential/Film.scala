package com.mgregory.sandbox.scala.essential

case class Film(name: String, yearOfRelease: Int, imdbRating: Double, director: Director) {

  def directorsAge: Int = yearOfRelease - director.yearOfBirth

  def isDirectedBy(director: Director): Boolean =
    if (this.director == director) true else false

}

object Film {

  def highestRating(film1: Film, film2: Film): Film =
    if (film1.imdbRating > film2.imdbRating) film1 else film2

  def oldestDirectorAtTheTime(film1: Film, film2: Film): Director =
    if (film1.directorsAge > film2.directorsAge) film1.director else film2.director

}
