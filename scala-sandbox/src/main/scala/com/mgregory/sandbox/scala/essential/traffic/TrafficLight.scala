package com.mgregory.sandbox.scala.essential.traffic

// This is an example of the sum-type pattern (is-a or)
sealed trait TrafficLight {
  // option 1: implement next() using polymorphism
  def polyNext: TrafficLight

  // option 2: implement next() using pattern matching within the trait
  def patternNextInternal: TrafficLight = this match {
    case Green => Yellow
    case Yellow => Red
    case Red => Green
  }

}

case object Green extends TrafficLight {
  override def polyNext: TrafficLight = Yellow
}

case object Yellow extends TrafficLight {
  override def polyNext: TrafficLight = Red
}

case object Red extends TrafficLight {
  override def polyNext: TrafficLight = Green
}


// option 3: implement next() using pattern matching external to trait
object TrafficCop {
  def patternNextExternal(current: TrafficLight): TrafficLight = current match {
    case Green => Yellow
    case Yellow => Red
    case Red => Green
  }
}