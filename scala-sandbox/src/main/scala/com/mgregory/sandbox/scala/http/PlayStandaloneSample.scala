package com.mgregory.sandbox.scala.http

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import play.api.libs.ws.WSAuthScheme.BASIC
import play.api.libs.ws.ahc.StandaloneAhcWSClient

import scala.util.{Failure, Success}

object PlayStandaloneSample extends App {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val client = StandaloneAhcWSClient()

  val req = client.url("http://www.google.com")

  // use .withAuth() to provide authentication
  req.get()
    .andThen { case _ => client.close() }
    .andThen { case _ => system.terminate() }
    .onComplete {
      case Success(r) => println(r.body)
      case Failure(e) => println(s"request failed: ${e.getMessage}")
    }

}
