package com.mgregory.sandbox.scala.play

import com.mgregory.sandbox.scala.essential.recursive_datatypes.JsonObject
import play.api.libs.json.{Json, OFormat}


class PlayJsonExamples {

}

case class MyObject(id: Int, name: String, collection: List[Labeled])
case class Labeled(id: Int, name: String, label: String)

object MyObject {
  implicit val format: OFormat[MyObject] = Json.format[MyObject]
}

object Labeled {
  implicit val format: OFormat[Labeled] = Json.format[Labeled]
}

object JsonTransformerExample extends App {

  val json = Json.parse(
    """{
       "id":0,
       "name":"object1",
       "collection":[
        {"id":0, "name":"sub0", "label":"SUB"},
        {"id":1, "name":"sub1", "label":"SUB"},
        {"id":2, "name":"sub2", "label":"SUB"}
       ]
       }
    """.stripMargin)

  println(s"Original JSON:\n${Json.prettyPrint(json)}")

  import play.api.libs.json._

  val collectionTransformer = __.read[JsArray].map {
    case JsArray(o) =>
      println(s"collection: $o")
      JsArray(o.map { o => o.as[JsObject] ++ Json.obj("label" -> "NEWLABEL") })
  }

  val jsonTransformer = (__ \ "collection").json.update(collectionTransformer)

  json.transform(jsonTransformer) match {
    case JsError(errors) => println(s"Error transforming JSON: $errors")
    case JsSuccess(value, _) => println(s"Transformed JSON:\n${Json.prettyPrint(value)}")
  }

}
