package com.mgregory.sandbox.scala.akka

class AkkaHttpSample {

}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer

import scala.concurrent.Await
import scala.concurrent.duration._


object Client {
  def main(args: Array[String]): Unit = {
    println(HttpRequester.getEmojiUrl("speaker"))
  }
}

object HttpRequester {
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  import spray.json.DefaultJsonProtocol._

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  def getEmojiUrl(url: String): String = {
    val result = for {
      r <- Http().singleRequest(HttpRequest(uri = "https://api.github.com/emojis"))
      m <- Unmarshal(r).to[Map[String, String]]
    } yield {
      m(url)
    }
    Await.result(result, 30 seconds)
  }

}