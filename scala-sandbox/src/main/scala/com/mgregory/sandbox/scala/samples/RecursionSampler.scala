package com.mgregory.sandbox.scala.samples

object RecursionSampler {

  case class Nested(values: List[Int], children: List[Nested])

  def allNestedValues(data: Nested): List[Int] = {

//    def recur(data: Nested, accum: List[Int]): List[Int] = {
//      data.children match {
//        case Nil => accum
//        case hd :: Nil => loop(hd, accum ++ hd.values)
//        case hd :: tl =>
//          loop(hd, accum ++ hd.values)
//          tl flatMap (t => loop(t, accum ++ t.values))
//      }
//    }

    def recur(data: Nested): List[Int] = {
      (data.children flatMap { c => recur(c) }) ++ data.values
    }

    recur(data).reverse
  }

}
