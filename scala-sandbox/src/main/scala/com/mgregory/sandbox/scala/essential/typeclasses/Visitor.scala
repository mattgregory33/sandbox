package com.mgregory.sandbox.scala.essential.typeclasses

import java.util.Date

sealed trait Visitor {
  def id: String
  def createdAt: Date
  def age: Long = new Date().getTime() - createdAt.getTime()
}

final case class Anonymous (id: String, createdAt: Date = new Date() ) extends Visitor

final case class User (id: String, email: String, createdAt: Date = new Date() ) extends Visitor

object VisitorImplicits {
  implicit object AnonymousWriter extends JsWriter[Anonymous] {
    override def write(value: Anonymous): JsValue = {
      JsObject(Map("id" -> JsString(value.id), "createdAt" -> JsString(value.createdAt.toString)))
    }
  }

  implicit object UserWriter extends JsWriter[User] {
    override def write(value: User): JsValue = {
      JsObject(Map("id" -> JsString(value.id), "email" -> JsString(value.email), "createdAt" -> JsString(value.createdAt.toString)))
    }
  }
}