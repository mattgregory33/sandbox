case class Person(firstName: String, lastName: String, age: Int)

val people = List(
  Person("Aaron", "Aaronson", 59),
  Person("Xavier", "Thomas", 23),
  Person("Homer", "Simpson", 39),
  Person("Marge", "Simpson", 38),
  Person("Abraham", "Lincoln", 209),
  Person("Biff", "Tanner", 23)
)

people.sortBy(_.firstName)

people.sortBy(_.lastName)

people.sortBy(_.age)

people.sortBy(p => (p.age, p.lastName))

people.sortBy(_.age).sortBy(_.lastName)

