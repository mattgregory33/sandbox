import scala.collection.mutable.ArrayBuffer
import scala.util.Random

// create an array of n random integers ranging 0 to n
val a = new Array[Int](10)
for (i <- 0 until 10) a(i) = Random.nextInt(10)
a


// swap adjacent elements in an array
val toSwap = Array.range(0, 11)
for (i <- toSwap.indices by 2 if i+1 < toSwap.length) {
  val temp = toSwap(i)
  toSwap(i) = toSwap(i+1)
  toSwap(i+1) = temp
}
toSwap


// swap adjacent elements of one array into a new array
val arr1 = (0 until 11).toArray
val arr2 = for (i <- arr1.indices) yield {
  if (i % 2 == 1)
    arr1(i-1)
  else {
    if (i == arr1.length-1)
      arr1(i)
    else
      arr1(i+1)
  }
}


// given one array produce a new array with the values > 0 followed by the
// values <= 0, maintaining the orders within the subsets
val arr4 = Array(-3, 5, 0, -5, 9, 8, -1, 0)
val arr4Pos = for (i <- arr4 if i > 0) yield i
val arr4Neg = for (i <- arr4 if i < 1) yield i
val newArr4 = arr4Pos ++ arr4Neg


// compute the value of an Array[Double]
val arr5: Array[Double] = Array(1.5, 4.4, 0, -1.99)
val avg = arr5.sum / arr5.length


// sort Array[Int] and ArrayBuffer[Int] in reverse order
var arr6 = Array(0,1,2,3,4,5)
arr6.sortWith(_ > _)

var arr6Buffer = ArrayBuffer(0,1,2,3,4,5)
arr6Buffer.sortWith(_ > _)


// remove duplicates from an Array
val arr7 = Array(0,0,1,1,2,2,3,3)
arr7.distinct


// remove all but first negative number from an Array
val arr8 = Array(6, -3, 5, 4, -6, 9, -1, -3)
val arr8NegIndexes = for (i <- arr8.indices if arr8(i) < 0) yield i
val arr8TakeRight = arr8NegIndexes.takeRight(arr8NegIndexes.length-1)
val arr8Pos = for (i <- arr8.indices) yield {
  if (arr8(i) > 0) arr8(i)
  else if (!arr8TakeRight.contains(i)) arr8(i)
}
