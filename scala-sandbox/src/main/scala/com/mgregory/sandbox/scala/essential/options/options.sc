import scala.util.Try

def addOptions(a: Option[Int], b: Option[Int]): Option[Int] = {
  for {
    x <- a
    y <- b
  } yield {
    x + y
  }
}

addOptions(Some(0), Some(0))
addOptions(Some(1), Some(2))
addOptions(Some(1), None)
addOptions(None, Some(1))
addOptions(None, None)

def addOptionsMap(a: Option[Int], b: Option[Int]): Option[Int] = {
  a.flatMap(x => b.map(y => x + y))
}

addOptionsMap(Some(0), Some(0))
addOptionsMap(Some(1), Some(2))
addOptionsMap(Some(1), None)
addOptionsMap(None, Some(1))
addOptionsMap(None, None)


def addOptions3(a: Option[Int], b: Option[Int], c: Option[Int]): Option[Int] = {
  for {
    x <- addOptions(a, b)
    y <- c
  } yield { x + y }
}

addOptions3(Some(0), Some(0), Some(0))
addOptions3(Some(1), Some(2), Some(3))
addOptions3(Some(1), None, Some(1))
addOptions3(None, Some(1), Some(1))
addOptions3(None, None, None)

def addOptionsMap3(a: Option[Int], b: Option[Int], c: Option[Int]): Option[Int] = {
  addOptions(a, b).flatMap(x => c.map(y => x + y))
}

addOptionsMap3(Some(0), Some(0), Some(0))
addOptionsMap3(Some(1), Some(2), Some(3))
addOptionsMap3(Some(1), None, Some(1))
addOptionsMap3(None, Some(1), Some(1))
addOptionsMap3(None, None, None)


def divide(dividend: Int, divisor: Int): Option[Double] = {
  divisor match {
    case 0 => None
    case _ => Some(dividend/divisor)
  }
}

divide(4, 2)
divide(1, 1)
divide(0, 1)
divide(1, 0)

def divideOptions(dividend: Option[Int], divisor: Option[Int]): Option[Double] = {
  for {
    x <- dividend
    y <- divisor
    r <- divide(x, y)
   } yield r
}

divideOptions(Some(4), Some(2))
divideOptions(Some(1), Some(1))
divideOptions(Some(0), Some(1))
divideOptions(Some(0), Some(0))
divideOptions(Some(1), None)
divideOptions(None, Some(1))
divideOptions(None, None)


def calculator(op1: String, op: String, op2: String): Unit = {
  val result = for {
    x <- Try(op1.toInt).toOption
    y <- Try(op2.toInt).toOption
    r <- op match {
      case "+" => Some(x + y)
      case "-" => Some(x - y)
      case "*" => Some(x * y)
      case "/" => divide(x, y)
      case _ => None
    }
  } yield r

  result match {
    case Some(x) => println(s"result=$x")
    case None => println("error")
  }
}

calculator("1", "+", "1")
calculator("1", "+", "0")
calculator("1", "+", "three")
calculator("1", "-", "4")
calculator("7", "*", "4")
calculator("f", "*", "4")
calculator("7", "/", "4")
calculator("7", "/", "0")
calculator("1", "x", "1")
