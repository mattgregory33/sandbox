import com.mgregory.sandbox.scala.essential.traffic.{Green, Red, TrafficCop, Yellow}

val green = Green
val yellow = Yellow
val red = Red

// option 1: access to polymorphic version of next()
green.polyNext
yellow.polyNext
red.polyNext


// option 2: access to internal pattern matched version of next()
green.patternNextInternal
yellow.patternNextInternal
red.patternNextInternal


// option 3: access to external pattern matched version of next()
TrafficCop.patternNextExternal(green)
TrafficCop.patternNextExternal(yellow)
TrafficCop.patternNextExternal(red)


// conclusion: option 1 seems best because
// - logic for which light comes next is contained in one place, adding a 4th color would mean changing one method
// - calling it is concise