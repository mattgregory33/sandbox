package com.mgregory.sandbox.scala.essential.recursive_datatypes

// option 1: sum/double can be provided via pattern matching within the trait
sealed trait Tree {
  def sum: Int = this match {
    case Leaf(element) => element
    case Node(left, right) => left.sum + right.sum
  }

  def double: Tree = this match {
    case Leaf(element) => Leaf(element*2)
    case Node(left, right) => Node(left.double, right.double)
  }
}

// option 2: sum/double can be provided within the Tree implementations polymorphically
case class Node(left: Tree, right: Tree) extends Tree {
  def pSum: Int = left.sum + right.sum
  def pDouble: Tree = Node(left.double, right.double)
}

case class Leaf(element: Int) extends Tree {
  def pSum: Int = element
  def pDouble: Tree = Leaf(element * 2)
}

// option 3: sum/double can be provided from a utility object
object TreeOps {
  def sum(tree: Tree): Int = {
    tree match {
      case Leaf(element) => element
      case Node(left, right) => sum(left) + sum(right)
    }
  }

  def double(tree: Tree): Tree = {
    tree match {
      case Leaf(element) => Leaf(element * 2)
      case Node(left, right) => Node(double(left), double(right))
    }
  }
}
