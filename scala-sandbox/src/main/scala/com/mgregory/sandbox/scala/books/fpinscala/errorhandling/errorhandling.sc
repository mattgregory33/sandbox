import com.mgregory.sandbox.scala.books.fpinscala.errorhandling

// Exercise 4.2 - implement variance in using flatMap
def variance(xs: Seq[Double]): Option[Double] = {

  def mean(xs: Seq[Double]): Option[Double] = {
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)
  }

  mean(xs) flatMap (m => mean(xs map (x => math.pow(x - m, 2))))
}

variance(Seq(1.0, 2.0, 3.0))
variance(Nil)




