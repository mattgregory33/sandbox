import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import play.api.libs.ws.ahc.StandaloneAhcWSClient
import scala.concurrent.duration._


import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

implicit val system = ActorSystem()
implicit val materializer = ActorMaterializer()
implicit val executionContext = ExecutionContext.global

val client = StandaloneAhcWSClient()

val req = client.url("http://www.google.com")
val resp = req.get()

val x = Await.result(resp, 30.seconds)
println(x.body)
