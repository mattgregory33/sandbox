package com.mgregory.sandbox.scala.essential.extractors

object Positive {

  def unapply(value: Int): Option[Int] = {
    if (value > 0) Some(value) else None
  }

}
