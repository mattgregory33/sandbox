implicit val absOrdering = Ordering.fromLessThan[Int](math.abs(_) < math.abs(_))

assert(List(-4, -1, 0, 2, 3).sorted(absOrdering) == List(0, -1, 2, 3, -4))
assert(List(-4, -3, -2, -1).sorted(absOrdering) == List(-1, -2, -3, -4))

assert(List(-4, -1, 0, 2, 3).sorted == List(0, -1, 2, 3, -4))
assert(List(-4, -3, -2, -1).sorted == List(-1, -2, -3, -4))


case class Rational(numerator: Int, denominator: Int)

implicit val rationalOrdering = Ordering.fromLessThan[Rational]((a, b) => a.numerator.toFloat/a.denominator.toFloat < b.numerator.toFloat/b.denominator.toFloat)
assert(List(Rational(1, 2), Rational(3, 4), Rational(1, 3)).sorted == List(Rational(1, 3), Rational(1, 2), Rational(3, 4)))