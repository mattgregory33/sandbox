package com.mgregory.sandbox.scala.essential

sealed trait Shape {
  def sides: Int
  def perimiter: Double
  def area: Double
  def describe: String
  def color: Color
}

object Draw {
  def apply(shape: Shape): String = {
    // commenting one of these cases out is a compile error because Shape is a sealed trait
    // note that the results of the cases use ${Draw(color)}
    shape match {
      case Circle(radius, color) => s"A ${Draw(color)} circle with radius $radius"
      case Square(size, color) => s"A ${Draw(color)} square with size of $size"
      case Rectangle(height, width, color) => s"A ${Draw(color)} rectangle with height of $height and width of $width"
    }
  }

  def apply(color: Color): String = {
    color match {
      case Red => "red"
      case Yellow => "yellow"
      case Pink => "pink"
      case c => c.shade
    }
  }
}

sealed trait Rectangular extends Shape {
  def width: Double
  def height: Double
  override val sides: Int = 4
  override def perimiter: Double = width * 2 + height * 2
  override def area: Double = width * height
}


final case class Circle(radius: Double, color: Color) extends Shape {
  override val sides: Int = 0
  override def perimiter: Double = 2 * math.Pi * radius
  override def area: Double = math.Pi * radius * radius
  override def describe: String = s"A circle with radius $radius"
}

final case class Rectangle(width: Double, height: Double, color: Color) extends Rectangular {
  override def describe: String = s"A rectangle with height of $height and width of $width"
}

final case class Square(size: Double, color: Color) extends Rectangular {
  override val width: Double = size
  override val height: Double = size
  override def describe: String = s"A square with size of $size"
}


sealed trait Color {
  def red: Int
  def green: Int
  def blue: Int
  def shade: String = {
    if (red + green + blue > 255*3/2) "dark" else "light"
  }
}

case object Red extends Color {
  override val red: Int = 255
  override val green: Int = 0
  override val blue: Int = 0
}

case object Yellow extends Color {
  override val red: Int = 255
  override val green: Int = 255
  override val blue: Int = 0
}

case object Pink extends Color {
  override val red: Int = 255
  override val green: Int = 192
  override val blue: Int = 203
}

final case class CustomColor(r: Int, g: Int, b: Int) extends Color {
  override val red: Int = r
  override val green: Int = g
  override val blue: Int = b
}