// 3 is an Int object so we can apply Int methods to it
3

// REPL stores results of expressions in res variables
math.sqrt(res0)
math.pow(res1, 2)
3 - res2

// res variables are vals, can't be reassigned
//res1 = 5 // reassignment to val error

// * on String concatenates
// * is defined in http://www.scala-lang.org/api/current/scala/collection/immutable/StringOps.html#*(n:Int):String
"crazy" * 3

// max finds the larger of the 2 inputs
// http://www.scala-lang.org/api/current/scala/Int.html#max(that:Int):Int
10 max 2
2 max 10


(2:BigInt).pow(1024)


// BigInt.probablePrime is a companion object method so can be used without qualification
// with the following imports
import scala.math.BigInt._
import scala.util.Random
probablePrime(100, Random)


// http://www.scala-lang.org/api/current/scala/math/BigInt.html#toString(radix:Int):String
// http://www.scala-lang.org/api/current/scala/collection/immutable/StringOps.html#slice(from:Int,until:Int):String
val rand = probablePrime(100, Random)
val base36Str = rand.pow(1024).toString(36)
base36Str.slice(0, 20)



// to get the first character of a string, use head
"string".head

// to get the last character, use last
"string".last

// take returns the first n elements of a string (or all if string is shorter)
"string".take(3)
"string".take(10)

// drop returns all elements except the first n (or empty if string is shorter)
"string".drop(3)
"string".drop(10)

// takeRight returns the last n elements of a string (or all if string is shorter)
"string".takeRight(3)
"string".takeRight(10)

// dropRight trims the last n elements of a string and returns the remaining (or empty if string is shorter)
"string".dropRight(3)
"string".dropRight(10)

// take, drop, takeRight, dropRight are better options than substring because substring
// needs to have a type conversion
// http://www.scala-lang.org/api/current/scala/collection/immutable/StringOps.html#substring(x$1:Int,x$2:Int):String