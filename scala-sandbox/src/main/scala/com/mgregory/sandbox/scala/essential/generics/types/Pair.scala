package com.mgregory.sandbox.scala.essential.generics.types

case class Pair[A, B](one: A, two: B)