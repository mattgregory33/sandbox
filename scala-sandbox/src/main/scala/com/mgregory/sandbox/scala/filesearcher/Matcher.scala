package com.mgregory.sandbox.scala.filesearcher

import java.io.File

import scala.annotation.tailrec

class Matcher(filter: String,
              val rootLocation: String = new File(".").getCanonicalPath,
              checkSubFolders: Boolean = false,
              contentFilter: Option[String] = None) {

  val rootIOObject = FileConverter.convertToIOObject(new File(rootLocation))

  def execute() = {
    // note recursiveMatch is nested within execute(), this helps classify it as a private helper within execute()
    @tailrec
    def recursiveMatch(files: List[IOObject], currentList: List[FileObject]): List[FileObject] =
    files match {
      case List() => currentList
      case head :: rest =>
        head match {
          case file: FileObject if FilterChecker(filter) matches file.name => recursiveMatch(rest, file :: currentList)
          case directory: DirectoryObject => recursiveMatch(rest ::: directory.children(), currentList)
          case _ => recursiveMatch(rest, currentList)
        }
    }

    val matchedFiles = rootIOObject match {
      case file: FileObject if FilterChecker(filter) matches file.name => List(file)
      case directory: DirectoryObject =>
        if (checkSubFolders) recursiveMatch(directory.children(), List())
        else FilterChecker(filter) findMatchedFiles directory.children()
      case _ => List()
    }

    val contentFilteredFiles = contentFilter match {
      case Some(dataFilter) => matchedFiles
        .map(ioObject => (ioObject, Some(FilterChecker(dataFilter).findMatchedContentCount(ioObject.file))))
        .filter(matchTuple => matchTuple._2.get > 0)
      case None => matchedFiles map (ioObject => (ioObject, None))
    }

    contentFilteredFiles map {case (ioObject, count) => (ioObject.name, count)}
  }
}
