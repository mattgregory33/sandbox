package com.mgregory.sandbox.scala.essential.generics.types

sealed trait Sum[+A, +B] {

  def fold[C](failure: (A) => C, success: (B) => C): C = this match {
    case Failure(v) => failure(v)
    case Success(v) => success(v)
  }

  def map[C](fn: B => C): Sum[A, C] = this match {
    case Failure(v) => Failure(v)
    case Success(v) => Success(fn(v))
  }

  def flatMap[AA >: A, C](fn: B => Sum[AA, C]): Sum[AA, C] = this match {
    case Failure(v) => Failure(v)
    case Success(v) => fn(v)
  }
}

case class Failure[A](value: A) extends Sum[A, Nothing]
case class Success[B](value: B) extends Sum[Nothing, B]