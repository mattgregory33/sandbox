package com.mgregory.sandbox.scala.essential

object ChipShop {
  def willServe(cat: Cat): Boolean = {
    cat match {
      case Cat(_, _, "Chips") => true
      case _ => false
    }
  }
}