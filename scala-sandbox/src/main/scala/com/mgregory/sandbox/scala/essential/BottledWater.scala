package com.mgregory.sandbox.scala.essential

// This example combines the Product Type (has-a and) and Sum Type (is-a or) patterns
// BottledWater uses the Product Type pattern
// Source uses the Sum Type pattern
final case class BottledWater(size: Int, source: Source, carbonated: Boolean)

sealed trait Source
case object Well extends Source
case object Spring extends Source
case object Tap extends Source
