import com.mgregory.sandbox.scala.essential.{Finite, Infinite, divide}

divide(4, 2)
divide(1, 0)


divide(1, 0) match {
  case Infinite() => "It's infinite"
  case Finite(_) => "It's finite"
}

val infinite = (a: Int, b: Int) => divide(a, b) match {
  case Infinite() => "It's infinite"
  case Finite(_) => "It's finite"
}

infinite(4, 7)
infinite(0, 1)
infinite(1, 0)