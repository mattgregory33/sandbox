import java.util.Date

import com.mgregory.sandbox.scala.essential.typeclasses.{JsWriter, Visitor, Anonymous, User}
import com.mgregory.sandbox.scala.essential.typeclasses.JsUtil._
import com.mgregory.sandbox.scala.essential.typeclasses.VisitorImplicits._

implicit object VisitorWriter extends JsWriter[Visitor] {
  def write(value: Visitor) = value match {
    case anon: Anonymous => anon.toJson
    case user: User => user.toJson
  }
}

val visitors: Seq[Visitor] = Seq(Anonymous("001", new Date), User("003", "dave@xample.com", new Date))

visitors.map(v => v.toJson.stringify)