// immutable
// maps are sets of key/value pairs
// contain 2-element tuples

val stateCodes = Map(
  "California" -> "CA",
  "New York" -> "NY",
  ("Florida", "FL") // entries can be specified in either tuple notation
)


// lookup by key
stateCodes("Florida")
//stateCodes("Texas") // NoSuchElementException!

// test if the map contains the key
stateCodes.contains("Texas")



// toMap converts list of tuples to a map
val states = List("Washington", "Oregon", "Montana")
val codes = List("WA", "OR", "MT")

val stateCodes2 = (states zip codes).toMap


// toList can convert keys/values from map to list
stateCodes.keySet.toList
stateCodes.values.toList


// use util.Try to catch exceptions
val texas = util.Try(stateCodes("Texas")) // exeption wrapped in Failure object and stored in val

texas match {
  case util.Success(code) => code
  case util.Failure(error) => s"Error was $error"
}
