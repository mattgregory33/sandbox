import com.mgregory.sandbox.scala.essential.{Adder, Counter}

Counter(10).inc.dec.inc.inc.count
Counter(1).inc(3).dec.dec(2).count

Counter(13).adjust(new Adder(5)).count
Counter().adjust(new Adder(1)).count

