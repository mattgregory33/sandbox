import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}

import scala.concurrent.Future
import scala.util.{Failure, Success}

val req = HttpRequest(uri = "https://akka.io")

implicit val system = ActorSystem()
implicit val executionContext = system.dispatcher

val resp: Future[HttpResponse] = Http().singleRequest(req)

resp
  .onComplete {
    case Success(res) => println(res)
    case Failure(_)   => sys.error("something wrong")
  }