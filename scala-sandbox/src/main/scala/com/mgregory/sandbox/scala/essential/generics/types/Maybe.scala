package com.mgregory.sandbox.scala.essential.generics.types

sealed trait Maybe[A] {
  def fold[B](empty: B, full: (A) => B): B = this match {
    case Empty() => empty
    case Full(v) => full(v)
  }

  def map[B](fn: A => B): Maybe[B] = this match {
    case Empty() => Empty()
    case Full(v) => Full(fn(v))
  }

  def flatMap[B](fn: A => Maybe[B]): Maybe[B] = this match {
    case Empty() => Empty()
    case Full(v) => fn(v)
  }
}

case class Full[A] (value: A) extends Maybe[A]
case class Empty[A] () extends Maybe[A]

