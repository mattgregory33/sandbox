package com.mgregory.sandbox.scala.essential.recursive_datatypes

import com.mgregory.sandbox.scala.essential.common.{Failure, Result, Success}

sealed trait Expression {
  def eval: Result[Double]
}

case class Addition(left: Expression, right: Expression) extends Expression {
  override def eval: Result[Double] = {
    left.eval match {
      case Failure(error) => Failure(error)
      case Success(leftVal) => {
        right.eval match {
          case Failure(error) => Failure(error)
          case Success(rightVal) => Success(leftVal + rightVal)
        }
      }
    }
  }
}

case class Subtraction(left: Expression, right: Expression) extends Expression {
  override def eval: Result[Double] = {
    left.eval match {
      case Failure(error) => Failure(error)
      case Success(leftVal) => {
        right.eval match {
          case Failure(error) => Failure(error)
          case Success(rightVal) => Success(leftVal - rightVal)
        }
      }
    }
  }
}

case class Division(left: Expression, right: Expression) extends Expression {
  override def eval: Result[Double] = {
    right.eval match {
      case Failure(error) => Failure(error)
      case Success(0) => Failure("Division by zero")
      case Success(rightVal) => {
        left.eval match {
          case Failure(error) => Failure(error)
          case Success(leftVal) => Success(leftVal/rightVal)
        }
      }
    }
  }
}

case class SquareRoot(operand: Expression) extends Expression {
  override def eval: Result[Double] = operand.eval match {
    case Failure(error) => Failure(error)
    case Success(value) if value < 0 => Failure("Square root of negative number")
    case Success(value) => Success(math.sqrt(value))
  }
}

case class Number(value: Double) extends Expression {
  override def eval: Result[Double] = Success(value)
}
