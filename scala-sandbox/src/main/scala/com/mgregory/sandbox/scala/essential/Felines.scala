package com.mgregory.sandbox.scala.essential

trait Feline {
  def color: String
  def sound: String

}

trait BigCat extends Feline {
  val sound = "roar"
}

case class Cat(color: String, food: String, name: String) extends Feline {
  val sound = "meow"
}

case class Tiger(color: String) extends BigCat

case class Lion(color: String, maneSize: Int) extends BigCat

case class Panther(color: String) extends BigCat