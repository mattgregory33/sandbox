package com.mgregory.sandbox.scala.essential.functions

sealed trait IntList {

  def fold[A](end: A, f: (Int, A) => A): A = this match {
    case End => end
    case Pair(head, tail) => f(head, tail.fold(end, f))
  }

  def length: Int = fold[Int](0, (_, tail) => 1 + tail)

  def product: Int = fold[Int](1, (head, tail) => head * tail)

  def sum: Int = fold[Int](0, (head, tail) => head + tail)

  def double: IntList = fold[IntList](End, (head, tail) => Pair(head * 2, tail))

}

case object End extends IntList
final case class Pair(head: Int, tail: IntList) extends IntList

