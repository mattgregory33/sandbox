package com.mgregory.sandbox.scala.essential.typeclasses

trait JsWriter[A] {
  def write(value: A) : JsValue
}

object JsUtil {
  implicit class JsUtil[A](value: A) {
    def toJson(implicit writer: JsWriter[A]): JsValue = {
      writer.write(value)
    }
  }
}

sealed trait JsValue {
  def stringify: String
}

case class JsObject(values: Map[String, JsValue]) extends JsValue {
  override def stringify: String = values.map {
    case (name, value) => "\"" + name + "\":" + value.stringify
  }.mkString("{", ",", "}")
}

case class JsString(value: String) extends JsValue {
  override def stringify: String = "\"" + value.replaceAll("\\|\"", "\\\\$1") + "\""
}