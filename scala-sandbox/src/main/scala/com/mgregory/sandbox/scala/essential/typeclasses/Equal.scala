package com.mgregory.sandbox.scala.essential.typeclasses

trait Equal[A] {
  def equals(o1: A, o2: A): Boolean
}

object Equal {
  def apply[A](implicit instance: Equal[A]): Equal[A] = instance

  implicit class ToEqual[A](in: A) {
    def ===(other: A)(implicit equal: Equal[A]): Boolean = equal.equals(in, other)
  }
}

case class Person(name: String, email: String)

object PersonEqualByEmailImplicit {
  implicit object PersonEqualByEmail extends Equal[Person] {
    override def equals(o1: Person, o2: Person): Boolean = o1.email == o2.email
  }
}

object PersonEqualByNameAndEmailImplicit {
  implicit object PersonEqualByNameAndEmail extends Equal[Person] {
    override def equals(o1: Person, o2: Person): Boolean = o1.email == o2.email && o1.name == o2.name
  }
}

object Eq {
  def apply[A](o1: A, o2: A)(implicit equal: Equal[A]): Boolean = equal.equals(o1, o2)
}
