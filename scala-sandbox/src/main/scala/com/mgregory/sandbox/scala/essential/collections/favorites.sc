import scala.collection.immutable.ListMap

val people = Set("Alice", "Bob", "Charlie", "Derek", "Edith", "Fred")

val ages = Map(
  "Alice" -> 20,
  "Bob" -> 30,
  "Charlie" -> 50,
  "Derek" -> 40,
  "Edith" -> 10,
  "Fred" -> 60
)

val favoriteColors = Map(
  "Bob" -> "green",
  "Derek" -> "magenta",
  "Fred" -> "yellow"
)

val favoriteLolcats = Map(
  "Alice" -> "Long Cat",
  "Charlie" -> "Ceiling Cat",
  "Edith" -> "Cloud Cat"
)


def favoriteColor(name: String): String = {
  favoriteColors.getOrElse(name, "Beige")
}

favoriteColor("Bob")
favoriteColor("Derek")
favoriteColor("Fred")
favoriteColor("Ashley")


def printColors(): Unit = {
  for {
    c <- favoriteColors
  } yield println(c._2)
}

printColors()


def lookup[T](name: String, m:Map[String, T]): T = {
  m(name)
}

lookup("Alice", favoriteLolcats)
lookup("Fred", favoriteColors)
lookup("Derek", ages)


// oldest person's favorite color
val oldest = ListMap(ages.toSeq.sortWith(_._2 > _._2):_*).head
favoriteColor(oldest._1„)