import com.mgregory.sandbox.scala.essential.typeclasses.PersonEqualByEmailImplicit.PersonEqualByEmail
import com.mgregory.sandbox.scala.essential.typeclasses.PersonEqualByNameAndEmailImplicit.PersonEqualByNameAndEmail
import com.mgregory.sandbox.scala.essential.typeclasses.{Eq, Equal, Person}

val homer = Person("Homer Simpson", "hjsimpson@spnuke.com")
val homerj = Person("Homer Jay Simpson", "hjsimpson@spnuke.com")
val marge = Person("Marge Simpson", "msimpson@smail.com")

PersonEqualByNameAndEmail.equals(homer, marge)
PersonEqualByNameAndEmail.equals(homer, homerj)
PersonEqualByEmail.equals(homer, homerj)


implicit val caseInsensitiveEquals = new Equal[String] {
  def equals(s1: String, s2: String) = s1.toLowerCase == s2.toLowerCase
}
import com.mgregory.sandbox.scala.essential.typeclasses.Equal._
"foo" === "FOO"