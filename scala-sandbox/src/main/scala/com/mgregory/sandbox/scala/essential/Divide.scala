package com.mgregory.sandbox.scala.essential

object divide {
  def apply(a: Int, b:Int): DivisionResult = {
    if (b == 0) Infinite() else Finite(a / b)
  }
}

sealed trait DivisionResult {
}

case class Finite(result: Double) extends DivisionResult
case class Infinite() extends DivisionResult

