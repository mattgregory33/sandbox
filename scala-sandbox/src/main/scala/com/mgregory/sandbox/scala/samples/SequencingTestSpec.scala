package com.mgregory.sandbox.scala.samples

import org.scalatest._

class SequencingTestSpec extends WordSpec with Matchers {

  var x = 0
  "the first test" should {
    "see the initial value" in {x should equal(0)}
    "increment it to 1" in {
      x += 1
      x should equal(1)
    }
    "increment it to 2" in {
      x += 2
      x should equal(2)
    }
  }

  "the second test" should {
    "see the initial value" in {x should equal(2)}
    "increment it to 3" in {
      x += 3
      x should equal(3)
    }
    "increment it to 4" in {
      x += 4
      x should equal(4)
    }
  }

}
