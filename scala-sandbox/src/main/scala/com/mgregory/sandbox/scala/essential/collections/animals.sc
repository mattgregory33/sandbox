import scala.collection.mutable

val animals = Seq("cat", "dog", "penguin")
animals ++ Seq("tyrannosaurus")
Seq("mouse") ++ animals

Seq(2) ++ animals


val mutableAnimals = mutable.Seq("cat", "dog", "penguin")
mutableAnimals(1) = "rat"
mutableAnimals
//mutableAnimals(0) = 7  does not compile
