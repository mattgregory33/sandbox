import com.mgregory.sandbox.scala.books.fpinscala._


// which case matches?
// A: 3rd case, 4th also matches but is preceded by the 3rd case which also matches
val answer = List(1,2,3,4,5) match {
  case Cons(x, Cons(2, Cons(4, _))) => x
  case Nil => 42
  case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y //this case matches
  case Cons(h, t) => h + List.sum(t)
  case _ => 101
}
