package com.mgregory.sandbox.scala.books.fpinscala.functionaldatastructures

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  // Exercise 3.25
  // Exercise 3.29 - rewrite in terms of fold
  def size[A](t: Tree[A]): Int = fold(t)(_ => 1)((a,b) => a + b + 1)

  // Exercise 3.26
  // Exercise 3.29 - rewrite in terms of fold
  def maximum(t: Tree[Int]): Int = fold(t)(x => x)((a,b) => a max b)

  // Exercise 3.27
  // Exercise 3.29 - rewrite in terms of fold
  def depth[A](t: Tree[A]): Int = fold(t)(_ => 1)((a,b) => 1+a max 1+b)

  // Exercise 3.28
  // Exercise 3.29 - rewrite in terms of fold
  def map[A,B](t: Tree[A])(f: A => B): Tree[B] = fold[A, Tree[B]](t)(a => Leaf(f(a)))((a,b) => Branch(a, b))

  // Exercise 3.29
  def fold[A,B](t: Tree[A])(a: A => B)(b: (B,B) => B): B = t match {
    case Leaf(x) => a(x)
    case Branch(l, r) => b(fold(l)(a)(b), fold(r)(a)(b))
  }
}
