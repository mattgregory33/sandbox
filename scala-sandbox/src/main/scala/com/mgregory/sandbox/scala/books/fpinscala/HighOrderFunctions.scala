package com.mgregory.sandbox.scala.books.fpinscala

import scala.annotation.tailrec

// exercises, chapter 2
object HighOrderFunctions {

  // monomorphic; recursive
  def fib(idx: Int): Int = {

    @tailrec
    def loop(n1: Int, n2: Int, cnt: Int): Int = {
      if (idx == cnt) n1 + n2
      else loop(n2, n1 + n2, cnt + 1)
    }

    if (idx <= 1) idx
    else loop(0, 1, 2)
  }

  // polymorphic; recursive
  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {

    @tailrec
    def loop(acc: Array[A]): Boolean = {
      if (acc.length <= 1) true
      else if (acc.length == 2) ordered(acc(0), acc(1))
      else if (ordered(acc(0), acc(1))) loop(acc.tail)
      else false
    }

    loop(as)
  }

  // currying, partial
  def curry[A,B,C](f: (A, B) => C): A => (B => C) = {
    a: A => (b: B) => f(a, b)
  }

  // currying, partial
  def uncurry[A,B,C](f: A => B => C): (A, B) => C = {
    (a: A, b: B) => f(a)(b)
  }

  def compose[A,B,C](f: B => C, g: A => B): A => C = {
    a: A => f(g(a))
    // could also do: f compose g
    // could also do: g andThen f
  }

}
