import com.mgregory.sandbox.scala.essential.{Dad, Director, Film}

val eastwood          = Director("Clint", "Eastwood", 1930)
val mcTiernan         = Director("John", "McTiernan", 1951)
val nolan             = Director("Christopher", "Nolan", 1970)
val someBody          = Director("Just", "Some Body", 1990)

val memento           = Film("Memento", 2000, 8.5, nolan)
val darkKnight        = Film("Dark Knight", 2008, 9.0, nolan)
val inception         = Film("Inception", 2010, 8.8, nolan)

val highPlainsDrifter = Film("High Plains Drifter", 1973, 7.7, eastwood)
val outlawJoseyWales  = Film("The Outlaw Josey Wales", 1976, 7.9, eastwood)
val unforgiven        = Film("Unforgiven", 1992, 8.3, eastwood)
val granTorino        = Film("Gran Torino", 2008, 8.2, eastwood)
val invictus          = Film("Invictus", 2009, 7.4, eastwood)

val predator          = Film("Predator", 1987, 7.9, mcTiernan)
val dieHard           = Film("Die Hard", 1988, 8.3, mcTiernan)
val huntForRedOctober = Film("The Hunt for Red October", 1990, 7.6, mcTiernan)
val thomasCrownAffair = Film("The Thomas Crown Affair", 1999, 6.8, mcTiernan)

eastwood.yearOfBirth         // should be 1930
dieHard.director.name        // should be "John McTiernan"
invictus.isDirectedBy(nolan) // should be false
inception.isDirectedBy(nolan)

val highPlainsDrifter2 = highPlainsDrifter.copy(name = "L'homme des hautes plaines")
// returns Film("L'homme des hautes plaines", 1973, 7.7, /* etc */)
highPlainsDrifter2.name
highPlainsDrifter2.yearOfRelease

val thomasCrownAffair2 = thomasCrownAffair.copy(yearOfRelease = 1968, director = new Director("Norman", "Jewison", 1926))
// returns Film("The Thomas Crown Affair", 1926, /* etc */)
thomasCrownAffair2.name
thomasCrownAffair2.yearOfRelease
thomasCrownAffair2.director.name

val inception2 = inception.copy().copy().copy()
// returns a new copy of `inception`
inception2.name

// instantiation of Director via companion object apply
val scorcese = Director("Martin", "Scorcese", 1942)
val ronning = Director("Joachim", "Ronning", 1972)

Director.older(scorcese, eastwood).lastName
Director.older(eastwood, nolan).lastName
Director.older(nolan, mcTiernan).lastName

val pirates = Film("Pirates of the Caribbean", 2017, 7.1, ronning)

//Film.highestRating(pirates, predator).name
//Film.highestRating(granTorino, memento).name

//Film.oldestDirectorAtTheTime(darkKnight, unforgiven).lastName

Dad.rate(predator)
Dad.rate(memento)
Dad.rate(inception)
Dad.rate(outlawJoseyWales)
