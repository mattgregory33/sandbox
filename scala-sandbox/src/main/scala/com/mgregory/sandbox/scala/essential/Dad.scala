package com.mgregory.sandbox.scala.essential

object Dad {

  def rate(film: Film): Double = {
    film.director match {
      case Director(_, "Eastwood", _) => 10.0
      case Director(_, "McTiernan", _) => 7.0
      case Director(_, _, _) => 3.0
    }
  }

}
