package com.mgregory.sandbox.scala.books.fpinscala.laziness

import com.mgregory.sandbox.scala.books.fpinscala.laziness.Stream._

import scala.annotation.tailrec

sealed trait Stream[+A] {

  def toList: List[A] = {
    this match {
      case Empty => Nil
      case Cons(h, tl) => h() :: tl().toList
    }
  }

  def take(n: Int): Stream[A] = this match {
     case Empty => empty
     case Cons(h, t) if n > 0 => cons(h(), t().take(n-1))
     case Cons(_, _) if n == 0 => this
   }

  @tailrec
  final def drop(n: Int): Stream[A] = this match {
    case Empty => empty
    case Cons(_, t) if n > 0 => t().drop(n-1)
  }

  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Empty => empty
    case Cons(h, t) if p(h()) => cons(h(), t().takeWhile(p))
    case Cons(_, _) => this
  }
}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {

  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))
}
