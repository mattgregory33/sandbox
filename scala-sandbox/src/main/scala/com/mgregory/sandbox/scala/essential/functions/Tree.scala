package com.mgregory.sandbox.scala.essential.functions

trait Tree[A] {
  def fold[B](leaf: A => B, node: (B, B) => B): B = this match {
    case Leaf(element) => leaf(element)
    case Node(left, right) => node(left.fold(leaf, node), right.fold(leaf, node))
  }
}

case class Node[A](left: Tree[A], right: Tree[A]) extends Tree[A]
case class Leaf[A](element: A) extends Tree[A]


