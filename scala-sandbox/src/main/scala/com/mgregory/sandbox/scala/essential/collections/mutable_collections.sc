// mutable versions of all collection.immutable collections exists as mutable versions
// mutable lists are called buffers
// must specify collection.mutable - Scala will default to immutable version

// new mutable buffer (list)
val someNumbers = collection.mutable.Buffer(10,20,30,40,50)

// new mutable map
val stateCodes = collection.mutable.Map(
  "California" -> "CA",
  "Florida" -> "FL"
)


// convert mutable to immutable
val listBuilder = List.newBuilder[Int]
someNumbers.foreach(listBuilder+=_)
listBuilder.result // gives us back an immutable List


// arrays
val daysOfWeek = Array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
daysOfWeek(0) // arrays are 0-based
daysOfWeek(0) = "Back to work" // elements can be reassigned
daysOfWeek

