package com.mgregory.sandbox.scala.books.fpinscala.errorhandling

// Exercise 4.1 Option implementation
sealed trait Option[+A] {
  def map[B](f: A => B): Option[B]
  def flatMap[B](f: A => Option[B]): Option[B]
  def getOrElse[B >: A](default: => B): B
  def orElse[B >: A](ob: => Option[B]): Option[B]
  def filter(f: A => Boolean): Option[A]
}

case class Some[+A](get: A) extends Option[A] {
  override def map[B](f: A => B): Option[B] = Some(f(get))

  override def flatMap[B](f: A => Option[B]): Option[B] = f(get)

  override def getOrElse[B >: A](default: => B): B = get

  override def orElse[B >: A](ob: => Option[B]): Option[B] = this

  override def filter(f: A => Boolean): Option[A] = if (f(get)) this else None
}

case object None extends Option[Nothing] {
  override def map[B](f: Nothing => B): Option[B] = None

  override def flatMap[B](f: Nothing => Option[B]): Option[B] = None

  override def getOrElse[B >: Nothing](default: => B): B = default

  override def orElse[B >: Nothing](ob: => Option[B]): Option[B] = ob

  override def filter(f: Nothing => Boolean): Option[Nothing] = None
}

object Option {
  def lift[A,B](f: A => B): Option[A] => Option[B] = _ map f

  // Exercise 4.3
  def map2[A,B,C](a: Option[A], b: Option[B])(f: (A,B) => C): Option[C] =
  for {
    aa <- a
    bb <- b
  } yield f(aa, bb)
  // this is equivalent to: a flatMap (aa => b map (bb => f(aa,bb)))

  // Exercise 4.4
  // Exercise 4.5 - rewrite sequence in terms of traverse
  def sequence[A](as: List[Option[A]]): Option[List[A]] =
  traverse(as)(a => a)

  // Exercise 4.5
  def traverse[A,B](as: List[A])(f: A => Option[B]): Option[List[B]] =
    as.foldRight[Option[List[B]]](Some(Nil))((a, agg) => map2(f(a), agg)(_ :: _))
}

