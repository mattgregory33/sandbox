import scala.collection.mutable

// find the smallest element of a Seq[Int]
def smallest(seq: Seq[Int]): Int = {
  seq.foldLeft(Int.MaxValue)(math.min(_, _))
}

smallest(List(1,2,3,4,5))
smallest(List(9,8,7,6,5))
smallest(List(853,93235,82305872,-1,0))


// find the unique elements of a Seq[Int]
def unique(seq: Seq[Int]): Seq[Int] = {
  var unique: mutable.Set[Int] = mutable.Set()
  seq.foreach(unique.add(_))
  unique.toSeq
}

unique(List(1,1,2,4,3,4))
unique(List(1,1,1,1))


// reverse elements of a sequence
def reverse(seq: Seq[Int]): Seq[Int] = {
  seq.foldLeft(Seq[Int]())((s,i) => i +: s)
}

reverse(List(1,2,3,4))


// write map in terms of foldRight
def foldMap[A, B](seq: Seq[A], f:A => B): Seq[B] = {
  seq.foldRight(Seq[B]())((a,b) => f(a) +: b)
}

foldMap[Int, Int](List(1,2,3), (a) => a + 1)
foldMap[Int, String](List(7,8,9), (a) => s"and a $a")


// fold left using foreach
def foldEach[A, B](seq: Seq[A], f:A => B): Seq[B] = {
  var result: Seq[B] = Seq[B]()
  seq.foreach( s =>
    result = result :+ f(s)
  )
  result
}

foldEach[Int, Int](List(1,2,3), (a) => a + 1)
foldEach[Int, String](List(7,8,9), (a) => s"and a $a")
