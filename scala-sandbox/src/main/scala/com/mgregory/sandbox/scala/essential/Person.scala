package com.mgregory.sandbox.scala.essential

case class Person (firstName: String, lastName: String) {
  def name = s"$firstName $lastName"
}


object Person {
  def apply (name: String): Person = {
    val nameParts = name.split(" ")
    Person(nameParts(0), nameParts(1))
  }
}