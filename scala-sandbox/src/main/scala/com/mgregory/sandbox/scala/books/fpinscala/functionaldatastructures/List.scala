package com.mgregory.sandbox.scala.books.fpinscala

import scala.annotation.tailrec

sealed trait List[+A]

case object Nil extends List[Nothing]

case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  def sum(is: List[Int]): Int =
  //    foldRight(ints, 0)((i, total) => i + total)
  // _ + _ is a more concise notation of the above
    foldRight(is, 0)(_ + _)

  // Exercise 3.11
  def sumLeft(is: List[Int]): Int =
    foldLeft(is, 0)(_ + _)

  def product(ds: List[Double]): Double =
  //    foldRight(ds, 1.0)((d, total) => d * total)
  // _ * _ is a more concise notation of the above
    foldRight(ds, 1.0)(_ * _)

  // Exercise 3.11
  def productLeft(ds: List[Double]): Double =
    foldLeft(ds, 1.0)(_ * _)

  // Exercise 3.13 - foldRight in terms of foldLeft
  def foldRight[A,B](as: List[A], agg: B)(f: (A, B) => B): B = foldLeft(reverse(as), agg)((agg, a) => f(a, agg))

  // Exercise 3.10
  def foldLeft[A, B](as: List[A], agg: B)(f: (B, A) => B): B = as match {
    case Nil => agg
    case Cons(hd, tl) => foldLeft(tl, f(agg, hd))(f)
  }

  // Exercise 3.2
  // Exercise 3.4 - refactored to use drop
  def tail[A](as: List[A]): List[A] = drop(as, 1)

  // Exercise 3.3
  def setHead[A](as: List[A], a: A): List[A] = as match {
    case Nil => List(a)
    case Cons(_, tl) => Cons(a, tl)
  }

  // Exercise 3.4 - generalize tail to drop
  @tailrec
  def drop[A](as: List[A], n: Int): List[A] =
    if (n <= 0)
      as
    else as match {
      case Nil => Nil
      case Cons(_, tl) => drop(tl, n - 1)
    }

  // Exercise 3.5
  @tailrec
  def dropWhile[A](as: List[A])(f: A => Boolean): List[A] = as match {
    case Nil => Nil
    case Cons(hd, tl) if f(hd) => dropWhile(tl)(f)
    case _ => as
  }

  // Exercise 3.14
  def append[A](ls: List[A], rs: List[A]): List[A] = foldRight(ls, rs)((l, rs) => Cons(l, rs))

  // Exercise 3.6
  def init[A](as: List[A]): List[A] = {
    def loop(as: List[A], agg: List[A]): List[A] = as match {
      case Nil => Nil
      case Cons(_, Nil) => agg
      case Cons(hd, tl) => loop(tl, append(agg, List(hd)))
    }

    loop(as, Nil)
  }

  // Exercise 3.9
  // Exercise 3.11 - length in terms of foldLeft
  def len[A](as: List[A]): Int = foldLeft(as, 0)((length, _) => length + 1)

  // Exercise 3.12
  def reverse[A](as: List[A]): List[A] = foldLeft(as, List[A]())((rev, a) => Cons(a, rev))

  // Exercise 3.15
  def flatten[A](ass: List[List[A]]): List[A] = foldRight(ass, List[A]())(append)

  // Exercise 3.16
  def increment(is: List[Int]): List[Int] = foldRight(is, List[Int]())((i, agg) => Cons(i + 1, agg))

  // Exercise 3.17
  def stringify(ds: List[Double]): List[String] = foldRight(ds, List[String]())((d, agg) => Cons(d.toString, agg))

  // Exercise 3.18
  def map[A,B](as: List[A])(f: A => B): List[B] = foldRight(as, List[B]())((a, agg) => Cons(f(a), agg))

  // Exercise 3.19
  // Exercise 3.21 - implement filter with flatMap
  def filter[A](as: List[A])(f: A => Boolean): List[A] = flatMap(as)(a => if (f(a)) List(a) else Nil)

  // Exercise 3.20
  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = flatten(map(as)(f))

  // Exercise 3.22
  def addElems(ls: List[Int], rs: List[Int]): List[Int] = (ls, rs) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(h1 + h2, addElems(t1, t2))
  }

  // Exercise 3.22
  def zipWith[A](ls: List[A], rs: List[A])(f: (A,A) => A): List[A] = (ls, rs) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(f(h1, h2), zipWith(t1, t2)(f))
  }

  // Exercise 3.23
  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = {
    def loop(sup: List[A], sub: List[A], matching: Boolean): Boolean = (sup, sub) match {
      case (Cons(suph, Nil), Cons(subh, Nil)) if suph == subh => true
      case (_, Nil) => matching
      case (Cons(Nil, _), Cons(subh, _)) if subh != Nil => false
      case (Cons(suph, supt), Cons(subh, subt)) if suph == subh => loop(supt, subt, matching = true)
      case (Cons(suph, _), Cons(subh, _)) if matching => false
      case (Cons(suph, supt), Cons(subh, subt)) if suph != subh => loop(supt, sub, matching = false)
      case _ => false
    }

    loop(sup, sub, matching = false)
  }
}