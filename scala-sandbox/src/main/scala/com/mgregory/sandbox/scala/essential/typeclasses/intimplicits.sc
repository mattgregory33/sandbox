import com.mgregory.sandbox.scala.essential.typeclasses.IntImplicits
import com.mgregory.sandbox.scala.essential.typeclasses.IntImplicits.ExtraIntMethods

new ExtraIntMethods(7).yeah

7.yeah


new ExtraIntMethods(3).times(i => println(s"Look - it's the number $i!"))

3.times(i => println(s"Look - it's the number $i!"))


7.times(i => println("Oh yeah!"))