package com.mgregory.sandbox.scala.essential.extractors

object Titlecase {

  def unapply(str: String): Option[String] = {
    val words = str.split(" ").toList map {
      word => word.substring(0, 1).toUpperCase() + word.substring(1)
    }

    Some(words.mkString(" "))
  }

}
