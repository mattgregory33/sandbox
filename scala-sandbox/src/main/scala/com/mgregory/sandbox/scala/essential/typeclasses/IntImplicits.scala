package com.mgregory.sandbox.scala.essential.typeclasses

object IntImplicits {

  implicit class ExtraIntMethods(val num: Int) {

    def yeah: Unit = for (_ <- 1 to num) println("Oh yeah!")

    def times(f: Int => Unit): Unit = for (_ <- 1 to num) f(num)
  }

}