package com.mgregory.sandbox.scala.essential.typeclasses

case class Order(units: Int, unitPrice: Double) {
  val totalPrice: Double = units * unitPrice
}

object Order {
  implicit val ordering: Ordering[Order] = Ordering.fromLessThan[Order](_.totalPrice < _.totalPrice)
}

object NumberOfUnitsOrdering {
  implicit val ordering: Ordering[Order] = Ordering.fromLessThan[Order](_.units < _.units)
}

object UnitPriceOrdering {
  implicit val ordering: Ordering[Order] = Ordering.fromLessThan[Order](_.unitPrice < _.unitPrice)
}