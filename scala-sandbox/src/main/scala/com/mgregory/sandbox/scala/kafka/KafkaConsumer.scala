package com.mgregory.sandbox.scala.kafka

import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import com.mgregory.sandbox.scala.essential.typeclasses.JsObject
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}
import play.api.libs.json.{Format, JsValue, Json}

import scala.util.control.NonFatal

object KafkaConsumer extends App {

  implicit val system = ActorSystem("kafka-consumer")
  implicit val materializer = ActorMaterializer()

  val config = system.settings.config.getConfig("akka.kafka.consumer")
  val consumerSettings =
    ConsumerSettings(config, new StringDeserializer, new ByteArrayDeserializer)
      .withBootstrapServers("localhost:9092")
      .withGroupId("group1")
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")

  // example 1 - consume the raw record
//  Consumer
//    .plainSource(consumerSettings, Subscriptions.topics("scala-consumer-test"))
//    .runWith(Sink.foreach(println))


  // example 2 - consume the record and emit the value as a String
//  Consumer
//    .plainSource(consumerSettings, Subscriptions.topics("scala-consumer-test"))
//    .map(_.value.map(_.toChar).mkString)
//    .runWith(Sink.foreach(println))


  // example 3 - consume the record and emit the json as a Person object
  Consumer
    .plainSource(consumerSettings, Subscriptions.topics("scala-consumer-test"))
    .map{r =>
      try{
        Json.parse(r.value).as[Person]
      } catch {
        case NonFatal(_) => r.value.map(_.toChar).mkString
      }}
    .runForeach(println)
}

case class Person(name: String, age: Int, payload: JsValue)

object Person {
  implicit val formatter: Format[Person] = Json.format[Person]
}