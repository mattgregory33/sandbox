import com.mgregory.sandbox.scala.essential.typeclasses.{Eq, Equal, Person}

def byNameAndEmail = {
  import com.mgregory.sandbox.scala.essential.typeclasses.PersonEqualByNameAndEmailImplicit._
  Eq(Person("Noel", "noel@example.com"), Person("Noel", "noel@example.com"))
}

byNameAndEmail

def byName = {
  import com.mgregory.sandbox.scala.essential.typeclasses.PersonEqualByEmailImplicit._
  Eq(Person("Noel", "noel@example.com"), Person("Noel2", "noel@example.com"))
}

byName


Equal[Person].equals(Person("Noel", "noel@example.com"), Person("Noel", "noel@example.com"))