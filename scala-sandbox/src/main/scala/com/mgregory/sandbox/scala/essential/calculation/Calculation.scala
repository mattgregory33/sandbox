package com.mgregory.sandbox.scala.essential.calculation

sealed trait Calculation

final case class Success(result: Int) extends Calculation

final case class Failure(message: String) extends Calculation


object Calculator {
  def +(c: Calculation, operand: Int): Calculation =
    c match {
      case Success(result) => Success(result + operand)
      case Failure(_) => c
    }

  def -(c: Calculation, operand: Int): Calculation =
    c match {
      case Success(result) => Success(result - operand)
      case Failure(_) => c
    }

  def /(c: Calculation, divisor: Int): Calculation = c match {
    case Success(result) => if (divisor == 0) Failure("Division by zero") else Success(result/divisor)
    case Failure(_) => c
  }
}

