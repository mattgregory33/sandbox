package com.mgregory.sandbox.scala.essential.generics

import com.mgregory.sandbox.scala.essential.common.{Failure, Result, Success}


sealed trait LinkedList[A] {
  def length: Int = this match {
    case End() => 0
    case Pair(_, tail) => 1 + tail.length
  }

  def contains(item: A): Boolean = this match {
    case End() => false
    case Pair(value, tail) if value == item => true
    case Pair(_, tail) => tail.contains(item)
  }

  def apply(index: Int): Result[A] = this match {
    case End() => Failure("Index out of bounds")
    case Pair(_, _) if index < 0 => Failure("Index out of bounds")
    case Pair(value, _) if index == 0 => Success(value)
    case Pair(_, tail) => tail(index - 1)
  }

  def map[B](fn: A => B): LinkedList[B] = this match {
    case End() => End()
    case Pair(head, tail) => Pair(fn(head), tail.map(fn))
  }
}

case class End[A]() extends LinkedList[A]
case class Pair[A](head: A, tail: LinkedList[A]) extends LinkedList[A]
