// create a Seq containing "cat", "dog", and "penguin"
val animals = Seq("cat", "dog", "penguin")

// append "tyrannosaurus"
animals :+ "tyrannosaurus"

// prepend "mouse"
"mouse" +: animals

// prepend a different type
2 +: animals
// prepending a new type makes the Seq become a Seq[Any]

