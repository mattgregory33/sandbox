package com.mgregory.sandbox.scala.essential.probabilities

case class Distribution[A](events: List[(A, Double)]) {
  def map[B](f: A => B): Distribution[B] = {
    Distribution(events map {case (d, p) => (f(d), p)})
  }

  def flatMap[B](f: A => Distribution[B]): Distribution[B] = {
    Distribution(events flatMap {case (a, p1) => f(a).events map { case (b, p2) => b -> (p1 * p2) }}).compact.normalize
  }

  def normalize: Distribution[A] = {
    val totalWeight = (events map {case (a, p) => p }).sum
    Distribution(events map { case (a,p) => a -> (p / totalWeight) })
  }

  def compact: Distribution[A] = {
    val distinct = (events map {case (a, p) => a }).distinct
    def prob (a: A): Double = (events filter { case (x, p) => x == a } map { case (a, p) => p }).sum
    Distribution(distinct map { a => a -> prob (a) })
  }
}

object Distribution {

  def uniform[A](items: List[A]): Distribution[A] = {
    val p = 1.0 / items.length
    Distribution(items.map(i => (i, p)))
  }

  def discrete[A](events: List[(A,Double)]): Distribution[A] = Distribution(events).compact.normalize

}
