import java.util.logging.Level
// "Object" means multiple things in scala:
//  an instance of a class
//  a singleton object


// object keyword creates singleton Logger
object Logger {
  def log(level: Level, string: String): Unit = {
    printf("%s %s%n", level, string)
  }
}

Logger.log(Level.INFO, "singleton logger")





// Companion Objects are similar to java statics inside classes
//  Scala splits the "static" content into the object and the non-static content into the class
//  and terms it companion objects
// useful for: factory methods
// privates can be accessed across companion objects because they are defined in the same file

// private constructor forces use of Customer object factory (apply)
class Customer private(val name: String, val address: String) {
  val id = Customer.nextId()
}

object Customer {
  // "apply" is a special method that will be located automatically based on signature
  def apply(name: String, address: String) = new Customer(name, address)

  private var sequenceOfIds = 0

  private def nextId() = {
    sequenceOfIds += 1
    sequenceOfIds
  }
}

val c1 = new Customer("c1", "addr1")
c1.id

val c2 = new Customer("c2", "addr2")
c2.id

val c3 = Customer("found", "using apply method")