// Functions are objects, but methods are not
// functions can be stored in vals or vars, methods cannot
// functions have slight overhead associated with being objects
// functions cannot accept type params or have param default values, methods can
// methods must exist inside classes (REPL works around this)

// Functions are first-class:
// - they can be stored in a val/var
// - the return value of a function can be a function
// - the parameter of a function can be a function

// this is a method
def areaMethod (radius:Double) :Double = {
  val pi = 3.14
  pi * radius * radius
}

// this is a function
val areaFunction = (radius:Double) => {
  val pi = 3.14
  pi * radius * radius
} :Double

s"areaMethod = ${areaMethod(10)}"
s"areaFuction = ${areaFunction(10)}"


// methods cannot be assigned:
//val calcByMethod = areaMethod // compile error

// methods can be assigned by wrapping with a function:
val calcByWrappedMethod: (Double) => Double = areaMethod
s"calcByWrappedMethod = ${calcByWrappedMethod(10)}"

// same thing can be done using the _ shortcut
// _ shortcut takes whatever param and return types from the right and uses them to
// create the function on the left
val calcByWrappedMethod2 = areaMethod _
s"calcByWrappedMethod2 = ${calcByWrappedMethod2(10)}"

// _ shortcut does not work when methods return functions/closures
def areaMethodClosure = {
  val pi = 3.14
  val getArea = (radius:Double) => {
    pi * radius * radius
  } :Double
  getArea
}

val calcByWrappedMethod3: (Double) => Double = areaMethodClosure
s"calcByWrappedMethod3 = ${calcByWrappedMethod3(10)}"

val calcByWrappedMethod4 = areaMethodClosure _
//calcByWrappedMethod4(10) // compile error: too many arguments
