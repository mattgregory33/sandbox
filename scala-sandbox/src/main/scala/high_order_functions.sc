val compareAsc = (s1:String, s2:String) => {
  if (s1 == s2) 0
  else if (s1 > s2) 1
  else -1
} :Int

compareAsc("abc", "abc")
compareAsc("abc", "xyz")
compareAsc("xyz", "abc")


val compareDesc = (s1:String, s2:String) => {
  if (s2 == s1) 0
  else if (s2 > s1) 1
  else -1
} :Int

compareDesc("abc", "abc")
compareDesc("abc", "xyz")
compareDesc("xyz", "abc")

// high-order function takes a function as a param:
val smartCompare = (s1:String, s2:String, cmpFn:(String, String) => Int) => {
  cmpFn(s1, s2)
} :Int

smartCompare("abc", "abc", compareAsc)
smartCompare("abc", "abc", compareDesc)
smartCompare("abc", "xyz", compareAsc)
smartCompare("abc", "xyz", compareDesc)
smartCompare("xyz", "abc", compareAsc)
smartCompare("xyz", "abc", compareDesc)


// function that returns a function
val getComparator = (asc:Boolean) => {
  if (asc) compareAsc
  else compareDesc
} :(String, String) => Int

val ascCompare = getComparator(true)
val descCompare = getComparator(false)

ascCompare("abc", "abc")
ascCompare("abc", "xyz")
ascCompare("xyz", "abc")

descCompare("abc", "abc")
descCompare("abc", "xyz")
descCompare("xyz", "abc")


// a partially applied function can expose alternative signatures:
val defaultCompare = smartCompare(_:String, _:String, compareAsc)

defaultCompare("abc", "abc")
defaultCompare("abc", "xyz")
defaultCompare("xyz", "abc")


// scala allows method params to be grouped:
// (why can't we define this as a function?)
def curriedCompare(cmpFn:(String, String) => Int) (s1:String, s2:String): Int = {
  cmpFn(s1, s2)
}

curriedCompare(compareAsc)("abc", "abc")
curriedCompare(compareAsc)("abc", "xyz")
curriedCompare(compareAsc)("xyz", "abc")
curriedCompare(compareDesc)("abc", "abc")
curriedCompare(compareDesc)("abc", "xyz")
curriedCompare(compareDesc)("xyz", "abc")

// param grouping supports currying:
val defaultCurriedCompare = curriedCompare(compareAsc)(_:String, _:String)
defaultCurriedCompare("abc", "xyz")
// curriedCompare(compareAsc)_ also works