val radius = 10

val area = {
  val PI = 3.1
  println(s"outer scope, PI=$PI");
  // new expression block creates new variable scope
  {
    val PI = 3.14
    println(s"inner scope, PI=$PI")
    PI * radius * radius
  }
  // comment this line out and the inner scope result will be returned
  PI * radius * radius
}

println(area)


