
val weekDays = List("Mon", "Tue", "Wed", "Thu", "Fri")


// foreach runs the given function for each element in the collection
weekDays.foreach(println(_))


// map returns a list of the results of each execution of the given function
weekDays.map(_ == "Wed")
weekDays.map(_.toUpperCase)

val isMonday = (x:String) => {x == "Mon"}:Boolean
weekDays.map(isMonday) // _ optional since only single param and single signature

// filter uses a predicate function and returns a List where predicate resolved to true
weekDays.filter(isMonday)

// partition returns a tuple of Lists where the predicate resolves to true and false
weekDays.partition(isMonday)


// sortBy returns a list of elements sorted by given criteria function
val sorted = weekDays.sortBy(_(0)) // sort by 1st (0th) character

// note new lists are returned - weekDays is unchanged
sorted
weekDays


// scan
val someNumbers = List(10,20,30,40,50,60)

// _ - _ function = operand1 - operand2
// 0 - the initial value
// scanRight operates from the end to the beginning of the collection (right to left) (right associative)
someNumbers.scanRight(0)(_ - _)

// scanLeft is same as scanRight but left associative
someNumbers.scanLeft(0)(_ - _)



// fold
// fold works like scan but returns only the last element instead of the entire list
someNumbers.foldRight(0)(_ - _)
someNumbers.foldLeft(0)(_ - _)


// reduce
// similar to fold but instead of accepting an intial value the first two elements are used
someNumbers.reduceRight(_ - _)
someNumbers.reduceLeft(_ - _)
