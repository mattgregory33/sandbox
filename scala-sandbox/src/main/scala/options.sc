// used to indicate absence/presence of value
// if Option has a value it will be an instance of Some
// if Option has no value it will be an instance of None


// [] notation is like <> Java generics
def fraction(numer:Double, denom:Double): Option[Double] = {
  if (denom == 0)
    None
  else
    Option(numer/denom)
}

fraction(53,9)
fraction(6,0)


// getOrElse leverages Options to do one thing or another depending on Some/None
val pi = fraction(22,7) getOrElse "oops"
val pi2 = fraction(22,0) getOrElse "oops"


// options can also be pattern matched
fraction(22,7) match {
  case Some(pi) => pi
  case None => "oops"
}

fraction(22,0) match {
  case Some(pi) => pi
  case None => "oops"
}



