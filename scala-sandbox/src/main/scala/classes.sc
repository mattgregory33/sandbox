class Person_v1(firstName: String, initial: String, lastName: String) {
  val fullName = if (initial != null && !initial.isEmpty)
    String.format("%s %s. %s", firstName, initial, lastName)
  else
    String.format("%s %s", firstName, lastName)

  // this() defines alternate constructor also using this() to call primary constructor
  def this(firstName: String, lastName: String) {
    this(firstName, "", lastName)
  }
}

val homer = new Person_v1("Homer", "J", "Simpson")
homer.fullName

val marge = new Person_v1("Marge", "Simpson")
marge.fullName



// can avoid extra constructor from v1 by providing default value for initial
class Person_v2(firstName: String, initial: String = "", lastName: String) {
  val fullName = if (initial != null && !initial.isEmpty)
    String.format("%s %s. %s", firstName, initial, lastName)
  else
    String.format("%s %s", firstName, lastName)
}

val poe = new Person_v2("Edgar", "A", "Poe")
poe.fullName

// must specify param name for (at least) lastName
val will = new Person_v2("William", lastName="Shakespeare")
will.fullName
