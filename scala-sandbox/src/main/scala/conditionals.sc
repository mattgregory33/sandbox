// if:then
// same as Java

val age = 33
if (age > 55) {
  println("retire")
} else {
  println("keep working")
}

// braces unnecessary
if (age > 55)
  println("retire")
else
  println("keep working")

// even one-liner
// if:then is an expression (returns a value) not a statement (carries out an action)
// equivalent to java ternaries
if (age > 55) println("retire") else println("keep working")

val outcome = if (age > 55) "retire" else "keep working"
println(outcome)

// Nothing is returned if if resolves false and there is no else, denoted by ()
if (false) println("no way")




// match (switch)
// because of pattern matching, scala can match on almost anything, including objects
def switch(month: String): String = {
  val quarter = month match {
    case "Jan" | "Feb" | "Mar" => "1st"
    case "Apr" | "May" | "Jun" => "2nd"
    case "Jul" | "Aug" | "Sep" => "3rd"
    case "Oct" | "Nov" | "Dec" => "4th"
    case _ => "unknown"
  }
  quarter
}
println(switch("Jan"))
println(switch("Dec"))
println(switch("Sep"))
println(switch("Jun"))
println(switch("sandwiches"))