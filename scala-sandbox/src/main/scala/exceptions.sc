import java.io.{BufferedReader, IOException, InputStreamReader}
import java.net.{MalformedURLException, URL}
// All exceptions in scala are RuntimeExceptions
// catching exceptions uses pattern matching

try {
  val url = new URL("http://badurl.com")
  val reader = new BufferedReader(new InputStreamReader(url.openStream()))
  try {
    var line = reader.readLine()
    while (line != null) {
      line = reader.readLine()
      println(line)
    }
  } finally { // no try with resources in scala (see "loan" pattern)
    reader.close()
  }
} catch {
  case e: MalformedURLException => println("Bad URL")
  case e: IOException => println("Problem reading data: " + e.getMessage)
}
