package com.mgregory.sandbox.scala.essential.randomwords

import org.scalatest.FlatSpec

class RandomTextTest extends FlatSpec {

  val subjects = List("Noel", "The cat", "The dog")

  val generator = new RandomText(subjects)

  "generateText" should "form sentences with all combinations of subject, verb, object" in {
    for (s <- generator.generateText) println(s)
  }

}
