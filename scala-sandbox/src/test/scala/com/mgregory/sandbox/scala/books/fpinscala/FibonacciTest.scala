package com.mgregory.sandbox.scala.books.fpinscala

import com.mgregory.sandbox.scala.books.fpinscala.HighOrderFunctions._
import org.scalatest.{FlatSpec, Matchers}

class FibonacciTest extends FlatSpec with Matchers {

  "fib" should "return nth fibonacci number" in {
    fib(0) should equal(0)
    fib(1) should equal(1)
    fib(2) should equal(1)
    fib(3) should equal(2)
    fib(4) should equal(3)
    fib(20) should equal(6765)
  }

  "isSorted" should "return whether Ints are sorted" in {
    val asc: (Int, Int) => Boolean = (x: Int, y: Int) => x < y
    isSorted(Array(1), asc) should equal(true)
    
    isSorted(Array(1,2), asc) should equal(true)
    isSorted(Array(1,2,3), asc) should equal(true)
    isSorted(Array(1,2,4), asc) should equal(true)

    isSorted(Array(2,1), asc) should equal(false)
    isSorted(Array(2,1,3), asc) should equal(false)
    isSorted(Array(1,2,0), asc) should equal(false)
  }
  
  it should "return whether Strings are sorted" in {
    val asc: (String, String) => Boolean = (x: String, y: String) => x < y
    isSorted(Array("a"), asc) should equal(true)

    isSorted(Array("a","b"), asc) should equal(true)
    isSorted(Array("a","b","c"), asc) should equal(true)
    isSorted(Array("a","b","d"), asc) should equal(true)

    isSorted(Array("b","a"), asc) should equal(false)
    isSorted(Array("b","a","c"), asc) should equal(false)
    isSorted(Array("a","b","_"), asc) should equal(false)
  }

  it should "return whether Strings are sorted by length" in {
    val asc: (String, String) => Boolean = (x: String, y: String) => x.length < y.length
    isSorted(Array("a"), asc) should equal(true)

    isSorted(Array("a","aa"), asc) should equal(true)
    isSorted(Array("a","aa","aaa"), asc) should equal(true)
    isSorted(Array("a","aa","aaaa"), asc) should equal(true)

    isSorted(Array("aa","a"), asc) should equal(false)
    isSorted(Array("aa","a","aaa"), asc) should equal(false)
    isSorted(Array("a","aa",""), asc) should equal(false)
  }

  "curry" should "work" in {
    val curried = curry((whole: Int, frac: Float) => s"$whole and a $frac")
    curried(1)(.5f) should equal("1 and a 0.5")
  }

  "uncurry" should "work" in {
    val uncurried = uncurry((whole: Int) => (frac: Float) => s"$whole and a $frac")
    uncurried(1, 0.5f) should equal ("1 and a 0.5")
  }

  "compose" should "work" in {
    val composed = compose((float: Float) => float.toString, (int: Int) => int/2F)
    composed(1) should equal ("0.5")
  }
}
