package com.mgregory.sandbox.scala.filesearcher

import java.io.File

import org.scalatest.FlatSpec

class MatcherTests extends FlatSpec {

  "Matcher that is passed a file matching the filter" should
  "return a list with a that file name" in {
    val matcher = new Matcher("fake", "fakePath")
    val results = matcher.execute()
    assert(results == List(("fakePath", None)))
  }

  "Matcher using a directory containing one file matching the filter" should
  "return a list with that file name" in {
    val matcher = new Matcher("txt", new File("src/test/resources/case_onefile").getCanonicalPath)
    val results = matcher.execute()
    assert(results == List(("file.txt", None)))
  }

  "Matcher that is not passed a root file location" should
  "use the current location" in {
    val matcher = new Matcher("filter")
    assert(matcher.rootLocation == new File(".").getCanonicalPath)
  }

  "Matcher given a path that has one file that matches file filter and content filter" should
  "return a list with that file name" in {
    val matcher = new Matcher("data", new File("src").getCanonicalPath, true, Some("pluralsight"))
    val matchedFiles = matcher.execute()
    assert(matchedFiles == List(("file_containing_pluralsight.data", Some(2))))
  }

  "Matcher given a path that has no file that matches file filter and content filter" should
  "return an empty list" in {
    val matcher = new Matcher("txt", new File("src").getCanonicalPath, true, Some("pluralsight"))
    val matchedFiles = matcher.execute()
    assert(matchedFiles isEmpty)
  }

}
