package com.mgregory.sandbox.scala.essential.generics.types

import org.scalatest.{FlatSpec, Matchers}

class SumTest extends FlatSpec with Matchers {

  "A Sum" should "hold either a left or right value" in {
    Failure[Int](1).value should equal (1)

    Success[String]("foo").value should equal ("foo")
  }

  it should "support pattern matching" in {
    val sum: Sum[Int, String] = Success("foo")

    (sum match {
      case Failure(x) => x
      case Success(x) => x
    }) should equal ("foo")
  }

  it should "be foldable" in {
    val failure = Failure(1.0)
    val success = Success("success")

    failure.fold[String]((x:Double) => x.toString, (x:String) => x.reverse) should equal ("1.0")
    success.fold[String]((x:Double) => x.toString, (x:String) => x.reverse) should equal ("sseccus")
  }

  "Sum.map" should "convert the Sum[A,B] to a Sum[A,C]" in {
    val s = Success(1)
    s.map(s => s * 2) should equal (Success(2))
  }

  it should "just return an identical Failure in the case of Failure" in {
    val f = Failure(2)
    f.map(f => "failed") should equal (Failure(2))
  }

  "Sum.flatMap" should "convert the Sum[A,B] to a Sum[A,C]" in {
    val s = Success(1)
    s.flatMap(s => Success(s * 2)) should equal (Success(2))
  }

  it should "just return an identical Failure in the case of Failure" in {
    val f = Failure(2)
    f.flatMap(f => Success("failed")) should equal (Failure(2))
  }
}
