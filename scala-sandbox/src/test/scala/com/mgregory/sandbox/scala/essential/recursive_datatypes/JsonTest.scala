package com.mgregory.sandbox.scala.essential.recursive_datatypes

import org.scalatest.{FlatSpec, Matchers}

class JsonTest extends FlatSpec with Matchers {

  "A Json String" should "print with quotes wrapped" in {
    JsonString("a").toString() should be ("\"a\"")
  }

  "A Json Double" should "print with one significant digit" in {
    JsonDouble(1).toString() should be ("1.0")
  }

  "A Json Int" should "print the integer value" in {
    JsonInt(1).toString() should be ("1")
  }

  "A Json Bool" should "print true or false" in {
    JsonBool(true).toString should be ("true")
    JsonBool(false).toString should be ("false")
  }

  "A Json Array" should "print wrapped in brackets with each element separated by commas" in {
    JsonArray(Seq(JsonString("a"), JsonString("b"))).toString should be ("[\"a\",\"b\"]")
  }

  it should "not print a comma if only one element" in {
    JsonArray(Seq(JsonString("a"))).toString should be ("[\"a\"]")
  }

  it should "print [] if no elmeents" in {
    JsonArray(Seq()).toString should be ("[]")
  }

  it should "print each element formatted correctly based on its type" in {
    JsonArray(Seq(JsonString("ab"), JsonDouble(1), JsonInt(2), JsonBool(true))).toString() should be ("[\"ab\",1.0,2,true]")
  }

  it should "print nested arrays" in {
    JsonArray(Seq(JsonInt(1), JsonArray(Seq(JsonString("a"), JsonInt(2), JsonBool(false))), JsonDouble(1.6))).toString should be ("[1,[\"a\",2,false],1.6]")
  }

  it should "handle embedded objects" in {
    JsonArray(Seq(JsonInt(1), JsonObject(Map("a" -> JsonInt(1))))).toString() should be ("[1,{\"a\":1}]")
  }

  "A Json Object" should "print colon-delimited key/value pairs wrapped in {} with keys wrapped in quotes" in {
    JsonObject(Map("a" -> JsonInt(1))).toString() should be ("{\"a\":1}")
  }

  it should "handle multiple key/value pairs with different types" in {
    JsonObject(Map("a" -> JsonString("a"), "b" -> JsonInt(2), "c" -> JsonDouble(3), "d" -> JsonBool(true))).toString should be
    ("""{"a":"a","b":2,"c":3.0,"d":true}""")
  }

  it should "handle embedded arrays" in {
    JsonObject(Map("int" -> JsonInt(7), "arr" -> JsonArray(Seq(JsonInt(1),JsonDouble(2),JsonBool(false))))).toString should be ("""{"int":7,"arr":[1,2.0,false]}""")
  }
}
