package com.mgregory.sandbox.scala.books.fpinscala.errorhandling

import com.mgregory.sandbox.scala.books.fpinscala.errorhandling.Option.{map2, sequence, traverse}
import org.scalatest.{FlatSpec, Matchers}

import scala.util.Try

class OptionTest extends FlatSpec with Matchers {

  "map" should "convert the contained type according to the given function" in {
    def even(x: Int): Boolean = x % 2 == 0

    None map even should equal(None)
    Some(1) map even should equal(Some(false))
    Some(2) map even should equal(Some(true))
  }

  "flatMap" should "convert the option using the given function" in {
    None flatMap { _ => None} should equal(None)
    None flatMap { _ => Some(1)} should equal(None)
    Some(1) flatMap {x => Some(x+1)} should equal(Some(2))
    Some(1) flatMap { _ => None} should equal(None)
  }

  "getOrElse" should "return the contained value or the given default if None" in {
    None getOrElse 1 should equal(1)
    Some(1) getOrElse 2 should equal(1)
  }

  "orElse" should "return the Option or the given Option if None" in {
    None orElse Some(1) should equal(Some(1))
    Some(1) orElse None should equal(Some(1))
    Some(1) orElse Some(2) should equal(Some(1))
  }

  "filter" should "return the Option if the given predicate passes" in {
    None filter {_ => true} should equal(None)
    None filter {_ => false} should equal(None)
    Some(1) filter {x => x > 0} should equal(Some(1))
    Some(1) filter {x => x < 0} should equal(None)
  }

  "map2" should "perform the given function if neither argument is None" in {
    map2(None, None)((_, _) => 1) == None
    map2(None, Some(1))((_, _) => 1) == None
    map2(Some(1), None)((_, _) => 1) == None
    map2(Some(1), Some(1))((a, b) => a+b) == Some(2)
  }

  "sequence" should "take a list of Options and return an Option List of all the Somes" in {
//    sequence(Nil) should equal(None)
    sequence(List(None)) should equal(None)
    sequence(List(None, None)) should equal(None)
    sequence(List(None, Some(1))) should equal(None)
    sequence(List(Some(1), None)) should equal(None)
    sequence(List(Some(1))) should equal(Some(List(1)))
    sequence(List(Some(1), Some(2))) should equal(Some(List(1,2)))
  }

  "traverse" should "map the elements with the given function and return the Somes in an option" in {
//    traverse(Nil)(_ => None) should equal(None)
//    traverse(Nil)(_ => Some(1)) should equal(None)
    traverse(List("1"))(a => try {Some(a.toInt)} catch {case _: Exception => None}) should equal(Some(List(1)))
    traverse(List("a"))(a => try {Some(a.toInt)} catch {case _: Exception => None}) should equal(None)
  }

  "lift" should "wrap an existing function in an Option" in {
    Option.lift(math.abs)(None) should equal(None)
    Option.lift(math.abs)(Some(0)) should equal(Some(0))
    Option.lift(math.abs)(Some(-1)) should equal(Some(1))
  }
}
