package com.mgregory.sandbox.scala.essential.extractors

import org.scalatest.FlatSpec

class TitlecaseTest extends FlatSpec {

  "unapply" should "convert the input to title case" in {
    assert ("Sir Lord Doctor David Gurnell" == (
      "sir lord doctor david gurnell" match {
        case Titlecase(str) => str
      })
    )
  }
}
