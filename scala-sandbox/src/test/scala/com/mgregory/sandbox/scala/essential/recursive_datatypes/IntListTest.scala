package com.mgregory.sandbox.scala.essential.recursive_datatypes

import org.scalatest.FlatSpec

class IntListTest extends FlatSpec {

  val example = Pair(1, Pair(2, Pair(3, End)))

  "An IntList" should "have a length equal to the number of Pairs in the structure" in {
    assert(example.length == 3)
    assert(example.tail.length == 2)
    assert(End.length == 0)
  }

  it should "have a product equal to the product of all the Pair heads in the structure" in {
    assert(example.product == 6)
    assert(example.tail.product == 6)
    assert(End.product == 1)
  }

  it should "be able to double each head value in the structure" in {
    assert (example.double == Pair( 2 , Pair( 4 , Pair( 6 , End))))
    assert (example.tail.double == Pair( 4 , Pair( 6 , End)))
    assert (End.double == End)
  }
}
