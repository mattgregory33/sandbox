package com.mgregory.sandbox.scala.essential.recursive_datatypes

import com.mgregory.sandbox.scala.essential.common.{Failure, Success}
import org.scalatest.FlatSpec

class ExpressionTest extends FlatSpec {

  "Number eval" should "return its value" in {
    assert(Number(0).eval == Success(0))
    assert(Number(1).eval == Success(1))
  }

  "Addition eval" should "return the sum of the nodes of the left and right trees" in {
    assert(Addition(Number(0), Number(0)).eval == Success(0))
    assert(Addition(Number(1), Number(1)).eval == Success(2))
    assert(Addition(Addition(Number(1), Number(1)), Addition(Number(1), Number(1))).eval == Success(4))
  }

  it should "handle negatives" in {
    assert(Addition(Number(-1), Number(0)).eval == Success(-1))
    assert(Addition(Number(-1), Number(1)).eval == Success(0))
    assert(Addition(Number(-1), Number(2)).eval == Success(1))
    assert(Addition(Number(1), Number(-1)).eval == Success(0))
  }

  "Subtraction eval" should "return the difference of the nodes of the left and right trees" in {
    assert(Subtraction(Number(0), Number(0)).eval == Success(0))
    assert(Subtraction(Number(1), Number(0)).eval == Success(1))
    assert(Subtraction(Number(1), Number(1)).eval == Success(0))
    assert(Subtraction(Number(2), Number(1)).eval == Success(1))
    assert(Subtraction(Subtraction(Number(4), Number(2)), Subtraction(Number(4), Number(2))).eval == Success(0))
  }

  it should "handle negatives" in {
    assert(Subtraction(Number(0), Number(1)).eval == Success(-1))
    assert(Subtraction(Number(1), Number(2)).eval == Success(-1))
  }

  "Division eval" should "return the quotient of the two sides" in {
    assert(Division(Number(4), Number(2)).eval == Success(2))
  }

  it should "fail with Division by zero error if the right side is zero" in {
    assert(Division(Number(4), Number(0)).eval == Failure("Division by zero"))
  }

  "Square root eval" should "return the square root of the input" in {
    assert(SquareRoot(Number(4)).eval == Success(2))
  }

  it should "fail when taking the square root of a negative number" in {
    assert(Addition(SquareRoot(Number(-1.0)), Number(2.0)).eval == Failure("Square root of negative number"))
  }
}
