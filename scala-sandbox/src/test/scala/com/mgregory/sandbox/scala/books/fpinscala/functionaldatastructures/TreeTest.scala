package com.mgregory.sandbox.scala.books.fpinscala.functionaldatastructures

import org.scalatest.{FlatSpec, Matchers}

class TreeTest extends FlatSpec with Matchers {

  "size" should "return the number of nodes (leaves and branches) in the tree" in {
    Tree.size(Leaf(1)) should equal(1)
    Tree.size(Branch(Leaf(1), Leaf(1))) should equal(3)
    Tree.size(Branch(Leaf(1), Branch(Leaf(2), Leaf(2)))) should equal(5)
    Tree.size(Branch(Branch(Leaf(1), Leaf(1)), Branch(Leaf(1), Leaf(1)))) should equal(7)
    Tree.size(Branch(Branch(Leaf(1), Leaf(1)), Branch(Leaf(1), Branch(Leaf(2), Leaf(2))))) should equal(9)
  }

  "maximum" should "return the highest value in a tree of Ints" in {
    Tree.maximum(Leaf(1)) should equal(1)
    Tree.maximum(Branch(Leaf(1), Leaf(1))) should equal(1)
    Tree.maximum(Branch(Leaf(1), Leaf(2))) should equal(2)
    Tree.maximum(Branch(Leaf(2), Leaf(1))) should equal(2)
    Tree.maximum(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Branch(Leaf(4), Leaf(5))))) should equal(5)
    Tree.maximum(Branch(Branch(Leaf(5), Leaf(4)), Branch(Leaf(3), Branch(Leaf(2), Leaf(1))))) should equal(5)
    Tree.maximum(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Branch(Leaf(2), Leaf(1))))) should equal(3)
  }

  "depth" should "return the maximum path length from the root to any leaf" in {
    Tree.depth(Leaf(1)) should equal(1)
    Tree.depth(Branch(Leaf(1), Leaf(1))) should equal(2)
    Tree.depth(Branch(Branch(Leaf(1), Leaf(1)), Leaf(1))) should equal(3)
    Tree.depth(Branch(Leaf(1), Branch(Leaf(1), Leaf(1)))) should equal(3)
  }

  "map" should "perform the given function on each leaf in the tree" in {
    val even: Int => Boolean = x => x % 2 == 0
    Tree.map(Leaf(1))(even) should equal(Leaf(false))
    Tree.map(Leaf(2))(even) should equal(Leaf(true))
    Tree.map(Branch(Leaf(2), Leaf(3)))(even) should equal(Branch(Leaf(true), Leaf(false)))
    Tree.map(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Branch(Leaf(2), Leaf(1)))))(even) should equal(
      Branch(Branch(Leaf(false), Leaf(true)), Branch(Leaf(false), Branch(Leaf(true), Leaf(false))))
    )
  }
}
