package com.mgregory.sandbox.scala.essential.recursive_datatypes

import org.scalatest.FlatSpec

class TreeTest extends FlatSpec {

    val t1 = Node(Leaf(1), Leaf(1))
    val t2 = Node(Leaf(1), Node(Leaf(1), Leaf(1)))

  "Summing a Tree" should "return the sum of all Leafs" in {
    assert(t1.sum == 2)
    assert(t2.sum == 3)
  }

  it should "also return the same result polymorphically" in {
    assert(t1.pSum == 2)
    assert(t2.pSum == 3)
  }

  it should "also return the same result via object" in {
    assert(TreeOps.sum(t1) == 2)
    assert(TreeOps.sum(t2) == 3)
  }

  "Doubling a Tree" should "return a tree with all Leaf nodes doubled" in {
    assert(t1.double == Node(Leaf(2), Leaf(2)))
    assert(t2.double ==  Node(Leaf(2), Node(Leaf(2), Leaf(2))))
  }

  it should "return the same result polymorphically" in {
    assert(t1.pDouble == Node(Leaf(2), Leaf(2)))
    assert(t2.pDouble ==  Node(Leaf(2), Node(Leaf(2), Leaf(2))))
  }

  it should "return the same result via object" in {
    assert(TreeOps.double(t1) == Node(Leaf(2), Leaf(2)))
    assert(TreeOps.double(t2) ==  Node(Leaf(2), Node(Leaf(2), Leaf(2))))
  }
}
