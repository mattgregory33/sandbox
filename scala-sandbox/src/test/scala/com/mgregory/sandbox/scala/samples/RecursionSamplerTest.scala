package com.mgregory.sandbox.scala.samples

import com.mgregory.sandbox.scala.samples.RecursionSampler.Nested
import org.scalatest.{FlatSpec, Matchers}

class RecursionSamplerTest extends FlatSpec with Matchers {

  "allNestedValues" should "return empty list given ()" in {
    RecursionSampler.allNestedValues(Nested(Nil, Nil)) shouldBe empty
  }

  it should "handle no nesting" in {
    RecursionSampler.allNestedValues(Nested(List(1), Nil)) should contain (1)
    RecursionSampler.allNestedValues(Nested(List(1,2,3), Nil)) should contain allOf (1,2,3)
  }

  it should "handle one level of nesting" in {
    RecursionSampler.allNestedValues(Nested(List(1), List(Nested(List(2), Nil)))) should contain allOf (1,2)
    RecursionSampler.allNestedValues(Nested(List(1,2,3), List(Nested(List(4,5,6), Nil)))) should contain allOf (1,2,3,4,5,6)
    RecursionSampler.allNestedValues(Nested(Nil, List(Nested(List(1), Nil)))) should contain (1)
    RecursionSampler.allNestedValues(Nested(Nil, List(Nested(List(1,2), Nil)))) should contain allOf (1,2)
  }

  it should "handle multiple levels of nesting" in {
    RecursionSampler.allNestedValues(Nested(List(1), List(Nested(List(2), List(Nested(List(3), Nil)))))) should contain allOf (1,2,3)
    RecursionSampler.allNestedValues(Nested(Nil, List(Nested(Nil, List(Nested(List(1), Nil)))))) should contain (1)
    RecursionSampler.allNestedValues(Nested(List(1,2,3), List(Nested(List(4,5,6), List(Nested(List(7,8,9), Nil)))))) should contain allOf (1,2,3,4,5,6,7,8,9)
  }

  it should "handle complex nesting" in {
    RecursionSampler.allNestedValues(
      Nested(List(1), List(
        Nested(Nil, Nil),
        Nested(List(2), List(
          Nested(Nil, Nil),
          Nested(List(3), Nil)
        ))
      ))) should contain allOf (1,2,3)
  }
}
