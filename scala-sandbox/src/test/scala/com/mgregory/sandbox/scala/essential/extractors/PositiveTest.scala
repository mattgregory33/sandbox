package com.mgregory.sandbox.scala.essential.extractors

import org.scalatest.FlatSpec

class PositiveTest extends FlatSpec {

  "unapply" should "only match positive integers" in {
    assert ( "No" == (
      0 match {
        case Positive(_) => "Yes"
        case _ => "No"
      })
    )

    assert ( "Yes" == (
      42 match {
        case Positive (_) => "Yes"
        case _ => "No"
      })
    )
  }
}
