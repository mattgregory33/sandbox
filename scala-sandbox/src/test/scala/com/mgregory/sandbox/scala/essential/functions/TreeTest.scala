package com.mgregory.sandbox.scala.essential.functions

import org.scalatest.{FlatSpec, Matchers}

class TreeTest extends FlatSpec with Matchers {

  val tree: Tree[String] = Node(Node( Leaf ( "To" ), Leaf ( "iterate" )), Node(Node( Leaf ( "is" ), Leaf ( "human," )), Node( Leaf ( "to" ), Node( Leaf ( "recurse" ), Leaf ( "divine" )))))

  "Folding the tree with string concatenation" should "produce the sentence" in {
    val folded = tree.fold[String]((a) => a, (a,b) => a + " " + b)
    folded should be ("To iterate is human, to recurse divine")
  }

  "Counting leaves in the tree" should "equal 7" in {
    val folded = tree.fold[Int]((a) => 1, (a, b) => a + b)
    folded should be (7)
  }

  "Misc other folds" should "work" in {
    println(tree.fold[String]((a) => a.reverse, (a,b) => s"$a $b"))
    println(tree.fold[String]((a) => a, (a,b) => b + a))
  }
}
