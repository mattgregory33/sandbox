package com.mgregory.sandbox.scala.books.fpinscala.functionaldatastructures

import com.mgregory.sandbox.scala.books.fpinscala.List._
import com.mgregory.sandbox.scala.books.fpinscala.{List, Nil}
import org.scalatest.{FlatSpec, Matchers}

class ListTest extends FlatSpec with Matchers {

  "sum" should "return the sum of the elements in the list" in {
    sum(Nil) should equal(0)
    sum(List(1)) should equal(1)
    sum(List(1,2)) should equal(3)
  }

  "sumLeft" should "return the sum of the elements in the list" in {
    sumLeft(Nil) should equal(0)
    sumLeft(List(1)) should equal(1)
    sumLeft(List(1,2)) should equal(3)
  }

  "product" should "return the product of the elements in the list" in {
    product(Nil) should equal(1.0)
    product(List(1.0)) should equal(1.0)
    product(List(1.0,2.0)) should equal(2.0)
  }

  "productLeft" should "return the product of the elements in the list" in {
    productLeft(Nil) should equal(1.0)
    productLeft(List(1.0)) should equal(1.0)
    productLeft(List(1.0,2.0)) should equal(2.0)
  }

  "tail" should "return Nil for a Nil List" in {
    tail(Nil) should equal(Nil)
  }

  it should "return Nil of a List with a single item" in {
    tail(List(1)) should equal(Nil)
  }

  it should "return the tail of a non-empty List" in {
    tail(List(1,2)) should equal(List(2))
  }

  "setHead" should "return Nil when called on a Nil list" in {
    setHead(Nil, 1) should equal(List(1))
  }

  it should "return a list with the new item when called on a single-element list" in {
    setHead(List(1), 2) should equal(List(2))
  }

  it should "return a list with the new head when called on a multi-element list" in {
    setHead(List("a", "b"), "c") should equal(List("c", "b"))
  }

  "drop" should "return Nil for a Nil list" in {
    drop(Nil, 0) should equal(Nil)
    drop(Nil, 1) should equal(Nil)
  }

  it should "return Nil when n meets or exceeds elements in the list" in {
    drop(List(1), 1) should equal(Nil)
    drop(List(1,2), 2) should equal(Nil)
    drop(List(1), 2) should equal(Nil)
    drop(List(1,2), 3) should equal(Nil)
  }

  it should "return the tail when n is less than elements in the list" in {
    drop(List(1), 0) should equal(List(1))
    drop(List(1,2), 1) should equal(List(2))
    drop(List(1,2,3), 1) should equal(List(2,3))
    drop(List(1,2,3), 2) should equal(List(3))
  }

  "dropWhile" should "return Nil for a Nil list" in {
    dropWhile(Nil)((x: Int) => x < 3) should equal(Nil)
  }

  it should "return Nil when f is true for all elements in the list" in {
    dropWhile(List(1))(x => x < 3) should equal(Nil)
    dropWhile(List(1,2))(x => x < 3) should equal(Nil)
  }

  it should "return the tail when f encounters an element for which it is false" in {
    dropWhile(List(1))(x => x < 1) should equal(List(1))
    dropWhile(List(1,2))(x => x < 2) should equal(List(2))
  }

  "append" should "return Nil when appending two Nil lists" in {
    append(Nil, Nil) should equal(Nil)
  }

  it should "return the 2nd list when appending non-Nil to Nil" in {
    append(Nil, List(1)) should equal(List(1))
  }

  it should "return the 1st list when appending Nil to non-Nil" in {
    append(List(1), Nil) should equal(List(1))
  }

  it should "return the appended list when appending two non-Nil lists" in {
    append(List(1), List(2)) should equal(List(1,2))
    append(List(1,2), List(3,4)) should equal(List(1,2,3,4))
  }

  "init" should "return Nil when called on a Nil list" in {
    init(Nil) should equal(Nil)
  }

  it should "return Nil when called on a single-element list" in {
    init(List(1)) should equal(Nil)
  }

  it should "return a list containing all but the last element when given a multi-element list" in {
    init(List(1,2)) should equal(List(1))
    init(List(1,2,3)) should equal(List(1,2))
  }

  "length" should "return the length of the list" in {
    len(Nil) should equal(0)
    len(List(1)) should equal(1)
    len(List(1,2)) should equal(2)
  }

  "reverse" should "return the list in reverse order" in {
    reverse(Nil) should equal(Nil)
    reverse(List(1)) should equal(List(1))
    reverse(List(1,2,3)) should equal(List(3,2,1))
  }

  "flatten" should "return a single list from nested lists" in {
    flatten(List(Nil)) should equal(Nil)
    flatten(List(List(1))) should equal(List(1))
    flatten(List(List(1,2,3))) should equal(List(1,2,3))
    flatten(List(List(1),List(2),List(3))) should equal(List(1,2,3))
  }

  "increment" should "increment each element of the list" in {
    increment(Nil) should equal(Nil)
    increment(List(1)) should equal(List(2))
    increment(List(1,2,3)) should equal(List(2,3,4))
  }

  "stringify" should "turn a List[Double] into a List[String]" in {
    stringify(Nil) should equal(Nil)
    stringify(List(1.0)) should equal(List("1.0"))
    stringify(List(1.0, 2.0)) should equal(List("1.0","2.0"))
  }

  "map" should "convert the objects of a list from A to B" in {
    map(Nil)(x => x) should equal(Nil)
    map(List(1))(_ + 1) should equal(List(2))
    map(List(1,2))(_ + 1) should equal(List(2,3))
    map(List(1.0, 2.0))(_.toString) should equal(List("1.0", "2.0"))
  }

  "filter" should "remove items from a list matching the given predicate" in {
    filter(Nil)(x => x) should equal(Nil)
    filter(List(1,2,3,4,5))(x => x % 2 == 0) should equal(List(2,4))
    filter(List(9,1,8,2,7,3,6,4,5))(x => x < 6) should equal(List(1,2,3,4,5))
  }

  "flatMap" should "map values in a list to multiple values in one list" in {
    flatMap(Nil)(x => List(x)) should equal(Nil)
    flatMap(List(1,2,3))(x => List(x,x)) should equal(List(1,1,2,2,3,3))
  }

  "addElems" should "add elements of two equal-length lists together" in {
    addElems(Nil, Nil) should equal(Nil)
    addElems(List(1,2,3), List(4,5,6)) should equal(List(5,7,9))
  }

  "zipWith" should "perform a function against elements of two equal-length lists" in {
    zipWith(Nil, Nil)((x: Any,y: Any) => x) should equal(Nil)
    zipWith(List(1,2,3), List(4,5,6))((a, b) => a + b) should equal(List(5,7,9))
  }

  "hasSubsequence" should "return true when sub is a subsequence of sub" in {
    hasSubsequence(List(1), List(1)) should equal(true)
    hasSubsequence(List(1,2,3), List(1)) should equal(true)
    hasSubsequence(List(1,2,3), List(1,2)) should equal(true)
    hasSubsequence(List(1,2,3), List(1,2,3)) should equal(true)
    hasSubsequence(List(1,2,3), List(2)) should equal(true)
    hasSubsequence(List(1,2,3), List(2,3)) should equal(true)
    hasSubsequence(List(1,2,3), List(3)) should equal(true)
  }

  it should "return false otherwise" in {
    hasSubsequence(Nil, Nil) should equal(false)
    hasSubsequence(Nil, List(1)) should equal(false)
    hasSubsequence(List(1,2,3), List(4)) should equal(false)
    hasSubsequence(List(1,2,3), List(1,3)) should equal(false)
  }
}
