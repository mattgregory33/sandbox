package com.mgregory.sandbox.scala.essential.generics.types

import org.scalatest.{FlatSpec, Matchers}

class MaybeTest extends FlatSpec with Matchers {

  "A Maybe" should "be able to hold an empty value" in {
    val nope: Maybe[Int] = Empty()
    assertDoesNotCompile("nope.value")
    nope should equal (Empty())
  }

  it should "be able to hold a value" in {
    val yep: Maybe[Int] = Full(1)
    yep should equal (Full(1))

    Full(2).value should equal (2)
  }

  it should "be foldable" in {
    val folder = Full(1)
    val folded = folder.fold[String]("", (x: Int) => x.toString)
    folded should equal ("1")
  }

  "Maybe.map" should "convert empty Maybe[A] into empty Maybe[B] given the conversion function" in {
    val empty = Empty()
    empty.map(a => 0) should equal(Empty())
  }

  it should "convert a full Maybe" in {
    val full = Full(2112)
    full.map(a => a.toString) should equal (Full("2112"))
  }

  "Maybe.flatMap" should "convert empty Maybe[A] into empty Maybe[B] given the conversion function" in {
    val empty = Empty()
    empty.flatMap(a => Full(0)) should equal(Empty())
  }

  it should "convert a full Maybe" in {
    val full = Full(2112)
    full.flatMap(a => Full(a.toString)) should equal (Full("2112"))
  }
}
