package com.mgregory.sandbox.scala.essential.generics.types

import org.scalatest.{FlatSpec, Matchers}

class PairTest extends FlatSpec with Matchers {

  "A Pair of String, Int" should "return the constructor values" in {
    val pair = Pair[String, Int]("Hi", 2)
    pair.one should equal ("Hi")
    pair.two should equal (2)
  }
}
