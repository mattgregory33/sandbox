package com.mgregory.sandbox.scala.essential.generics

import com.mgregory.sandbox.scala.essential.common.{Failure, Success}
import org.scalatest.{FlatSpec, Matchers}

class LinkedListTest extends FlatSpec with Matchers {

  "LinkedList length" must "equal the number of pairs" in {
    val example = Pair(1, Pair(2, Pair(3, End())))
    assert(example.length == 3)
    assert(example.tail.length == 2)
    assert(End().length == 0)
  }

  val example = Pair(1, Pair(2, Pair(3, End())))

  "LinkedList contains" must "return true if the given item equals any element in the list, false otherwise" in {
    assert(example.contains(3) == true)
    assert(example.contains(4) == false)
    assert(End().contains(0) == false)
  }

  it should "handle varying data types" in {
    assert(Pair("a", Pair("b", Pair("c", End()))).contains("c")) == true
    assert(Pair(1.0, End()).contains(1.0)) == true
  }

  "LinkedList apply" should "return the item at the given index from the list" in {
    assert (example(0) == Success(1))
    assert (example(1) == Success(2))
    assert (example(2) == Success(3))
  }

  it should "throw an exception if the requested index does not exist" in {
    assert(example(3) == Failure("Index out of bounds"))
    assert(example(-1) == Failure("Index out of bounds"))
  }

  "LinkedList.map" should "convert the LinkedList[A] to a LinkedList[B] using the given function" in {
    val list: LinkedList[Int] = Pair(1, Pair(2, Pair(3, End())))

    // double all elements in the list
    list.map(l => l * 2) should equal (Pair(2, Pair(4, Pair(6, End()))))

    // add one to all elements in the list
    list.map(l => l + 1) should equal (Pair(2, Pair(3, Pair(4, End()))))

    // divide by 3 all elements in the list
    list.map(l => l / 3) should equal (Pair(1/3, Pair(2/3, Pair(3/3, End()))))
  }
}
