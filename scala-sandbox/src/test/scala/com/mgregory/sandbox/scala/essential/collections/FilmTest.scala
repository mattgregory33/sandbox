package com.mgregory.sandbox.scala.essential.collections

import org.scalatest.{FlatSpec, Matchers}

class FilmTest extends FlatSpec with Matchers {

  val memento = Film("Memento", 2000, 8.5)
  val darkKnight = Film("Dark Knight", 2008, 9.0)
  val inception = Film("Inception", 2010, 8.8)
  val highPlainsDrifter = Film("High Plains Drifter", 1973, 7.7)
  val outlawJoseyWales = Film("The Outlaw Josey Wales", 1976, 7.9)
  val unforgiven = Film("Unforgiven", 1992, 8.3)
  val granTorino = Film("Gran Torino", 2008, 8.2)
  val invictus = Film("Invictus", 2009, 7.4)
  val predator = Film("Predator", 1987, 7.9)
  val dieHard = Film("Die Hard", 1988, 8.3)
  val huntForRedOctober = Film("The Hunt for Red October", 1990, 7.6)
  val thomasCrownAffair = Film("The Thomas Crown Affair", 1999, 6.8)

  val films = Seq(memento, darkKnight, inception, highPlainsDrifter, outlawJoseyWales, unforgiven, granTorino, invictus,
                  predator, dieHard, huntForRedOctober, thomasCrownAffair)

  val eastwood = Director("Clint", "Eastwood", 1930, Seq(highPlainsDrifter, outlawJoseyWales, unforgiven, granTorino))
  val mcTiernan = Director("John", "McTiernan", 1951, Seq(predator, dieHard, huntForRedOctober, thomasCrownAffair))
  val nolan = Director("Christopher", "Nolan", 1970, Seq(memento, darkKnight, inception))
  val someGuy = Director("Just", "Some Guy", 1990, Seq())

  val directors = Seq (eastwood, mcTiernan, nolan, someGuy)

  def numberOfFilms(num: Int): Seq[Director] = {
    directors.filter(d => d.films.size > num)
  }

  "numberOfFilms" should "return the directors who have directed more than the given number of films" in {
    numberOfFilms(0) should equal (Seq(eastwood, mcTiernan, nolan))
    numberOfFilms(3) should equal (Seq(eastwood, mcTiernan))
    numberOfFilms(-1) should equal (Seq(eastwood, mcTiernan, nolan, someGuy))
    numberOfFilms(4) should equal (Seq())
  }

  def bornBefore(year: Int): Option[Director] = {
    directors.find(d => d.yearOfBirth < year)
  }

  "bornBefore" should "return a director who was born before the given year" in {
    bornBefore(1971) should equal (Some(eastwood))
    bornBefore(1991) should equal (Some(eastwood))
    bornBefore(1930) should equal (None)
  }

  def bornBeforeAndNumberOfFilms(year: Int, num: Int): Seq[Director] = {
    directors.filter(d => d.yearOfBirth < year).filter(d => d.films.size > num)
  }

  "bornBeforeAndNumberOfFilms" should "combine both predicates" in {
    bornBeforeAndNumberOfFilms(1930, 0) should equal (Seq())
    bornBeforeAndNumberOfFilms(1991, 4) should equal (Seq())
    bornBeforeAndNumberOfFilms(1971, 3) should equal (Seq(eastwood, mcTiernan))
  }

  def ascending(asc: Boolean = true): Seq[Director] = {
    if (asc) directors.sortWith(_.yearOfBirth > _.yearOfBirth) else directors.sortWith(_.yearOfBirth < _.yearOfBirth)
  }

  "ascending" should "sort the directors by age" in {
    ascending() should equal (Seq(someGuy, nolan, mcTiernan, eastwood))
    ascending(true) should equal (Seq(someGuy, nolan, mcTiernan, eastwood))
    ascending(false) should equal (Seq(eastwood, mcTiernan, nolan, someGuy))
  }

  def nolanFilms(): Seq[String] = {
    nolan.films.map(f => f.name)
  }

  def nolanFilmsFor(): Seq[String] = {
    for {
      f <- nolan.films
    } yield {
      f.name
    }
  }

  "nolanFilms" should "return a List of the film names directed by Christopher Nolan" in {
    val expected = List("Memento", "Dark Knight", "Inception")
    nolanFilms() should equal (expected)
    nolanFilmsFor() should equal (expected)
  }

  def filmsViaDirectors(): Seq[String] = {
    directors.flatMap(d =>
      d.films.map(f => f.name))
  }

  def filmsViaDirectorsFor(): Seq[String] = {
    for {
      d <- directors
      f <- d.films
    } yield {
      f.name
    }
  }

  "filmsViaDirectors" should "return the names of all Films starting with the directors List" in {
    val expected = List("Memento", "Dark Knight", "Inception", "High Plains Drifter",
      "The Outlaw Josey Wales", "Unforgiven", "Gran Torino", "Predator", "Die Hard", "The Hunt for Red October",
      "The Thomas Crown Affair")
    filmsViaDirectors() should contain theSameElementsAs expected
    filmsViaDirectorsFor() should contain theSameElementsAs expected
  }

  def earliestMcTiernan(): Int = {
    mcTiernan.films.sortWith(_.yearOfRelease < _.yearOfRelease).head.yearOfRelease
  }

  "earliestMcTiernan" should "return the date of the earliest McTiernan film" in {
    earliestMcTiernan() should equal (1987)
  }

  def filmsSortedByIMDB(): Seq[Film] = {
    directors.flatMap(d => d.films).sortWith(_.imdbRating > _.imdbRating)
  }

  def filmsSortedByIMDBFor(): Seq[Film] = {
    val films = for {
      d <- directors
      f <- d.films
    } yield {
      f
    }

    films.sortWith(_.imdbRating > _.imdbRating)
  }

  "filmsSortedByIMDB" should "return all films sorted by IMDB rating desc" in {
    val expected = List(darkKnight, inception, memento, unforgiven, dieHard, granTorino,
      outlawJoseyWales, predator, highPlainsDrifter, huntForRedOctober, thomasCrownAffair)
    filmsSortedByIMDB() should contain theSameElementsInOrderAs expected
    filmsSortedByIMDBFor() should contain theSameElementsInOrderAs expected
  }

  def averageIMDBScore(): Double = {
    val directedFilms = directors.flatMap(_.films)
    directedFilms.foldRight(0.0)(_.imdbRating + _) / directedFilms.size
  }

  "averageIMDBScore" should "return the average IMDB score across all directed films" in {
    averageIMDBScore() should equal (8.09 +- 0.01)
  }

  def printListings(): Unit = {
    directors.foreach(d => d.films.foreach(f => println(s"Tonight Only! ${f.name} by ${d.firstName} ${d.lastName}")))
  }

  def printListingsFor(): Unit = {
    for {
      d <- directors
      f <- d.films
    } println(s"Tonight Only! ${f.name} by ${d.firstName} ${d.lastName}")
  }

  "printListings" should "print listings" in {
    printListings()
    printListingsFor()
  }

  def findEarliestFilm(): Film = {
    directors.flatMap(_.films).sortWith(_.yearOfRelease < _.yearOfRelease).head
  }

  "findEarliestFilm" should "return highPlainsDrifter" in {
    findEarliestFilm() should equal (highPlainsDrifter)
  }
}
