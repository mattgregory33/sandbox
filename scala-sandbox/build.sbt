name := "scala-sandbox"

version := "1.0"

scalaVersion := "2.12.8"
val playVersion = "2.6.20"

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play" % playVersion,
  "com.typesafe.play" %% "play-ahc-ws-standalone" % "2.0.4",
  "com.typesafe.play" %% "play-ws-standalone-json" % "2.0.4",
  "com.typesafe.akka" %% "akka-http" % "10.1.7",
  "com.typesafe.akka" %% "akka-stream-kafka" % "1.0.3",
//  "com.typesafe.akka" %% "akka-stream" % "2.5.19",
  "com.typesafe.akka" %% "akka-actor" % "2.5.23",
  "com.typesafe.akka" %% "akka-http" % "10.1.9",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.9",
  "org.scalatest" %% "scalatest" % "3.0.1",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.7" % Test
)