package org.mgregory.samples.jettyspringjersey;

import org.eclipse.jetty.server.Server;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 4/23/12
 */
public class HelloWorldMain {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("jetty-spring-jersey.xml");
        Server server = (Server) context.getBean("server");
        server.start();
    }
}
