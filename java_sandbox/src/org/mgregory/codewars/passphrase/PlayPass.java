package org.mgregory.codewars.passphrase;

public class PlayPass {

    public static char[] letters = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

    public static String playPass(String s, int n) {
        char[] chars = s.toCharArray();
        char[] transformed = new char[chars.length];
        for (int i=0 ; i<chars.length ; i++) {
            char currentChar = chars[i];
            if (Character.isLetter(currentChar)) {
                transformed[i] = transformLetter(currentChar, n);
            } else if (Character.isDigit(currentChar)) {
                transformed[i] = transformDigit(currentChar);
            } else {
                transformed[i] = currentChar;
            }
        }

        transformed = alternateCase(transformed);
        transformed = reverse(transformed);

        return new String(transformed);
    }

    private static char transformLetter(char currentChar, int numShifts) {
        for (int i=0 ; i<letters.length ; i++) {
            if (letters[i] == currentChar) {
                return letters[getShiftedLettersIndex(i, numShifts)];
            }
        }
        return currentChar;
    }

    private static int getShiftedLettersIndex(int currrentIndex, int numShifts) {
        int newIndex = currrentIndex + numShifts;
        if (newIndex >= letters.length) {
            return newIndex - letters.length;
        } else {
            return newIndex;
        }
    }

    private static char transformDigit(char currentChar) {
        return Character.forDigit(9 - Character.getNumericValue(currentChar), 10);
    }

    private static char[] alternateCase(char[] chars) {
        for (int i=0 ; i<chars.length ; i++) {
            char currentChar = chars[i];
            if (Character.isLetter(currentChar)) {
                if (i % 2 == 0) {
                    chars[i] = Character.toUpperCase(currentChar);
                } else {
                    chars[i] = Character.toLowerCase(currentChar);
                }
            }
        }
        return chars;
    }

    private static char[] reverse(char[] chars) {
        char[] reversed = new char[chars.length];
        for (int i=0 ; i<chars.length ; i++) {
            reversed[chars.length-i-1] = chars[i];
        }
        return reversed;
    }
}