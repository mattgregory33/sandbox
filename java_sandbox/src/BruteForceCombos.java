import java.util.HashSet;
import java.util.Set;

public class BruteForceCombos {

    /**
     * Prints out 4-digit padlock combinations fitting the following criteria:
     * 1) matches pattern MMDD (assumes all months have 31 days)
     * 2) contains no duplicate digits
     */
    public static void main(String[] args) {

        for (int i=0; i<=1231; i++) {
            String combo = String.format("%04d", i);
            if (valid(combo))
                System.out.println(combo);
        }

    }

    private static boolean valid(String combo) {
        return monthPartWithinRange(combo)
                && dayPartWithinRange(combo)
                && containsNoDuplicates(combo);
    }

    private static boolean dayPartWithinRange(String combo) {
        Integer dayPart = Integer.valueOf(combo.substring(2, 4));
        return dayPart <= 31;
    }

    private static boolean monthPartWithinRange(String combo) {
        Integer monthPart = Integer.valueOf(combo.substring(0, 2));
        return monthPart <= 12;
    }

    private static boolean containsNoDuplicates(String combo) {
        Set<String> digits = new HashSet<>(4);
        for (int i=0; i<4; i++) {
            digits.add(combo.substring(i, i+1));
        }

        return digits.size() == 4;
    }
}
