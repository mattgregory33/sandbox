import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        for (int monthPart=1 ; monthPart<=12 ; monthPart++) {
            for (int dayPart=1 ; dayPart<=31 ; dayPart++) {

                List<Integer> combo = Arrays.asList(0,0,0,0);
                boolean valid = true;

                if (monthPart < 10) {
                    combo.set(1, monthPart);
                } else if (monthPart != 11) {
                    combo.set(0, 1);
                    combo.set(1, monthPart - 10);
                } else {
                    // i dislike breaks but it short-circuits, a boolean-free solution would be better
                    break;
                }

                int dayPartTens = dayPart / 10;
                int dayPartOnes = dayPart % 10;

                if (valid && !combo.contains(dayPartTens)) {
                    combo.set(2, dayPartTens);
                } else {
                    valid = false;
                }

                if (valid && !combo.contains(dayPartOnes)) {
                    combo.set(3, dayPartOnes);
                } else {
                    valid = false;
                }

                if (valid)
                    printCombo(combo);
            }
        }

    }

    private static void printCombo(List<Integer> combo) {
        System.out.println(String.format("%d%d%d%d", combo.get(0), combo.get(1), combo.get(2), combo.get(3)));
    }


}
