package org.mgregory.sandbox.spring.scope;

public class InnerBean {

    public int identifyYourself() {
        return this.hashCode();
    }
}
