package org.mgregory.sandbox.spring.aspects;

import org.mgregory.sandbox.spring.Knight;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class KnightMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("knights-aspects.xml");
		Knight knight = (Knight)context.getBean("knight");
		knight.embarkOnQuest();
	}

}
