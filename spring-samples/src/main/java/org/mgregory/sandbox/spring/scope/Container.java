package org.mgregory.sandbox.spring.scope;

public class Container {

    private InnerBean inner;

    public int identifyInner() {
        return inner.identifyYourself();
    }

    public InnerBean getInner() {
        return inner;
    }

    public void setInner(InnerBean inner) {
        this.inner = inner;
    }
}
