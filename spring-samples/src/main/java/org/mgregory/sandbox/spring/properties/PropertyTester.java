package org.mgregory.sandbox.spring.properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.Environment;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 5/14/12
 */
public class PropertyTester {

    private String valueSetByPlaceholder;
    private String valueNotSetByPlaceholder;

    public String getValueSetByPlaceholder() {
        return valueSetByPlaceholder;
    }

    public void setValueSetByPlaceholder(String valueSetByPlaceholder) {
        this.valueSetByPlaceholder = valueSetByPlaceholder;
    }

    public String getValueNotSetByPlaceholder() {
        return (String)PropertiesHolder.getProperty("non.placeholder.value");
    }

    public void setValueNotSetByPlaceholder(String valueNotSetByPlaceholder) {
        this.valueNotSetByPlaceholder = valueNotSetByPlaceholder;
    }
}
