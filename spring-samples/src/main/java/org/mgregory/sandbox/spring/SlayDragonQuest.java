package org.mgregory.sandbox.spring;

public class SlayDragonQuest implements Quest {

	public void embark() {
		System.out.println("Embarking on quest to slay a dragon.");
	}

}
