package org.mgregory.sandbox.spring.properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 5/14/12
 */
public class PropertiesMain {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("properties-context.xml");
        PropertyTester tester = (PropertyTester)context.getBean("propertyTester");
        System.out.println(tester.getValueSetByPlaceholder());
        System.out.println(tester.getValueNotSetByPlaceholder());
    }
}
