package org.mgregory.sandbox.spring.properties;

import java.util.Properties;

/**
 * Created using IntelliJ IDEA.
 * User: mgregory
 * Date: 5/14/12
 */
public class PropertiesHolder {

    private static Properties properties;

    public static Object getProperty(String key) {
        return properties.getProperty(key);
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
