package org.mgregory.sandbox.spring;

public interface Knight {

	public void embarkOnQuest();
	
}
