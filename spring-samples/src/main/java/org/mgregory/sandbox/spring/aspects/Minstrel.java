package org.mgregory.sandbox.spring.aspects;

public class Minstrel {

	public void singBeforeQuest() {
		System.out.println("Fa la la, the knight is so brave!");
	}
	
	public void singAfterQuest() {
		System.out.println("Tee hee hee, the knight did embark on a quest!");
	}
}
