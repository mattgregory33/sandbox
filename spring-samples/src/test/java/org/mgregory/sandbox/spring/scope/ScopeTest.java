package org.mgregory.sandbox.spring.scope;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/scope-context.xml"})
public class ScopeTest implements ApplicationContextAware {

    ApplicationContext applicationContext;

    @Autowired
    Container containerWithSingleton;

    @Autowired
    Container containerWithPrototype;

    @Test
    public void singletonScopeReturnsSameInstance() throws Exception {
        assertThat(containerWithSingleton.identifyInner(), is(equalTo(containerWithSingleton.identifyInner())));
    }

    @Test
    public void prototypeScopeReturnsDifferentInstances() throws Exception {
       assertThat(containerWithPrototype.identifyInner(), not(equalTo(containerWithPrototype.identifyInner())));
    }

    @Test
    public void prototypeScopeReturnsDifferentInstancesUsingGetBean() throws Exception {
        Container container = (Container) applicationContext.getBean("emptyContainer");
        InnerBean inner1 = (InnerBean) applicationContext.getBean("orphanInnerPrototype");
        container.setInner(inner1);

        int result1 = container.identifyInner();

        InnerBean inner2 = (InnerBean) applicationContext.getBean("orphanInnerPrototype");
        container.setInner(inner2);

        int result2 = container.identifyInner();

        assertThat(result1, not(equalTo(result2)));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
