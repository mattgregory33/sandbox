package org.mgregory.sandbox.spring.ioc;

import org.junit.Test;
import org.mgregory.sandbox.spring.BraveKnight;
import org.mgregory.sandbox.spring.Quest;

import static org.mockito.Mockito.*;

public class BraveKnightTest {

	@Test
	public void knightShouldEmbarkOnQuest() {
		Quest mockQuest = mock(Quest.class);
		
		BraveKnight knight = new BraveKnight(mockQuest);
		knight.embarkOnQuest();
		
		verify(mockQuest, times(1)).embark();
	}

}
